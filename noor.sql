#
# TABLE STRUCTURE FOR: bank_details
#

DROP TABLE IF EXISTS `bank_details`;

CREATE TABLE `bank_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL,
  `deposit` double NOT NULL,
  `withdraw` double NOT NULL,
  `cheque` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `shop_id` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `bank_details` (`id`, `bank_id`, `deposit`, `withdraw`, `cheque`, `date`, `shop_id`) VALUES (1, 1, '0', '700', '1', '2019-09-10', 'vn_1');
INSERT INTO `bank_details` (`id`, `bank_id`, `deposit`, `withdraw`, `cheque`, `date`, `shop_id`) VALUES (2, 1, '400', '0', '1', '2019-09-10', 'cs_1');
INSERT INTO `bank_details` (`id`, `bank_id`, `deposit`, `withdraw`, `cheque`, `date`, `shop_id`) VALUES (3, 1, '600', '0', '123', '2019-09-10', 'cs_1');
INSERT INTO `bank_details` (`id`, `bank_id`, `deposit`, `withdraw`, `cheque`, `date`, `shop_id`) VALUES (4, 1, '0', '2000', '12', '2019-09-10', 'vn_1');
INSERT INTO `bank_details` (`id`, `bank_id`, `deposit`, `withdraw`, `cheque`, `date`, `shop_id`) VALUES (5, 1, '0', '10000', '123', '2019-09-10', '-');
INSERT INTO `bank_details` (`id`, `bank_id`, `deposit`, `withdraw`, `cheque`, `date`, `shop_id`) VALUES (6, 1, '0', '10000', '1', '2019-09-10', '-');
INSERT INTO `bank_details` (`id`, `bank_id`, `deposit`, `withdraw`, `cheque`, `date`, `shop_id`) VALUES (7, 2, '10000', '0', '1', '2019-09-10', '-');


#
# TABLE STRUCTURE FOR: banks
#

DROP TABLE IF EXISTS `banks`;

CREATE TABLE `banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `banks` (`id`, `bank_name`) VALUES (1, 'Cash in Hand');
INSERT INTO `banks` (`id`, `bank_name`) VALUES (2, 'Meezan Bank');
INSERT INTO `banks` (`id`, `bank_name`) VALUES (3, 'UBL LTD');


#
# TABLE STRUCTURE FOR: book
#

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` double NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `shop_id` int(11) NOT NULL,
  `discount` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `book` (`id`, `total`, `credit`, `debit`, `shop_id`, `discount`) VALUES (1, '1160', '1000', '0', 1, '160');


#
# TABLE STRUCTURE FOR: book_detail
#

DROP TABLE IF EXISTS `book_detail`;

CREATE TABLE `book_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `date` date NOT NULL,
  `shop_id` int(11) NOT NULL,
  `discount` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `book_detail` (`id`, `credit`, `debit`, `date`, `shop_id`, `discount`) VALUES (1, '400', '0', '2019-09-10', 1, '40');
INSERT INTO `book_detail` (`id`, `credit`, `debit`, `date`, `shop_id`, `discount`) VALUES (2, '0', '1200', '2019-09-10', 1, '40');
INSERT INTO `book_detail` (`id`, `credit`, `debit`, `date`, `shop_id`, `discount`) VALUES (3, '600', '0', '2019-09-10', 1, '80');


#
# TABLE STRUCTURE FOR: book_detail_vendor
#

DROP TABLE IF EXISTS `book_detail_vendor`;

CREATE TABLE `book_detail_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `date` date NOT NULL,
  `shop_id` int(11) NOT NULL,
  `discount` double NOT NULL,
  `bilty` double NOT NULL,
  `bill` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `book_detail_vendor` (`id`, `credit`, `debit`, `date`, `shop_id`, `discount`, `bilty`, `bill`) VALUES (1, '700', '2000', '2019-09-10', 1, '93', '0', '1');
INSERT INTO `book_detail_vendor` (`id`, `credit`, `debit`, `date`, `shop_id`, `discount`, `bilty`, `bill`) VALUES (2, '2000', '0', '2019-09-10', 1, '0', '0', '');


#
# TABLE STRUCTURE FOR: book_total
#

DROP TABLE IF EXISTS `book_total`;

CREATE TABLE `book_total` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` double NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `shop_id` int(11) NOT NULL,
  `discount` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `book_total` (`id`, `total`, `credit`, `debit`, `shop_id`, `discount`) VALUES (1, '1680', '1000', '0', 1, '80');


#
# TABLE STRUCTURE FOR: book_total_vendor
#

DROP TABLE IF EXISTS `book_total_vendor`;

CREATE TABLE `book_total_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` double NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `shop_id` int(11) NOT NULL,
  `discount` double NOT NULL,
  `bilty` double NOT NULL,
  `bill` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `book_total_vendor` (`id`, `total`, `credit`, `debit`, `shop_id`, `discount`, `bilty`, `bill`) VALUES (1, '2793', '2700', '0', 1, '93', '0', '1');


#
# TABLE STRUCTURE FOR: book_vendor
#

DROP TABLE IF EXISTS `book_vendor`;

CREATE TABLE `book_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` double NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `shop_id` int(11) NOT NULL,
  `discount` double NOT NULL,
  `bilty` double NOT NULL,
  `bill` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `book_vendor` (`id`, `total`, `credit`, `debit`, `shop_id`, `discount`, `bilty`, `bill`) VALUES (1, '2793', '2700', '0', 1, '93', '0', '1');


#
# TABLE STRUCTURE FOR: category
#

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

INSERT INTO `category` (`id`, `name`) VALUES (1, 'Floor Waste');
INSERT INTO `category` (`id`, `name`) VALUES (2, 'Jet Shower');
INSERT INTO `category` (`id`, `name`) VALUES (3, 'Toilet Shower');
INSERT INTO `category` (`id`, `name`) VALUES (4, 'Towel');
INSERT INTO `category` (`id`, `name`) VALUES (5, 'Bib Cock');
INSERT INTO `category` (`id`, `name`) VALUES (6, 'Double Bib Cock');
INSERT INTO `category` (`id`, `name`) VALUES (7, 'Shower Rod');
INSERT INTO `category` (`id`, `name`) VALUES (8, 'Ball Valve');
INSERT INTO `category` (`id`, `name`) VALUES (9, 'Under Ground');
INSERT INTO `category` (`id`, `name`) VALUES (10, 'Flush Tank');
INSERT INTO `category` (`id`, `name`) VALUES (11, 'Nipple');
INSERT INTO `category` (`id`, `name`) VALUES (12, 'U Clip');
INSERT INTO `category` (`id`, `name`) VALUES (13, 'Mirror');
INSERT INTO `category` (`id`, `name`) VALUES (14, 'Main Hole');
INSERT INTO `category` (`id`, `name`) VALUES (15, 'Side Pillar Cock');
INSERT INTO `category` (`id`, `name`) VALUES (16, 'Sink Cock');
INSERT INTO `category` (`id`, `name`) VALUES (17, 'Accessories Set');
INSERT INTO `category` (`id`, `name`) VALUES (18, 'Bowel');
INSERT INTO `category` (`id`, `name`) VALUES (19, 'Wall Shower');
INSERT INTO `category` (`id`, `name`) VALUES (20, 'Tee Cock');
INSERT INTO `category` (`id`, `name`) VALUES (21, 'Sink Mixture');
INSERT INTO `category` (`id`, `name`) VALUES (22, 'Chuck Valve');
INSERT INTO `category` (`id`, `name`) VALUES (24, 'Non Return Valve');
INSERT INTO `category` (`id`, `name`) VALUES (25, 'Set Cover');
INSERT INTO `category` (`id`, `name`) VALUES (26, 'Comode Thimble');
INSERT INTO `category` (`id`, `name`) VALUES (27, 'Gizer');
INSERT INTO `category` (`id`, `name`) VALUES (28, 'Basin Mixture');
INSERT INTO `category` (`id`, `name`) VALUES (29, 'Basin Waste');
INSERT INTO `category` (`id`, `name`) VALUES (30, 'Basin');
INSERT INTO `category` (`id`, `name`) VALUES (31, 'Waste Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (33, 'Soup Dish');
INSERT INTO `category` (`id`, `name`) VALUES (34, 'Paper Holder');
INSERT INTO `category` (`id`, `name`) VALUES (35, 'Cell Tape');
INSERT INTO `category` (`id`, `name`) VALUES (36, 'Chanch Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (37, 'Bolt Kit');
INSERT INTO `category` (`id`, `name`) VALUES (38, 'Sheilf');
INSERT INTO `category` (`id`, `name`) VALUES (39, 'Shampoo Dish');
INSERT INTO `category` (`id`, `name`) VALUES (40, 'Elbow');
INSERT INTO `category` (`id`, `name`) VALUES (41, 'Tee');
INSERT INTO `category` (`id`, `name`) VALUES (42, 'Socket');
INSERT INTO `category` (`id`, `name`) VALUES (43, 'EndCap');
INSERT INTO `category` (`id`, `name`) VALUES (44, 'Union');
INSERT INTO `category` (`id`, `name`) VALUES (45, 'OverCross');
INSERT INTO `category` (`id`, `name`) VALUES (46, 'Mixture Elbow 25*1/2\"');
INSERT INTO `category` (`id`, `name`) VALUES (47, 'Copper Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (48, 'Plug');
INSERT INTO `category` (`id`, `name`) VALUES (49, 'Jubly Clump');
INSERT INTO `category` (`id`, `name`) VALUES (50, 'U Clump');
INSERT INTO `category` (`id`, `name`) VALUES (51, 'GI Pipe Nipple');
INSERT INTO `category` (`id`, `name`) VALUES (52, 'Nozel');
INSERT INTO `category` (`id`, `name`) VALUES (53, 'Gate Valve');
INSERT INTO `category` (`id`, `name`) VALUES (54, 'PPRC Pipes Pn 20');
INSERT INTO `category` (`id`, `name`) VALUES (55, 'PPRC Pipes Pn 16');
INSERT INTO `category` (`id`, `name`) VALUES (56, 'Yee');
INSERT INTO `category` (`id`, `name`) VALUES (57, 'Connector');
INSERT INTO `category` (`id`, `name`) VALUES (58, 'Head');
INSERT INTO `category` (`id`, `name`) VALUES (59, 'Sink Waste');
INSERT INTO `category` (`id`, `name`) VALUES (60, 'Rowel Plug');
INSERT INTO `category` (`id`, `name`) VALUES (61, 'Connection Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (62, 'Nick');
INSERT INTO `category` (`id`, `name`) VALUES (63, 'Daga');
INSERT INTO `category` (`id`, `name`) VALUES (64, 'Gas');
INSERT INTO `category` (`id`, `name`) VALUES (65, 'Mohra');
INSERT INTO `category` (`id`, `name`) VALUES (66, 'Hockey Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (67, 'Brush Holder');
INSERT INTO `category` (`id`, `name`) VALUES (68, 'Steel Cap');
INSERT INTO `category` (`id`, `name`) VALUES (69, 'Mixture Pipes');
INSERT INTO `category` (`id`, `name`) VALUES (70, 'Supreme Asia Ceramics');
INSERT INTO `category` (`id`, `name`) VALUES (71, 'PCS Ceramics');
INSERT INTO `category` (`id`, `name`) VALUES (72, 'Band');
INSERT INTO `category` (`id`, `name`) VALUES (73, 'PVC Solution');
INSERT INTO `category` (`id`, `name`) VALUES (74, 'Supida');
INSERT INTO `category` (`id`, `name`) VALUES (75, 'Tanky Bush');
INSERT INTO `category` (`id`, `name`) VALUES (76, 'Plug Tee');
INSERT INTO `category` (`id`, `name`) VALUES (77, 'Ball Cock');
INSERT INTO `category` (`id`, `name`) VALUES (78, 'Sink');
INSERT INTO `category` (`id`, `name`) VALUES (79, 'MF Elbow');
INSERT INTO `category` (`id`, `name`) VALUES (80, 'PVC Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (81, 'P Trape');
INSERT INTO `category` (`id`, `name`) VALUES (82, 'PPR Stop Cock');
INSERT INTO `category` (`id`, `name`) VALUES (83, 'Water Tank');
INSERT INTO `category` (`id`, `name`) VALUES (85, 'Hydensy Color Pipes');
INSERT INTO `category` (`id`, `name`) VALUES (86, 'Plug Elbow');
INSERT INTO `category` (`id`, `name`) VALUES (87, 'MF Tee');
INSERT INTO `category` (`id`, `name`) VALUES (88, 'HDPE Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (89, 'Tayal ');
INSERT INTO `category` (`id`, `name`) VALUES (90, 'Plug Socket');
INSERT INTO `category` (`id`, `name`) VALUES (91, 'Loha ');
INSERT INTO `category` (`id`, `name`) VALUES (92, 'Water Proof');
INSERT INTO `category` (`id`, `name`) VALUES (93, 'PPRC Pipes Pn 16');
INSERT INTO `category` (`id`, `name`) VALUES (94, 'JD  Pump');
INSERT INTO `category` (`id`, `name`) VALUES (95, 'CP Asia ');
INSERT INTO `category` (`id`, `name`) VALUES (96, 'Garden Pipes');
INSERT INTO `category` (`id`, `name`) VALUES (97, 'Gi Bush');
INSERT INTO `category` (`id`, `name`) VALUES (98, 'Keel');
INSERT INTO `category` (`id`, `name`) VALUES (99, 'Gi Pipe');
INSERT INTO `category` (`id`, `name`) VALUES (100, 'PPRC Katter');
INSERT INTO `category` (`id`, `name`) VALUES (101, 'PPRC  Heater');


#
# TABLE STRUCTURE FOR: customers
#

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `customers` (`id`, `fullname`, `email`, `address`, `phone`, `status`) VALUES (1, 'Cash Counter', '', '', '333', 1);


#
# TABLE STRUCTURE FOR: expense_names
#

DROP TABLE IF EXISTS `expense_names`;

CREATE TABLE `expense_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `expense_names` (`id`, `exp_name`) VALUES (1, 'Bilty');
INSERT INTO `expense_names` (`id`, `exp_name`) VALUES (2, 'Rent');


#
# TABLE STRUCTURE FOR: expenses
#

DROP TABLE IF EXISTS `expenses`;

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_id` bigint(20) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `comments` text NOT NULL,
  `date` date NOT NULL,
  `rec_no` varchar(255) NOT NULL,
  `bank` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `expenses` (`id`, `exp_id`, `amount`, `comments`, `date`, `rec_no`, `bank`) VALUES (1, '2', '10000', 'Building', '2019-09-10', '123', 1);


#
# TABLE STRUCTURE FOR: invoice
#

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `invoice` (`invoice_id`, `shop_id`, `date`) VALUES (1, 1, '2019-09-10');
INSERT INTO `invoice` (`invoice_id`, `shop_id`, `date`) VALUES (2, 1, '2019-09-10');
INSERT INTO `invoice` (`invoice_id`, `shop_id`, `date`) VALUES (3, 1, '2019-09-10');


#
# TABLE STRUCTURE FOR: invoices
#

DROP TABLE IF EXISTS `invoices`;

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` double NOT NULL,
  `paid` double NOT NULL,
  `old` double NOT NULL,
  `net` double NOT NULL,
  `discount` double NOT NULL,
  `inv_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `type_payment` varchar(200) NOT NULL,
  `billno` varchar(200) NOT NULL,
  `comments` text NOT NULL,
  `dis_per` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inv_id` (`inv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `invoices` (`id`, `total`, `paid`, `old`, `net`, `discount`, `inv_id`, `date`, `type_payment`, `billno`, `comments`, `dis_per`) VALUES (1, '440', '400', '0', '0', '40', 2, '2019-09-10', '1-1', '', '', '');
INSERT INTO `invoices` (`id`, `total`, `paid`, `old`, `net`, `discount`, `inv_id`, `date`, `type_payment`, `billno`, `comments`, `dis_per`) VALUES (2, '720', '0', '0', '680', '40', 3, '2019-09-10', ' ', '', '', '');


#
# TABLE STRUCTURE FOR: invoices_vendor
#

DROP TABLE IF EXISTS `invoices_vendor`;

CREATE TABLE `invoices_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` double NOT NULL,
  `paid` double NOT NULL,
  `old` double NOT NULL,
  `net` double NOT NULL,
  `discount` double NOT NULL,
  `inv_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `type_payment` varchar(200) NOT NULL,
  `bilty` double NOT NULL,
  `bill` int(11) NOT NULL,
  `comments` text NOT NULL,
  `dis_per` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inv_id` (`inv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `invoices_vendor` (`id`, `total`, `paid`, `old`, `net`, `discount`, `inv_id`, `date`, `type_payment`, `bilty`, `bill`, `comments`, `dis_per`) VALUES (1, '2793', '700', '0', '2000', '93', 1, '2019-09-10', '1-1', '0', 1, '', '');


#
# TABLE STRUCTURE FOR: logs
#

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact` varchar(200) NOT NULL,
  `msg` text NOT NULL,
  `date` datetime NOT NULL,
  `from_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `logs` (`id`, `contact`, `msg`, `date`, `from_user`) VALUES (1, '333', 'Dear Cash Counter, Your Invoice No. 2 has Total Amount: 440, Paid Amount: 400, Old Balance: 0. Net Amount: 0. Please Deposit As Soon As Possible.', '2019-09-10 19:59:43', 3);
INSERT INTO `logs` (`id`, `contact`, `msg`, `date`, `from_user`) VALUES (2, '333', 'Dear Cash Counter, Your Invoice No. 3 has Total Amount: 1240, Paid Amount: 0, Old Balance: 0. Net Amount: 1200. Please Deposit As Soon As Possible.', '2019-09-10 20:03:53', 3);
INSERT INTO `logs` (`id`, `contact`, `msg`, `date`, `from_user`) VALUES (3, '923339471086', 'Dear Fixee Swat, You have Paid: 10000 on Receipt No. 123 on the Account of Bilty', '2019-09-10 20:06:08', 3);


#
# TABLE STRUCTURE FOR: products
#

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `pprice` double NOT NULL,
  `sprice` double NOT NULL,
  `hprice` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=629 DEFAULT CHARSET=latin1;

INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (1, '8*8 Normal', 1, '115', '180', '130');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (2, '6*6 Dell', 1, '58', '100', '65');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (3, '6*6 Normal', 1, '40', '80', '50');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (4, 'China Hard', 1, '125', '180', '135');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (5, 'CP', 2, '110', '200', '125');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (6, 'PCI', 2, '120', '180', '130');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (7, 'CP Normal', 3, '310', '450', '330');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (8, 'CP Hard', 3, '410', '550', '440');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (9, 'Ring', 4, '210', '300', '230');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (10, 'Rod', 4, '210', '300', '220');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (11, 'Plastic', 4, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (12, 'Master CP', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (13, 'Nice ABS', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (14, 'China Fancy', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (15, 'Yousaf', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (16, 'China CP', 6, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (17, 'Plastic ABS', 6, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (18, 'Yousaf CP', 6, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (19, 'Brass', 7, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (20, 'Plastic', 7, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (21, 'Master 1\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (22, 'Master 1/2\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (23, 'Master 3/4\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (24, 'IA 1\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (25, 'IA 1/2\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (26, 'IA 3/4\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (27, 'Yousaf 1\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (28, 'Yousaf 1/2\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (29, 'Yousaf 3/4\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (30, 'Faisal 1\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (31, 'Faisal 1/2\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (32, 'Faisal 3/4\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (33, 'LED', 2, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (34, 'Fazal 3/4\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (35, 'IA 1-1/2\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (36, 'Faisal 1/2\"', 9, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (37, 'Hi Tech', 10, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (38, 'Grace', 10, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (39, 'Dawn', 10, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (40, 'Dawn Plate', 10, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (41, 'CP Hard 1\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (42, 'CP Hard 1-1/2\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (43, 'CP Hard 2\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (44, 'CP Hard 4\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (45, '4\"', 12, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (46, '2\"', 12, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (47, '3\"', 12, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (48, 'Chawras', 13, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (49, 'China Fancy', 13, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (50, 'ABS Hard', 3, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (51, 'Plastic Hard 9*9', 14, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (52, 'Plastic Hard 12*12', 14, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (53, 'Master Brass', 15, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (54, 'Master Brass', 16, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (55, '5 Peice ', 17, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (56, 'CP 18 Normal', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (57, '1\"', 12, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (58, '3/4\"', 12, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (59, '1/2\"', 12, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (60, '6 Piece', 17, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (61, '14*17 Hard', 18, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (62, '14*17 Normal', 18, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (63, 'MZ', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (64, 'MZ', 15, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (65, 'MZ', 16, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (66, 'MZ Silver', 19, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (67, 'Yousaf CP', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (68, 'Yousaf CP', 15, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (69, 'Yousaf CP', 21, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (70, 'Master Brass', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (71, 'Hi Tech 4 Piece CP', 17, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (72, 'Bremex CP', 3, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (74, 'Victor', 2, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (75, '1-1/4\"', 22, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (77, 'Rizwan', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (78, 'Hard 3/4\" ', 24, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (79, 'Normal 1/2\" ', 24, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (80, 'CP Normal 1\" ', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (81, 'China Steel 1/2\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (82, 'CP Normal 3/4\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (83, 'CP Normal 1\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (84, 'Porta Hard', 25, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (85, 'IFO Hard', 25, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (86, 'Porta Normal', 25, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (87, 'IFO Normal', 25, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (88, 'Normal', 26, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (89, '6 Liter China', 27, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (90, 'China', 10, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (91, 'Color', 19, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (92, 'Brass Color', 28, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (93, 'China', 28, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (94, 'ABS', 28, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (95, 'Yousaf CP', 28, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (96, 'Brass', 29, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (97, 'Liver Project', 19, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (98, 'Rizwan Crown', 19, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (99, 'Rizwan Super Queen', 19, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (100, 'Faisal Economy', 19, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (101, '15*18 Color', 30, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (102, 'Smart Color', 30, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (103, 'Asia', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (104, 'Star Asia Nozzel ', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (105, 'China CP', 31, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (106, 'Grey with Waste', 31, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (107, 'Grey', 31, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (108, 'Asia', 16, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (109, 'Sunny', 3, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (110, 'China Hard', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (111, 'Only Star Asia', 19, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (112, 'Super Master', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (113, 'ABS', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (114, 'China CP', 33, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (115, 'ABS', 16, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (116, 'China CP', 34, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (117, 'CP Normal', 33, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (118, 'Jambo', 35, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (119, 'Hard Meter', 36, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (120, 'China 4*4', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (121, 'China 3*3', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (122, 'Fooly', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (123, 'PCI', 15, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (124, 'China', 37, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (125, 'Normal', 37, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (126, 'Fatry', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (127, '3\"', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (128, 'China CP', 38, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (129, 'China Plastic', 39, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (130, 'Alpha P 25*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (131, '25mm Alpha', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (132, '32mm Alpha', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (133, 'Apha P 25*3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (134, 'Normal 32*1/2\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (135, 'Alpha P 32*3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (136, 'Alpha P 32*1\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (137, '63mm Turk', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (138, '50mm Turk', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (139, '40mm Turk', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (140, '50*32mm Turk', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (141, '32mm Normal', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (142, '25mm Normal', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (143, '25*32mm Normal', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (144, '25*32mm Alpha', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (145, '25mm 45°', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (146, '32mm 45° Minhas', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (147, 'Normal P 32*1\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (148, 'Normal P 25*3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (149, 'Normal P 32*3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (150, 'Alpha 32*1/2\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (151, '25mm Acuufit', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (152, '32mm Acuufit', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (153, 'Acuufit P 25*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (154, 'Acuufit 25*3/4\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (155, '25mm Alpha', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (156, '32mm Alpha', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (157, 'Alpha P 25*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (158, 'Alpha P 25*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (159, 'Alpha 32*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (160, 'Alpha P 32*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (161, 'Alpha P 32*1\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (162, 'Normal P 32*1\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (163, 'Normal P 32*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (164, 'Normal 32*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (165, 'Normal P 25*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (166, 'Normal 25*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (167, 'Acuufit P 32*1\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (168, '25mm Alpha', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (169, '32mm Normal', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (170, '25mm Normal', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (171, '25*32mm Normal', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (172, '25mm Yellow', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (173, 'Yellow P 25*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (174, 'Normal P 25*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (175, '32mm Alpha', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (176, 'Alpha P 32*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (177, 'Alpha P 32*1\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (178, '63mm Turk', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (179, '40*32mm Turk', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (180, '50mm Turk', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (181, '63mm Turk', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (182, '40mm Turk', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (183, '40*25mm Turk', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (184, '40*32mm Turk', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (185, '25mm', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (186, '32mm', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (187, '25mm Alpha', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (188, '32mm Alpha', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (189, 'PPR 25mm China', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (190, 'PPR 32mm China', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (191, 'PPR 25mm Hard China', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (192, 'PPR 20mm China', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (193, '25mm Alpha', 45, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (194, '32mm Alpha', 45, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (195, '25*32mm Alpha', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (196, 'Alpha', 46, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (197, 'Acuufit', 46, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (198, '12\"', 47, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (199, '20mm Alpha', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (200, '20mm Normal', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (201, '25mm Normal', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (202, 'GI 1/2\"', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (203, 'GI 1-1/4\"', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (204, '32mm Normal', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (205, '32*25mm Normal', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (206, 'Minhas 32*3/4\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (207, '25mm Normal', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (208, 'Acuufit P 25*3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (209, '25mm Yellow', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (210, '2\"', 49, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (211, 'Gas', 49, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (212, '32mm Minhas', 50, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (213, '3*3/4\"', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (214, '11*3/4', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (215, '10*3/4\"', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (216, 'Water Normal 1/2\" ', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (217, 'Water Normal 1\" ', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (218, 'Water Normal 3/4\" ', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (219, 'Gas 1/2\" ', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (220, '32*25mm Alpha', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (221, '32*25mm Alpha', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (222, 'Alpha P 25*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (223, 'Normal 32*1/2\" ', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (224, 'Acuufit 32*1\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (225, 'Acuufit 40*1\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (226, '25mm Alpha', 9, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (227, 'Normal 1\"', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (228, '32mm Normal', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (229, 'HDP 32mm', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (230, '25mm Acuufit', 9, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (231, '25mm Acuufit', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (232, '25*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (233, '25mm Minhas', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (234, '32mm Minhas', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (235, '25mm Alpha', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (236, 'China CP', 21, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (237, 'China CP', 15, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (238, '25*1/2\" Yellow', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (239, '25*3/4\" P Yellow', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (240, '25*1/2\" Yellow', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (241, 'Alpha 25mm', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (242, 'Alpha 32mm', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (243, 'Alpha 25mm', 55, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (244, 'Alpha 32mm', 55, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (245, 'PVC Poly 4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (246, 'PVC Poly 3\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (247, 'PVC Poly 2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (248, 'PVC Poly 4*3\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (249, 'PVC Poly 4*2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (250, 'PVC Poly 3*2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (251, 'PVC Poly 4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (252, 'PVC Poly 4*2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (254, 'PVC Poly 4\"', 56, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (255, 'PVC Poly 3\" ', 56, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (256, 'PVC Poly 45° 4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (257, 'PVC Poly 45° \r\n 3\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (258, 'Yellow', 35, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (259, 'Small', 35, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (260, '++', 35, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (261, 'Color', 29, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (262, '32*25mm ', 57, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (263, 'HDP 20mm', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (264, 'HDP 20mm', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (265, 'HDP 25mm', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (266, 'Plastic', 58, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (267, 'Brass', 59, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (268, 'Big', 60, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (269, 'Gelatt', 16, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (270, 'Plastic', 33, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (271, 'Meter Daga', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (272, 'Meter Hard', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (273, 'Hard 18\" ', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (274, 'Hard 24\" ', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (275, 'Spring Hard', 62, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (276, 'China Hard', 62, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (277, 'Gool', 63, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (278, 'China CP Small', 7, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (279, 'China CP Large', 7, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (280, 'No. 12', 60, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (281, 'Clip Small', 13, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (282, '12*3/4\" ', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (283, '8*3/4\"', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (284, '6*3/4\"', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (285, '5*3/4\"', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (286, '4*3/4\"', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (287, '3*1/2\"', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (288, '24\"', 47, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (289, '18\"', 47, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (290, 'Plastic', 59, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (291, 'K', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (292, '25mm Yellow', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (293, 'China', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (294, 'Socket', 64, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (295, 'Tee', 64, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (296, 'Poly', 15, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (297, 'ABS', 3, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (298, '1/4\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (299, 'Big 1/4\" ', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (300, 'Normal', 65, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (301, 'Hard', 65, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (302, 'Hard Big', 65, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (303, 'Color', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (304, 'Color', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (305, 'China', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (306, 'Minhas P \r\n 32*1\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (307, '25mm Minhas', 9, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (308, 'Minhas P \r\n 25*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (309, 'Minhas P \r\n 25*3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (310, 'Fatry 3\"', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (311, 'Plastic', 38, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (312, 'Normal Meter', 36, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (313, 'Minhas P \r\n 25*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (314, 'Minhas 1\"', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (315, 'Minhas P \r\n 25*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (316, 'Minhas P 25*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (317, 'Daga 24\" ', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (318, 'Daga 18\" ', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (319, 'Normal', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (320, 'Normal', 66, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (321, 'Yousaf Nozzel', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (322, 'Plastic', 34, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (323, '15*19 Normal', 18, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (324, 'China Fancy', 67, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (325, '8*8 CP Normal', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (326, 'China Steel 1\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (327, 'Meter cp', 47, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (328, '14 Hard Steel ', 1, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (329, 'Normal 18+24\" ', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (330, '40mm Minhas', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (331, '40mm', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (332, 'Bib Cock', 68, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (333, 'Underground', 68, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (334, '24\"', 69, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (335, 'Hard 18\" ', 69, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (336, 'Gool Plastic 12\"', 14, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (337, 'Gool Plastic 9\" ', 14, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (338, 'Turky Handal ', 5, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (339, 'GI 2*1\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (340, 'GI Cross 1\" ', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (341, 'GI Cross 3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (342, 'GI 1-1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (343, 'GI 1-1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (344, 'GI 1-1/2\"', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (345, 'GI 1-1/4\" ', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (346, 'GI 1-1/4\"', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (347, 'GI 1-1/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (348, 'GI 4\"', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (349, 'GI 4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (350, 'GI 2-1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (351, 'GI 2-1/2\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (352, 'GI 3\"', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (353, 'GI 1\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (354, 'GI 1\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (355, 'GI 1\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (356, 'GI 1-1/4*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (357, 'GI 1-1/2*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (358, 'GI 1-1/4*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (359, 'GI 2*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (360, 'GI 1-1/2*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (361, 'GI 1*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (362, 'GI 1*3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (363, 'GI 1-1/2*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (364, 'GI 1-1/4*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (366, 'GI 1-1/4*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (367, 'GI 1-1/2*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (368, 'Orrisa Pari Plate White', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (369, 'Orrisa Small White', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (370, 'Baisan Smart White', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (371, 'Brave White', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (372, 'Modren White', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (373, 'Seto Teady White', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (374, 'Baby A', 71, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (375, 'Orrisa A', 71, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (376, 'Orrisa B', 71, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (377, 'PVC Poly 4*6\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (378, 'PVC Poly 4*3\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (379, 'PVC Poly 2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (380, 'PVC Poly 3\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (381, 'PVC Poly 4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (382, 'PVC Poly 6\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (383, 'PVC Poly 5*6\" ', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (384, 'China 12\"', 61, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (386, 'Seto Teady B', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (387, 'PVC Hard 4\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (388, 'Hard 3\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (390, 'Hard 3', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (391, 'GI 1-1/2*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (392, 'GI 1*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (393, 'GI 1*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (394, 'GI 3/4*1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (395, 'GI 1-1/4*1-1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (396, 'Bush 1-1/2*1\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (397, 'Bush 1-1/4*3/4\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (398, 'Degi Bush 1-1/2*1/4\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (399, 'Degi Bush 1-1/2*1/2\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (400, 'Degi Bush 1-1/2*3/4\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (401, 'GI 4*2-1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (402, 'GI 3\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (403, 'GI 2*1\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (404, 'GI 3*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (405, 'GI 3\"', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (406, 'Valve Meter 3/4\"', 64, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (407, 'Chawras 6\"', 14, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (408, 'GI 2*1-1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (409, 'GI 2*1-1/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (410, 'GI 1-1/2*1-1/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (411, 'GI 1-1/4*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (412, 'GI 1-1/4*1\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (413, 'PPR Hard China 1\" ', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (414, 'GI 2-1/2\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (415, 'GI 2\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (416, 'GI 1-1/2\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (417, 'GI 1-1/4\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (418, 'ACM 75ML ', 73, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (419, 'ACM 117ML', 73, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (420, 'ACM 237ML', 73, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (421, 'Small', 74, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (422, 'GI 3/4\"', 45, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (423, 'GI 1/2\"', 45, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (424, 'PVC Poly 3\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (425, '32mm Normal', 9, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (426, 'PVC 5*4 Poly', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (427, '3/4*1/2\"', 75, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (428, 'PVC Poly 4*3\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (429, 'PVC 3\"', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (430, 'Hard 4', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (431, 'Hard 2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (432, 'Hard 3\"', 76, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (433, 'Hard 4\" ', 56, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (434, 'Hard 3\"', 56, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (435, 'Poly 4\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (436, 'PVC Poly 4*2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (437, 'Poly 2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (438, 'Turk 40mm', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (439, 'English Comode', 77, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (440, 'Normal 3/4\"', 77, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (441, 'Hard 3/4\"', 77, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (442, '18*36 Normal', 78, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (443, '18*36 Medium', 78, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (444, '32mm Acuufit', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (445, 'Minhas P 32*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (446, 'Minhas P 25*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (447, '32mm Minhas', 45, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (448, '25mm Minhas', 45, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (449, '32*25mm Minhas', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (450, 'Minhas', 46, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (451, '25mm Minhas', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (452, '32mm Minhas', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (453, '25mm Minhas', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (454, '32*25mm Minhas', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (455, 'Minhas 1/2\"', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (456, 'Minhas 3/4\"', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (457, '25mm Minhas', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (458, 'GI 2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (459, 'GI 2\"', 48, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (460, 'GI 1/2\"', 79, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (461, 'GI 3/4\"', 79, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (462, 'GI 1\"', 79, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (463, 'GI 1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (464, 'GI 3/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (465, 'GI 1\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (466, 'GI 1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (467, 'GI 3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (468, 'GI 1\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (469, 'GI 1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (470, 'GI 3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (471, 'GI 1\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (472, 'GI 1/2', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (473, 'GI 3/4\"', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (474, 'GI 1\"', 44, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (475, 'GI 1/2\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (476, 'GI 3/4\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (477, 'GI 1\"', 11, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (478, 'GI 1/2\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (479, 'GI 3/4\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (480, 'GI 1\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (481, 'R 3/4*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (482, '1*1/2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (483, 'GI 1*1-1/4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (484, 'GI 1*1-1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (485, 'GI  2*3/4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (486, 'GI 2-1/2*1\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (487, 'GI 2*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (488, 'GI 1*1-1/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (489, 'GI Cross 1/2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (490, 'Degi Bush 1*3/4\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (491, 'Degi Bush 1-1/4*1/2\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (492, '32mm Minhas', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (493, '25mm Minhas', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (494, '40mm Minhas', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (495, 'Acuufit Universal 2\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (496, 'GI 1*3/4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (497, 'Universal Medium 4\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (498, 'Universal Medium 3\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (499, 'Durr 4', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (500, 'Acuufit 4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (501, 'Acuufit 250ML', 73, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (502, 'Alpha P 25*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (503, '32mm Alpha', 53, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (504, '25mm Alpha', 82, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (505, '125GL Polo', 83, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (506, 'Atlas 1000L', 83, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (507, '400GL Polo', 83, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (508, '300GL Polo', 83, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (509, '200GL Polo', 83, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (510, 'Atlas 1200L', 83, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (511, 'Classic Star 2\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (512, 'Classic Star 3\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (513, 'Classic Star 4\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (514, 'Classic Star 5\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (515, 'Classic Star 6\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (516, '32mm Minhas', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (517, '32mm Minhas', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (518, 'GI 3/4*1/2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (519, 'Poly 4\"', 79, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (520, 'Poly 3', 79, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (521, 'MZ', 21, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (522, 'Gray 4.5\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (523, 'Gray 5\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (524, 'Gray 6\" ', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (525, '4\"', 85, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (526, '3', 85, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (527, '32mm Acuufit', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (528, '25mm Acuufit', 55, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (529, '25mm Acuufit', 54, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (530, '20mm Normal', 55, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (531, 'Acuufit 2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (532, 'Acuufit 3\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (533, 'Acuufit 45°', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (534, 'Acuufit 45° 3\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (535, 'Acuufit 45° 4\" ', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (536, 'Acuufit 3\"', 86, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (537, 'Acuufit 4\"', 86, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (538, 'Acuufit 3\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (539, 'Acuufit 4\"', 72, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (540, 'Acuufit 2\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (541, 'Acuufit 3\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (542, 'Acuufit 4\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (543, 'Acuufit 3\"', 79, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (544, 'Acuufit 4\"', 79, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (545, 'Acuufit 3\"', 87, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (546, 'Acuufit 4\"', 87, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (547, 'Acuufit 4\"', 76, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (548, 'Acuufit 3\"', 76, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (549, 'Acuufit 3\"', 56, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (550, 'Acuufit 4\"', 56, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (551, 'Acuufit 2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (552, 'Acuufit 3\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (553, 'Acuufit 4\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (554, 'Acuufit 3\"', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (555, 'Acuufit 4\"', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (556, 'Acuufit 4*3', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (557, 'Acuufit 4*3', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (558, 'Acuufit 4*3', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (559, 'Acuufit 3\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (560, 'Acuufit 2\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (561, 'Acuufit 4\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (562, '2', 85, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (563, 'Pure 20mm', 88, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (564, 'Pure 25mm', 88, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (565, 'Pure 32mm', 88, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (566, 'Color 20mm', 88, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (567, 'Color 25mm', 88, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (568, 'Color 32mm', 88, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (569, 'Orrisa Pari Plate Color', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (570, 'Orrisa Small Color', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (571, '15*18 White', 30, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (572, 'Baisan Smart Color', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (573, 'Cornor Basin White', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (574, 'Cornor Basin Color', 70, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (575, 'Alpha B 4\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (576, 'Sarvice 1/2\"', 52, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (577, 'Alpha 25mm', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (578, 'Alpha 32mm', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (579, 'PVC Hard 45° 2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (580, 'King Star 3\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (581, 'King Star 4\"', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (582, 'King Star Conduite  4\" ', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (583, 'Turk 3\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (584, 'Turk 4\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (585, 'Bond Polo', 89, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (586, 'Turk 4\"', 90, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (587, 'Turk 3\"', 90, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (588, 'hook ', 91, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (589, 'Patty', 92, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (590, 'Acuufit 32mm', 93, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (591, '4*1/2', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (592, '6*1/2', 51, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (593, 'A C M 473 ML', 73, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (594, 'S T 237 ML', 73, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (595, 'Hard 2\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (596, 'Gray 6\" 13kg hard', 80, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (597, 'M Z', 20, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (598, 'M Z', 6, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (599, 'Turk 4\"', 76, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (600, 'Turk 3\"', 76, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (601, 'Turk 3*2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (602, 'Turk 4*2\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (603, 'Turk 4\"', 40, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (604, 'Turk 3\"', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (605, '.50', 94, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (606, 'Ampelar .75 ', 94, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (607, 'Bib Cock', 95, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (608, 'Supream Asia 3\" ', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (609, '3/4\" Normal', 96, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (610, '3/4 Pure', 96, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (611, '1*3/4\"', 97, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (612, 'Peach 1.1/2', 98, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (613, '1/2\"', 99, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (614, 'Fazal 1/2\"', 8, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (615, 'PCS A 4\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (616, 'PCS B 4\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (617, 'PCS A 3\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (618, 'PCS B 3\"', 81, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (619, 'Chaina', 100, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (620, 'Chaina', 101, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (621, 'Pprc Minhas32*25', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (622, 'Pprc Minhas 32mm', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (623, 'Pprc Minhas 25mm', 43, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (624, 'Pprc Minhas 40mm', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (625, 'Pprc Minhas 25mm', 41, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (626, 'Pprc Minhas Point 40*1\"', 42, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (627, 'IIL El 3/4', 99, '0', '0', '0');
INSERT INTO `products` (`id`, `name`, `cat_id`, `pprice`, `sprice`, `hprice`) VALUES (628, 'Pax Gota 3/4', 99, '0', '0', '0');


#
# TABLE STRUCTURE FOR: returns
#

DROP TABLE IF EXISTS `returns`;

CREATE TABLE `returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `sale` int(255) NOT NULL,
  `date` date NOT NULL,
  `inv_id` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `returns` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`) VALUES (1, 1, 1, '2019-09-10', 2, '180');
INSERT INTO `returns` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`) VALUES (2, 2, 1, '2019-09-10', 2, '100');
INSERT INTO `returns` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`) VALUES (3, 1, 1, '2019-09-10', 3, '180');
INSERT INTO `returns` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`) VALUES (4, 3, 1, '2019-09-10', 3, '80');
INSERT INTO `returns` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`) VALUES (5, 4, 1, '2019-09-10', 3, '180');
INSERT INTO `returns` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`) VALUES (6, 3, 1, '2019-09-10', 3, '80');


#
# TABLE STRUCTURE FOR: sale
#

DROP TABLE IF EXISTS `sale`;

CREATE TABLE `sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(255) NOT NULL,
  `sale` double NOT NULL,
  `date` date NOT NULL,
  `inv_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `sprice` double NOT NULL,
  `billno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `sale` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`, `sprice`, `billno`) VALUES (3, 3, '2', '2019-09-10', 2, '80', '40', '1');
INSERT INTO `sale` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`, `sprice`, `billno`) VALUES (4, 1, '1', '2019-09-10', 2, '180', '115', '1');
INSERT INTO `sale` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`, `sprice`, `billno`) VALUES (5, 2, '1', '2019-09-10', 2, '100', '58', '1');
INSERT INTO `sale` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`, `sprice`, `billno`) VALUES (6, 1, '2', '2019-09-10', 3, '180', '115', '1');
INSERT INTO `sale` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`, `sprice`, `billno`) VALUES (7, 3, '0', '2019-09-10', 3, '80', '40', '1');
INSERT INTO `sale` (`id`, `product_id`, `sale`, `date`, `inv_id`, `price`, `sprice`, `billno`) VALUES (8, 4, '2', '2019-09-10', 3, '180', '125', '1');


#
# TABLE STRUCTURE FOR: sms_temp
#

DROP TABLE IF EXISTS `sms_temp`;

CREATE TABLE `sms_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms` text NOT NULL,
  `from_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: stock
#

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(255) NOT NULL,
  `stock` double NOT NULL,
  `date` date NOT NULL,
  `inv_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `biltyno` varchar(255) NOT NULL,
  `bill` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `stock` (`id`, `product_id`, `stock`, `date`, `inv_id`, `price`, `biltyno`, `bill`) VALUES (1, 1, '5', '2019-09-10', 1, '115', '1', 1);
INSERT INTO `stock` (`id`, `product_id`, `stock`, `date`, `inv_id`, `price`, `biltyno`, `bill`) VALUES (2, 2, '1', '2019-09-10', 1, '58', '1', 1);
INSERT INTO `stock` (`id`, `product_id`, `stock`, `date`, `inv_id`, `price`, `biltyno`, `bill`) VALUES (3, 3, '4', '2019-09-10', 1, '40', '1', 1);
INSERT INTO `stock` (`id`, `product_id`, `stock`, `date`, `inv_id`, `price`, `biltyno`, `bill`) VALUES (4, 4, '16', '2019-09-10', 1, '125', '1', 1);


#
# TABLE STRUCTURE FOR: total_stock
#

DROP TABLE IF EXISTS `total_stock`;

CREATE TABLE `total_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `total_stock` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `total_stock` (`id`, `product_id`, `total_stock`) VALUES (1, 1, '2');
INSERT INTO `total_stock` (`id`, `product_id`, `total_stock`) VALUES (2, 2, '0');
INSERT INTO `total_stock` (`id`, `product_id`, `total_stock`) VALUES (3, 3, '2');
INSERT INTO `total_stock` (`id`, `product_id`, `total_stock`) VALUES (4, 4, '14');


#
# TABLE STRUCTURE FOR: user
#

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `fullname`, `username`, `email`, `password`, `address`, `phone`, `status`) VALUES (3, 'Xpertz Dev', 'admin', 'admin@email.com', 'caG2/l/l4PaoQ', 'Noor Corporation Abasin Market No.2 Mingora Swat', '03339471086', 1);


#
# TABLE STRUCTURE FOR: vendor
#

DROP TABLE IF EXISTS `vendor`;

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `vendor` (`id`, `fullname`, `email`, `address`, `phone`, `status`) VALUES (1, 'Cash Counter', 'no@email.com', 'Mingora', '3339471086', 1);


