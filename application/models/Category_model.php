<?php

class Category_model extends CI_Model
{

    
    public function all()
    {
         $this->db->order_by('id', 'ASC');
        
        $query = $this->db->get('category');
        return $query->result();
    }

    public function create()
    {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'name',
                'label' => 'Category Name',
                'rules' => 'required|is_unique[category.name]'
            ]
        ];
        
        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if($this->form_validation->run() != FALSE){
            $name    = $this->input->post('name',TRUE);
            
          
            // Insert user into DB
            $sql = $this->db->insert(
                'category',
                [
                    'name' => $name,
                
                    
                ]
                );
            if($sql){
                set_flash_alert('Category created successfully', 'success');
                return TRUE;
            }else{
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }
    
    public function update($id)
    {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
         $rules = [
            [
                'field' => 'name',
                'label' => 'Fullname',
                'rules' => 'required'
            ]
           
           
        ];
        
        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if($this->form_validation->run() != FALSE){
                 $name    = $this->input->post('name',TRUE);
          
            
            // Data for db
            $update['name'] = $name;
           
     
            
            // Update user into DB
            $this->db->where('id',$id);
            $sql = $this->db->update('category',$update);
            if($sql){
                set_flash_alert('Category updated successfully', 'success');
                return TRUE;
            }else{
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }
    
  
    
    public function find($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get('category');
        return $query->result();
    }
    
    public function delete($id)
    {
        $query = $this->db->delete('category',['id'=>$id]);
        if($query){
            set_flash_alert('Category deleted','success');
        }else{
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
    
    
    
}
