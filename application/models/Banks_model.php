<?php

class Banks_model extends CI_Model {

       public function all() {
        $this->db->order_by("id", "ASC");
        $q = $this->db->get('banks');
        return $q->result();
    }

    public function all_details() {
        $this->db->where('Year(date)', date("Y"));
        $this->db->order_by("date", "ASC");
        $q = $this->db->get('bank_details');
        return $q->result();
    }
	
    public function find($id) {
        $this->db->where('id', $id);
        $q = $this->db->get('banks');
        return $q->result();
    }

    public function find_bank($id) {
        $this->db->where('id', $id);
        $q = $this->db->get('bank_details');
        return $q->result();
    }

    public function find_name($id) {
        $this->db->where('id', $id);
        $q = $this->db->get('banks');
        return $q->result();
    }

    public function add_bank() {

        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'bank',
                'label' => 'Bank Name',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $exp = $this->input->post('bank', TRUE);


            // Insert user into DB
            $sql = $this->db->insert(
                    'banks', [
                'bank_name' => $exp
                    ]
            );
            if ($sql) {
                set_flash_alert('Bank Name created successfully', 'success');
                redirect("admin/banks");
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function edit_bank($id) {

        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'bank',
                'label' => 'Bank Name',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $exp = $this->input->post('bank', TRUE);

            $this->db->where('id', $id);

            $sql = $this->db->update(
                    'banks', [
                'bank_name' => $exp,
                    ]
            );

            if ($sql) {
                set_flash_alert('Bank Name Updated successfully', 'success');
                redirect("admin/banks");
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function add_payment() {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [

            [
                'field' => 'cheque',
                'label' => 'Cheque / Receipt No',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $bank = $this->input->post('bank', TRUE);

            $deposit = $this->input->post('deposit', TRUE);
            $withdraw = $this->input->post('withdraw', TRUE);
            $chq = $this->input->post('cheque', TRUE);
            $date = $this->input->post('date', TRUE);

            $newbank = $this->input->post('bank_new', TRUE);
          
            if (!empty($newbank)) {
                // Insert user into DB
                $this->db->insert(
                        'banks', [
                    'bank_name' => $newbank
                        ]
                );
                $bank_new = $this->db->insert_id();


                $sql = $this->db->insert(
                        'bank_details', [
                    'bank_id' => $bank_new,
                    'deposit' => $deposit,
                    'withdraw' => $withdraw,
                    'cheque' => $chq,
                    'date' => $date,
                    'shop_id' => '-',
                        ]
                );
            }
            else {

                $sql = $this->db->insert(
                        'bank_details', [
                    'bank_id' => $bank,
                    'deposit' => $deposit,
                    'withdraw' => $withdraw,
                    'cheque' => $chq,
                    'date' => $date,
                    'shop_id' => '-',
                        ]
                );
            }


            if ($sql) {
                set_flash_alert('Bank Details created successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function edit_stat($id) {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'bank',
                'label' => 'Bank Name',
                'rules' => 'required'
            ],
            [
                'field' => 'cheque',
                'label' => 'Cheque / Receipt No',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $bank = $this->input->post('bank', TRUE);

            $deposit = $this->input->post('deposit', TRUE);
            $withdraw = $this->input->post('withdraw', TRUE);
            $chq = $this->input->post('cheque', TRUE);
            $date = $this->input->post('date', TRUE);

            $this->db->where("id", $id);
            $sql = $this->db->update(
                    'bank_details', [
                'bank_id' => $bank,
                'deposit' => $deposit,
                'withdraw' => $withdraw,
                'cheque' => $chq,
                'date' => $date,
                'shop_id' => '-',
                    ]
            );



            if ($sql) {
                set_flash_alert('Bank Details Updated successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function transfer() {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'from',
                'label' => 'From Bank Name',
                'rules' => 'required'
            ],
            [
                'field' => 'to',
                'label' => 'To Bank Name',
                'rules' => 'required'
            ],
            [
                'field' => 'cheque',
                'label' => 'Cheque / Receipt No',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $from = $this->input->post('from', TRUE);
            $to = $this->input->post('to', TRUE);

            $deposit = $this->input->post('deposit', TRUE);
            $chq = $this->input->post('cheque', TRUE);
            $date = $this->input->post('date', TRUE);


$sql = $this->db->insert(
                    'bank_details', [
                'bank_id' => $from,
                'deposit' => 0,
                'withdraw' => $deposit,
                'cheque' => $chq,
                'date' => $date,
                'shop_id' => '-',
                    ]
            );

            $sql = $this->db->insert(
                    'bank_details', [
                'bank_id' => $to,
                'deposit' => $deposit,
                'withdraw' => 0,
                'cheque' => $chq,
                'date' => $date,
                'shop_id' => '-',
                    ]
            );

            


            if ($sql) {
                set_flash_alert('Payment Transfer successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

}
