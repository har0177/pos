<?php

class Book_model extends CI_Model {

    public function vendor() {
        $query = $this->db->get('book_vendor');
        return $query->result();
    }

    public function all() {
        $query = $this->db->get('book');
        return $query->result();
    }

    public function datewise() {

        $this->db->order_by('id', 'ASC');

        $query = $this->db->get('book_detail');
        return $query->result();
    }

    public function create() {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'shop',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'total',
                'label' => 'Total Amount',
                'rules' => 'required'
            ],
            [
                'field' => 'credit',
                'label' => 'Credit Amount',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $shop = $this->input->post('shop', TRUE);

            $total = $this->input->post('total', TRUE);
            $credit = $this->input->post('credit', TRUE);


            $debit = $total - $credit;
            $date = date("Y-m-d");

            $array = array(
                'shop_id' => $shop);
            $this->db->where($array);
            $q = $this->db->get('book');
            if ($q->num_rows() > 0) {
                $res = $q->result_array();
                $totalamount = $res[0]['total'];
                $totalcredit = $res[0]['credit'];
                $totaldebit = $res[0]['debit'];



                $tamount = $total + $totalamount;
                $tcredit = $credit + $totalcredit;
                $tdebit = $debit + $totaldebit;


                $this->db->where('shop_id', $shop);
                // Insert user into DB
                $sql = $this->db->update(
                        'book', [

                    'total' => $tamount,
                    'credit' => $tcredit,
                    'debit' => $tdebit,
                        ]
                );

                $this->db->insert(
                        'book_detail', [
                    'credit' => round($credit, 2),
                    'debit' => round($debit, 2),
                    'discount' => 0,
                    'bilty' => 0,
                    'date' => $date,
                    'shop_id' => $shop,
                        ]
                );
            }
            else {
                $this->db->insert(
                        'book_detail', [
                    'credit' => round($credit, 2),
                    'debit' => round($debit, 2),
                    'discount' => 0,
                    'bilty' => 0,
                    'date' => $date,
                    'shop_id' => $shop,
                        ]
                );
                // Insert user into DB
                $sql = $this->db->insert(
                        'book', [
                    'shop_id' => $shop,
                    'total' => $total,
                    'credit' => $credit,
                    'debit' => $debit,
                        ]
                );
            }

            if ($sql) {
                set_flash_alert('Book Amount created successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function create_vendor() {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'shop',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'total',
                'label' => 'Total Amount',
                'rules' => 'required'
            ],
            [
                'field' => 'credit',
                'label' => 'Credit Amount',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $date = date("Y-m-d");
            $shop = $this->input->post('shop', TRUE);

            $total = $this->input->post('total', TRUE);
            $credit = $this->input->post('credit', TRUE);


            $debit = $total - $credit;



            $array = array(
                'shop_id' => $shop);
            $this->db->where($array);
            $q = $this->db->get('book_vendor');
            if ($q->num_rows() > 0) {
                $res = $q->result_array();
                $totalamount = $res[0]['total'];
                $totalcredit = $res[0]['credit'];
                $totaldebit = $res[0]['debit'];



                $tamount = $total + $totalamount;
                $tcredit = $credit + $totalcredit;
                $tdebit = $debit + $totaldebit;


                $this->db->where('shop_id', $shop);
                // Insert user into DB
                $sql = $this->db->update(
                        'book_vendor', [

                    'total' => $tamount,
                    'credit' => $tcredit,
                    'debit' => $tdebit,
                        ]
                );

                $sql = $this->db->insert(
                        'book_detail_vendor', [

                    'credit' => $credit,
                    'debit' => $debit,
                    'date' => $date,
                    'shop_id' => $shop,
                        ]
                );
            }
            else {
                // Insert user into DB
                $sql = $this->db->insert(
                        'book_vendor', [
                    'shop_id' => $shop,
                    'total' => $total,
                    'credit' => $credit,
                    'debit' => $debit,
                        ]
                );

                $sql = $this->db->insert(
                        'book_detail_vendor', [

                    'credit' => $credit,
                    'debit' => $debit,
                    'date' => $date,
                    'shop_id' => $shop,
                        ]
                );
            }

            if ($sql) {
                set_flash_alert('Book Amount created successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function update($id) {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'shop',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'newcredit',
                'label' => 'New Credit Amount',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
          $shop = $this->input->post('shop', TRUE);
            $debit = $this->input->post('debithidden', TRUE);
            $credit = $this->input->post('credithidden', TRUE);
            $new = $this->input->post('newcredit', TRUE);
            $total = $this->input->post('totalhidden', TRUE);
            $discount = $this->input->post('discount', TRUE);
            $disc = $this->input->post('dischidden', TRUE);
            $bilty = $this->input->post('bilty', TRUE);
            $biltyhidden = $this->input->post('biltyhidden', TRUE);
            $bank = $this->input->post('bank', TRUE);

            $cheque = $this->input->post('cheque', TRUE);
            $date = $this->input->post('date', TRUE);

            $d = $debit - $new - $discount + $bilty;
            $c = $credit + $new;
            $dis = $disc + $discount;
            $bil = $bilty + $biltyhidden;
            if ($new < 0) {
                $wd = str_replace('-', '', $new);
                $t = $total;
                AdminLTE::bank_entry($bank, 0, $wd, $cheque, $date, 'cs_' . $shop, $d, $debit);
            }
            else {

                $t = $total + $new;
                AdminLTE::bank_entry($bank, $new, 0, $cheque, $date, 'cs_' . $shop, $d, $debit);
            }

            // Data for db
            $update['debit'] = round($d, 2);
            $update['credit'] = round($c, 2);
            $update['discount'] = round($dis, 2);
            $update['bilty'] = round($bil, 2);
            // Update user into DB
            $this->db->where('id', $id);
            $sql = $this->db->update('book', $update);

            $this->db->insert(
                    'book_detail', [
                'credit' => round($new, 2),
                'debit' => round($d, 2),
                'discount' => round($discount, 2),
                'bilty' => round($bilty, 2),
                'date' => $date,
                'shop_id' => $shop,
                    ]
            );


            if ($sql) {
                set_flash_alert('Book Amount updated successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function edit_cheque($id) {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'cheque',
                'label' => 'Cheque No',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $cheque = $this->input->post('cheque', TRUE);
            $cus_id = $this->input->post('customer', TRUE);
            $update['cheque'] = $cheque;
            // Update user into DB
            $this->db->where('id', $id);
            $sql = $this->db->update('bank_details', $update);
            if ($sql) {
                set_flash_alert('Cheque updated successfully', 'success');
                redirect("admin/book/invoices/" . $cus_id);
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function update_vendor($id) {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'shop',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'newcredit',
                'label' => 'New Credit Amount',
                'rules' => 'required'
            ],
            [
                'field' => 'bank',
                'label' => 'Bank Name',
                'rules' => 'required'
            ],
            [
                'field' => 'cheque',
                'label' => 'Cheque / Receipt No',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $shop = $this->input->post('shop', TRUE);
            $debit = $this->input->post('debithidden', TRUE);
            $credit = $this->input->post('credithidden', TRUE);
            $new = $this->input->post('newcredit', TRUE);
            $bank = $this->input->post('bank', TRUE);
            $cheque = $this->input->post('cheque', TRUE);
            $date = $this->input->post('date', TRUE);

            $d = $debit - $new;
            $c = $credit + $new;
            if ($new < 0) {
                $wd = str_replace('-', '', $new);

                AdminLTE::bank_entry($bank, $wd, 0, $cheque, $date, 'vn_' . $shop, $d, $debit);
            }
            else {

                AdminLTE::bank_entry($bank, 0, $new, $cheque, $date, 'vn_' . $shop, $d, $debit);
            }

            // Data for db
            $update['debit'] = round($d, 2);
            $update['credit'] = round($c, 2);
            // Update user into DB
            $this->db->where('id', $id);
            $sql = $this->db->update('book_vendor', $update);


            $this->db->insert(
                    'book_detail_vendor', [

                'credit' => round($new, 2),
                'debit' => round($d, 2),
                'date' => $date,
                'shop_id' => $shop,
                    ]
            );

            if ($sql) {
                set_flash_alert('Book Amount updated successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function find($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('book');
        return $query->result();
    }

    public function find_details($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('bank_details');
        return $query->result();
    }

    public function find_vendor($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('book_vendor');
        return $query->result();
    }

    public function delete($id) {
        $query = $this->db->delete('book', ['id' => $id]);
        if ($query) {
            set_flash_alert('Book Amount deleted', 'success');
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }

    public function activate($id) {
        $query = $this->db->update('book', ['deleted' => '0'], ['id' => $id]);
        if ($query) {
            set_flash_alert('Book Amount Activated', 'success');
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }

    public function deactivate($id) {
        $query = $this->db->update('book', ['deleted' => '1'], ['id' => $id]);
        if ($query) {
            set_flash_alert('Book Amount Deactivated', 'success');
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }

}
