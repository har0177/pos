<?php

class Stock_model extends CI_Model {

    function getReport() {

        $from = $this->input->post('from');
        $to = $this->input->post('to');

        if (!empty($from) || !empty($to)) {
            $q = $this->db->query('Select DISTINCT(product_id) from stock where DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '" order by product_id ASC');
            if ($q->num_rows() > 0) {
                return $q->result();
            }
            else {

                set_flash_alert("No data Found!", 'danger');
            }
        }
    }

    public function add_stock($stoc, $shop, $cus_new, $cus_new_mobile, $date) {
        $inv = "";
        if (isset($cus_new) && !empty($cus_new)) {

            $sql = $this->db->insert(
                    'vendor', [
                'fullname' => $cus_new,
                'phone' => $cus_new_mobile,
                'status' => 1,
                    ]
            );

            $vendor_new = $this->db->insert_id();
            $this->db->insert(
                    'invoice', [
                'shop_id' => $vendor_new,
                'date' => $date
                    ]
            );

            $inv = $this->db->insert_id();
            $this->bookadd($inv, $vendor_new);
        }
        else {
            $this->db->insert(
                    'invoice', [
                'shop_id' => $shop,
                'date' => $date
                    ]
            );
            $inv = $this->db->insert_id();
            $this->bookadd($inv, $shop);
        }



        for ($x = 0;
                $x < count($stoc);
                $x++) {
            if ($stoc[$x]['pprice'] > AdminLTE::pprice($stoc[$x]['id']) || $stoc[$x]['sprice'] > AdminLTE::sprice($stoc[$x]['id']) || $stoc[$x]['pprice'] < AdminLTE::pprice($stoc[$x]['id']) || $stoc[$x]['sprice'] < AdminLTE::sprice($stoc[$x]['id'])) {
                $this->db->where('id', $stoc[$x]['id']);
                $sql = $this->db->update(
                        'products', [
                    'pprice' => $stoc[$x]['pprice'],
                    'sprice' => $stoc[$x]['sprice'],
                        ]
                );
            }

            $array = array(
                'product_id' => $stoc[$x]['id']);
            $this->db->where($array);
            $q = $this->db->get('total_stock');
            if ($q->num_rows() > 0) {
                $res = $q->result_array();
                $pre_stock = $res[0]['total_stock'];
                $total = $pre_stock + $stoc[$x]['stock'];

                $this->db->where('product_id', $stoc[$x]['id']);
                $sql = $this->db->update(
                        'total_stock', [
                    'total_stock' => $total,
                        ]
                );
            }
            else {

                $sql = $this->db->insert(
                        'total_stock', [
                    'product_id' => $stoc[$x]['id'],
                    'total_stock' => $stoc[$x]['stock'],
                        ]
                );
            }


            $data[] = array(
                'product_id' => $stoc[$x]['id'],
                'stock' => $stoc[$x]['stock'],
                'date' => $stoc[$x]['date'],
                'price' => $stoc[$x]['pprice'],
                'biltyno' => $stoc[$x]['biltyno'],
                'bill' => $stoc[$x]['bill'],
                'inv_id' => $inv,
            );
        }




        try {
            for ($x = 0;
                    $x < count($stoc);
                    $x++) {
                $this->db->insert("stock", $data[$x]);
            }

            return $inv;
        }
        catch (Exception $e) {
            return "failed";
        }
    }

    public function all() {
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('stock');
        return $query->result();
    }

    public function total() {
        $query = $this->db->get('total_stock');
        return $query->result();
    }

    public function create() {
// Load form validation library
        $this->load->library('form_validation');
// define rules
        $rules = [


            [
                'field' => 'date',
                'label' => 'Date',
                'rules' => 'required'
            ]
        ];

// Set rules
        $this->form_validation->set_rules($rules);
// Check form
        if ($this->form_validation->run() != FALSE) {
            $products = $this->input->post('product', TRUE);
            $stock = $this->input->post('stock', TRUE);
            $bill = $this->input->post('bill', TRUE);
            $biltyno = $this->input->post('biltyno', TRUE);
            $date = $this->input->post('date', TRUE);

            $pprice = $this->input->post('pprice', TRUE);
            $sprice = $this->input->post('sprice', TRUE);

            $cus_new = $this->input->post('cus_new', TRUE);
            $cus_new_mobile = $this->input->post('cus_new_mobile', TRUE);

            $vendor = $this->input->post('vendor', TRUE);

            if (empty($products)) {
                set_flash_alert('Please Select Products Using Check Box', 'danger');
                redirect('admin/stock/add');
                return FALSE;
            }

            if (isset($cus_new) && !empty($cus_new)) {

                $sql = $this->db->insert(
                        'vendor', [
                    'fullname' => $cus_new,
                    'phone' => $cus_new_mobile,
                    'status' => 1,
                        ]
                );

                $vendor_new = $this->db->insert_id();
                $this->db->insert(
                        'invoice', [
                    'shop_id' => $vendor_new,
                    'date' => $date
                        ]
                );


                $inv = $this->db->insert_id();

                foreach ($products as
                        $p =>
                        $product) {
                    if (!empty($pprice)) {
                        if ($pprice[$p] > AdminLTE::pprice($product) || $sprice[$p] > AdminLTE::sprice($product) || $pprice[$p] < AdminLTE::pprice($product) || $sprice[$p] < AdminLTE::sprice($product)) {
                            $this->db->where('id', $product);
                            $sql = $this->db->update(
                                    'products', [
                                'pprice' => $pprice[$p],
                                'sprice' => $sprice[$p],
                                    ]
                            );
                        }
                    }


                    $array = array(
                        'product_id' => $product);
                    $this->db->where($array);
                    $q = $this->db->get('total_stock');
                    if ($q->num_rows() > 0) {
                        $res = $q->result_array();
                        $pre_stock[$p] = $res[0]['total_stock'];
                        $total[$p] = $pre_stock[$p] + $stock[$p];

// Insert user into DB
                        $sql = $this->db->insert(
                                'stock', [
                            'product_id' => $product,
                            'stock' => $stock[$p],
                            'date' => $date,
                            'inv_id' => $inv,
                            'price' => AdminLTE::pprice($product),
                            'bill' => $bill,
                            'biltyno' => $biltyno
                                ]
                        );

                        $this->db->where('product_id', $product);
                        $sql = $this->db->update(
                                'total_stock', [
                            'total_stock' => $total[$p],
                                ]
                        );
                    }
                    else {

                        $sql = $this->db->insert(
                                'stock', [
                            'product_id' => $product,
                            'stock' => $stock[$p],
                            'date' => $date,
                            'inv_id' => $inv,
                            'price' => AdminLTE::pprice($product),
                            'bill' => $bill,
                            'biltyno' => $biltyno
                                ]
                        );
                        $sql = $this->db->insert(
                                'total_stock', [
                            'product_id' => $product,
                            'total_stock' => $stock[$p],
                                ]
                        );
                    }
                }
            }
            else {
                if (empty($vendor)) {
                    set_flash_alert('Please Select Vendor', 'danger');
                    redirect('admin/stock/add');
                    return FALSE;
                }
                $this->db->insert(
                        'invoice', [
                    'shop_id' => $vendor,
                    'date' => $date
                        ]
                );


                $inv = $this->db->insert_id();

                foreach ($products as
                        $p =>
                        $product) {
                    if (!empty($pprice)) {
                        if ($pprice[$p] > AdminLTE::pprice($product) || $sprice[$p] > AdminLTE::sprice($product) || $pprice[$p] < AdminLTE::pprice($product) || $sprice[$p] < AdminLTE::sprice($product)) {
                            $this->db->where('id', $product);
                            $sql = $this->db->update(
                                    'products', [
                                'pprice' => $pprice[$p],
                                'sprice' => $sprice[$p],
                                    ]
                            );
                        }
                    }
                    $array = array(
                        'product_id' => $product);
                    $this->db->where($array);
                    $q = $this->db->get('total_stock');
                    if ($q->num_rows() > 0) {
                        $res = $q->result_array();
                        $pre_stock[$p] = $res[0]['total_stock'];
                        $total[$p] = $pre_stock[$p] + $stock[$p];

// Insert user into DB
                        $sql = $this->db->insert(
                                'stock', [
                            'product_id' => $product,
                            'stock' => $stock[$p],
                            'date' => $date,
                            'inv_id' => $inv,
                            'price' => AdminLTE::pprice($product),
                            'bill' => $bill,
                            'biltyno' => $biltyno
                                ]
                        );

                        $this->db->where('product_id', $product);
                        $sql = $this->db->update(
                                'total_stock', [
                            'total_stock' => $total[$p],
                                ]
                        );
                    }
                    else {
                        $sql = $this->db->insert(
                                'stock', [
                            'product_id' => $product,
                            'stock' => $stock[$p],
                            'date' => $date,
                            'inv_id' => $inv,
                            'price' => AdminLTE::pprice($product),
                            'bill' => $bill,
                            'biltyno' => $biltyno
                                ]
                        );
                        $sql = $this->db->insert(
                                'total_stock', [
                            'product_id' => $product,
                            'total_stock' => $stock[$p],
                                ]
                        );
                    }
                }
            }


            if ($sql) {
                set_flash_alert('Stock created successfully', 'success');
                redirect('admin/stock/print_invoice/' . $inv);

                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function all_invoices($id) {
        $array = array(
            'inv_id' => $id);
        $this->db->where($array);
        $query = $this->db->get('invoices_vendor');
        return $query->result();
    }

    public function invoice_print($id) {
        $this->db->where('inv_id', $id);
        $query = $this->db->get('stock');
        return $query->result();
    }

    public function update($id) {
// Load form validation library
        $this->load->library('form_validation');
// define rules
        $rules = [
            [
                'field' => 'product',
                'label' => 'Stock Name',
                'rules' => 'required'
            ],
            [
                'field' => 'stock',
                'label' => 'Stock',
                'rules' => 'required'
            ],
            [
                'field' => 'date',
                'label' => 'Date',
                'rules' => 'required'
            ]
        ];

// Set rules
        $this->form_validation->set_rules($rules);
// Check form
        if ($this->form_validation->run() != FALSE) {
            $product = $this->input->post('product', TRUE);
            $stock = $this->input->post('stock', TRUE);
            $date = $this->input->post('date', TRUE);
            $pprice = $this->input->post('pprice', TRUE);
            $sprice = $this->input->post('sprice', TRUE);


// Data for db
            $update['product_id'] = $product;
            $update['stock'] = $stock;
            $update['date'] = $date;
            $update['pprice'] = $pprice;
            $update['sprice'] = $sprice;



// Update user into DB
            $this->db->where('id', $id);
            $sql = $this->db->update('stock', $update);
            if ($sql) {
                set_flash_alert('Stock updated successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function find($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('stock');
        return $query->result();
    }

    public function delete($id) {
        $array = array(
            'id' => $id);
        $this->db->where($array);
        $q = $this->db->get('stock');
        $res = $q->result_array();
        $sale = $res[0]['stock'];
        $product = $res[0]['product_id'];

        $array1 = array(
            'product_id' => $product);
        $this->db->where($array1);
        $qg = $this->db->get('total_stock');
        $ress = $qg->result_array();
        $pre = $ress[0]['total_stock'];
        $total = $pre - $sale;

        $this->db->where('product_id', $product);
        $sql = $this->db->update(
                'total_stock', [
            'total_stock' => $total,
                ]
        );



        $query = $this->db->delete('stock', ['id' => $id]);
        if ($query) {
            set_flash_alert('Stock deleted', 'success');
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }

    public function bookadd($id, $shop) {

        $discount = $this->input->post('discount', TRUE);
        $bilty = $this->input->post('bilty_discount', TRUE);
        $bill = $this->input->post('bill', TRUE);
        $total = $this->input->post('total', TRUE);
        $old = $this->input->post('old', TRUE);
        $date = $this->input->post('date', TRUE);
        $paid = $this->input->post('paid', TRUE);
        $bank_name = $this->input->post('banking');
        $bank = $this->input->post('bank');
        $comments = $this->input->post('comments');
        $dis_per = $this->input->post('dis_per');
        if (empty($paid)) {
            $paid = 0;
        }
        $type_payment = "";

        $debit = ($total - $discount) - $paid;
        $net = ($total - $discount) - $paid + $old;

        if ($bank_name == " ") {
            $type_payment = " ";
        }
        else {
            $type_payment = $bank_name . "-" . $bank;

            AdminLTE::bank_entry($bank_name, 0, $paid, $bank, $date, 'vn_' . $shop);
        }


        $dis = "";
        if (!empty($dis_per)) {

            $dis = $dis_per . " %";
        }

        $array = array(
            'inv_id' => $id);
        $this->db->where($array);
        $q = $this->db->get('invoices_vendor');
        if ($q->num_rows() === 0) {


            $array = array(
                'shop_id' => $shop);
            $this->db->where($array);
            $q = $this->db->get('book_vendor');
            if ($q->num_rows() > 0) {
                $res = $q->result_array();
                $totalamount = $res[0]['total'];
                $totalcredit = $res[0]['credit'];
                $totaldebit = $res[0]['debit'];
                $totaldiscount = $res[0]['discount'];
                $totaldiscountbilty = $res[0]['bilty'];

                $tamount = $total + $totalamount;
                $tdisc = $discount + $totaldiscount;
                $tdiscbilty = $bilty + $totaldiscountbilty;
                $tcredit = $paid + $totalcredit;
                $tdebit = $debit + $totaldebit;


                $this->db->where('shop_id', $shop);
                // Insert user into DB
                $sql = $this->db->update(
                        'book_vendor', [

                    'total' => round($tamount, 2),
                    'credit' => round($tcredit, 2),
                    'debit' => round($tdebit, 2),
                    'discount' => round($tdisc, 2),
                    'bilty' => round($tdiscbilty, 2),
                    'bill' => $bill
                        ]
                );

                $sql = $this->db->insert(
                        'invoices_vendor', [

                    'inv_id' => $id,
                    'total' => $total,
                    'paid' => $paid,
                    'old' => $old,
                    'net' => $net,
                    'discount' => $discount,
                    'bill' => $bill
                    ,
                    'bilty' => $bilty,
                    'date' => $date,
                    'type_payment' => $type_payment,
                    'comments' => $comments,
                    'dis_per' => $dis,
                ]);

                if ($bilty > 0) {
                    $this->db->insert(
                            'expenses', [
                        'rec_no' => "Invoice No. " . $id,
                        'amount' => $bilty,
                        'exp_id' => 1,
                        'comments' => "Invoice No. " . $id . " and Bill No " . $bill . " Bilty Payment",
                        'date' => $date,
                        'bank' => 1
                            ]
                    );
                    $this->db->insert(
                            'bank_details', [
                        'bank_id' => 1,
                        'deposit' => 0,
                        'withdraw' => $bilty,
                        'cheque' => "Invoice No. " . $id,
                        'date' => $date,
                        'shop_id' => '-',
                            ]
                    );
                }
            }
            else {

                $sql = $this->db->insert(
                        'book_detail_vendor', [

                    'credit' => round($paid, 2),
                    'debit' => round($debit, 2),
                    'date' => $date,
                    'shop_id' => $shop,
                    'discount' => round($discount, 2),
                    'bilty' => round($bilty, 2),
                    'bill' => $bill
                        ]
                );

                // Insert user into DB
                $sql = $this->db->insert(
                        'book_vendor', [
                    'shop_id' => $shop,
                    'total' => $total,
                    'credit' => $paid,
                    'debit' => $debit,
                    'discount' => $discount
                    ,
                    'bill' => $bill,
                    'bilty' => $bilty,
                        ]
                );
                $sql = $this->db->insert(
                        'invoices_vendor', [

                    'inv_id' => $id,
                    'total' => $total,
                    'paid' => $paid,
                    'old' => $old,
                    'net' => $net,
                    'discount' => $discount,
                    'bill' => $bill,
                    'bilty' => $bilty,
                    'date' => $date,
                    'type_payment' => $type_payment,
                    'comments' => $comments,
                    'dis_per' => $dis,
                ]);

                if ($bilty > 0) {
                    $this->db->insert(
                            'expenses', [
                        'rec_no' => 123,
                        'amount' => $bilty,
                        'exp_id' => 1,
                        'comments' => "Invoice No. " . $id . " and Bill No " . $bill . " Bilty Payment",
                        'date' => $date,
                        'bank' => 1
                            ]
                    );
                    $this->db->insert(
                            'bank_details', [
                        'bank_id' => 1,
                        'deposit' => 0,
                        'withdraw' => $bilty,
                        'cheque' => "Invoice No. " . $id,
                        'date' => $date,
                        'shop_id' => '-',
                            ]
                    );
                }
            }
        }
    }

}
