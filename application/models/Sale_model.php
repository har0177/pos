<?php

class Sale_model extends CI_Model {

    function getReport_cus() {

        $from = $this->input->post('from');
        $to = $this->input->post('to');

        if (!empty($from) || !empty($to)) {
            $q = $this->db->query('Select DISTINCT(inv_id) from sale where DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '" order by inv_id ASC');
            if ($q->num_rows() > 0) {
                return $q->result();
            }
            else {

                set_flash_alert("No data Found!", 'danger');
            }
        }
    }

    function getReport() {

        $from = $this->input->post('from');
        $to = $this->input->post('to');

        if (!empty($from) || !empty($to)) {
            $q = $this->db->query('Select DISTINCT(product_id) from sale where DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '" order by product_id ASC');
            if ($q->num_rows() > 0) {
                return $q->result();
            }
            else {

                set_flash_alert("No data Found!", 'danger');
            }
        }
    }

    public function add_sale($stoc, $shop, $cus_new, $cus_new_mobile, $date) {
        $inv = "";
        if (isset($cus_new) && !empty($cus_new)) {

            $sql = $this->db->insert(
                    'customers', [
                'fullname' => $cus_new,
                'phone' => $cus_new_mobile,
                'status' => 1,
                    ]
            );

            $vendor_new = $this->db->insert_id();
            $this->db->insert(
                    'invoice', [
                'shop_id' => $vendor_new,
                'date' => $date
                    ]
            );
            $inv = $this->db->insert_id();
            $this->bookadd($inv, $vendor_new);
        }
        else {
            $this->db->insert(
                    'invoice', [
                'shop_id' => $shop,
                'date' => $date
                    ]
            );
            $inv = $this->db->insert_id();
            $this->bookadd($inv, $shop);
        }



        for ($x = 0;
                $x < count($stoc);
                $x++) {

            $array = array(
                'product_id' => $stoc[$x]['id']);
            $this->db->where($array);
            $q = $this->db->get('total_stock');
            if ($q->num_rows() > 0) {
                $res = $q->result_array();
                $pre_stock = $res[0]['total_stock'];

                $total = $pre_stock - $stoc[$x]['stock'];

                $this->db->where('product_id', $stoc[$x]['id']);
                $sql = $this->db->update(
                        'total_stock', [
                    'total_stock' => $total,
                        ]
                );
            }else{
				   $sql = $this->db->insert(
                        'total_stock', [
                    'product_id' => $stoc[$x]['id'],
                    'total_stock' => - $stoc[$x]['stock'],
                        ]
                );
			}

            $data[] = array(
                'product_id' => $stoc[$x]['id'],
                'sale' => $stoc[$x]['stock'],
                'date' => $stoc[$x]['date'],
                'price' => $stoc[$x]['sprice'],
                'sprice' => AdminLTE::pprice($stoc[$x]['id']),
                'billno' => $stoc[$x]['billno'],
                'inv_id' => $inv,
            );
        }




        try {
            for ($x = 0;
                    $x < count($stoc);
                    $x++) {
                $this->db->insert("sale", $data[$x]);
            }

            return $inv;
        }
        catch (Exception $e) {
            return "failed";
        }
    }

    public function update_sale($stoc) {


        $inv = "";
        try {
            $date = "";
            $billno = "";
            for ($x = 0;
                    $x < count($stoc);
                    $x++) {
                $inv = $stoc[$x]['inv'];
                $product_id = $stoc[$x]['pid'];

                if (empty($stoc[$x]['stock']) || $stoc[$x]['stock'] == 0) {
                    continue;
                }
                else {
                    $array = array(
                        'product_id' => $product_id,
                        'inv_id' => $stoc[$x]['inv']);
                    $this->db->where($array);
                    $q = $this->db->get('sale');
                    if ($q->num_rows() > 0) {

                        $res = $q->result_array();
                        $pre_sale = $res[0]['sale'];
                        $date = $res[0]['date'];
                        $billno = $res[0]['billno'];
                        $array1 = array(
                            'product_id' => $product_id,
                            'inv_id' => $stoc[$x]['inv']);
                        $this->db->where($array1);
                        $sql = $this->db->update(
                                'sale', [
                            'sale' => $stoc[$x]['stock'],
                                ]
                        );


                        $array2 = array(
                            'product_id' => $product_id);
                        $this->db->where($array2);
                        $q = $this->db->get('total_stock');
                        if ($q->num_rows() > 0) {
                            $res = $q->result_array();
                            $pre_stock = $res[0]['total_stock'];
                            $total = 0;
                            if ($stoc[$x]['stock'] > $pre_sale) {
                                $newstock = $stoc[$x]['stock'] - $pre_sale;
                                $total = $pre_stock - $newstock;
                            }
                            else if ($stoc[$x]['stock'] == $pre_sale) {
                                $total = $pre_stock;
                            }
                            else {
                                $newstock = $pre_sale - $stoc[$x]['stock'];
                                $total = $pre_stock + $newstock;
                            }
                            $this->db->where('product_id', $res[0]['product_id']);
                            $sql = $this->db->update(
                                    'total_stock', [
                                'total_stock' => $total,
                                    ]
                            );
                        }

                        $update['product_id'] = $stoc[$x]['pid'];
                        $update['sale'] = $stoc[$x]['stock'];
                        $update['price'] = $stoc[$x]['sprice'];
                        $update['sprice'] = AdminLTE::pprice($stoc[$x]['pid']);



// Update user into DB
                        $this->db->where('id', $stoc[$x]['inv']);
                        $sql = $this->db->update('sale', $update);
                    }
                    else {

                        $array = array(
                            'product_id' => $stoc[$x]['pid']);
                        $this->db->where($array);
                        $q = $this->db->get('total_stock');
                        if ($q->num_rows() > 0) {
                            $res = $q->result_array();
                            $pre_stock = $res[0]['total_stock'];
                            $total = $pre_stock - $stoc[$x]['stock'];

                            $this->db->where('product_id', $res[0]['product_id']);
                            $sql = $this->db->update(
                                    'total_stock', [
                                'total_stock' => $total,
                                    ]
                            );
                        }

                        if (empty($date)) {
                            $date = date("Y-m-d");
                        }



                        $this->db->insert(
                                'sale', [
                            'product_id' => $stoc[$x]['pid'],
                            'sale' => $stoc[$x]['stock'],
                            'date' => $date,
                            'price' => $stoc[$x]['sprice'],
                            'sprice' => AdminLTE::pprice($stoc[$x]['pid']),
                            'billno' => $billno,
                            'inv_id' => $inv,
                                ]
                        );
                    }
                }
            }

            $shop = AdminLTE::customers_name($inv);
            $discount = $this->input->post('discount', TRUE);
            $bilty = $this->input->post('bilty_discount', TRUE);
            $total = $this->input->post('total', TRUE);
            $old = $this->input->post('old', TRUE);
            $paid = $this->input->post('paid', TRUE);

            $net = ($total - $discount + $bilty) - $paid + $old;

            $newtotal = 0;
            $newcredit = 0;
            $newdebit = 0;
            $newdiscount = 0;

            $newbilty = 0;

            $array22 = array(
                'inv_id' => $inv);
            $this->db->where($array22);
            $q22 = $this->db->get('invoices');
            if ($q22->num_rows() > 0) {
                $gh = $q22->result_array();
                $invtotal = $gh[0]['total'];
                $invcredit = $gh[0]['paid'];
                $invdebit = $gh[0]['net'];
                $invdiscount = $gh[0]['discount'];
                $invdiscountbilty = $gh[0]['bilty'];
                $array = array(
                    'shop_id' => $shop);
                $this->db->where($array);
                $q = $this->db->get('book');
                if ($q->num_rows() > 0) {
                    $res = $q->result_array();
                    $totalamount = $res[0]['total'];
                    $totalcredit = $res[0]['credit'];
                    $totaldebit = $res[0]['debit'];
                    $totaldiscount = $res[0]['discount'];
                    $totaldiscountbilty = $res[0]['bilty'];

                    if ($total > $invtotal) {
                        $newtotal = $total - $invtotal;
                        $tamount = $newtotal + $totalamount;
                    }
                    else if ($total < $invtotal) {

                        $newtotal = $invtotal - $total;
                        $tamount = $totalamount - $newtotal;
                    }
                    else {
                        $tamount = $totalamount;
                    }

                    if ($net > $invdebit) {
                        $newdebit = $net - $invdebit;

                        $tdebit = $newdebit + $totaldebit;
                    }
                    else if ($net < $invdebit) {
                        $newdebit = $invdebit - $net;

                        $tdebit = $totaldebit - $newdebit;
                    }
                    else {
                        $tdebit = $totaldebit;
                    }

                    if ($paid > $invcredit) {
                        $newcredit = $paid - $invcredit;
                        $tcredit = $newcredit + $totalcredit;
                        AdminLTE::bank_entry(1, $newcredit, 0, "update Invoice" . $inv, $date, "cs_" . $shop);
                    }
                    else if ($paid < $invcredit) {
                        $newcredit = $invcredit - $paid;
                        $tcredit = $totalcredit - $newcredit;
                        AdminLTE::bank_entry(1, 0, $newcredit, "update Invoice" . $inv, $date, "cs_" . $shop);
                    }
                    else {
                        $tcredit = $totalcredit;
                    }

                    if ($discount > $invdiscount) {
                        $newdiscount = $discount - $invdiscount;
                        $tdisc = $newdiscount + $totaldiscount;
                    }
                    else if ($discount < $invdiscount) {
                        $newdiscount = $invdiscount - $discount;
                        $tdisc = $totaldiscount - $newdiscount;
                    }
                    else {
                        $tdisc = $totaldiscount;
                    }

                    if ($bilty > $invdiscountbilty) {
                        $newbilty = $bilty - $invdiscountbilty;
                        $tdiscbilty = $newbilty + $totaldiscountbilty;
                    }
                    else if ($bilty < $invdiscountbilty) {
                        $newbilty = $invdiscountbilty - $bilty;
                        $tdiscbilty = $totaldiscountbilty - $newbilty;
                    }
                    else {
                        $tdiscbilty = $totaldiscountbilty;
                    }



                    $this->db->where('shop_id', $shop);
                    // Insert user into DB
                    $sql = $this->db->update(
                            'book', [
                        'total' => $tamount,
                        'credit' => $tcredit,
                        'debit' => $tdebit,
                        'discount' => $tdisc,
                        'bilty' => $tdiscbilty,
                            ]
                    );

                  

                    $this->db->where('inv_id', $inv);
                    $sql = $this->db->update(
                            'invoices', [
                        'total' => $total,
                        'paid' => $paid,
                        'old' => $old,
                        'net' => $net,
                        'discount' => $discount,
                        'bilty' => $bilty,
                    ]);
                }
            }

            return $inv;
        }
        catch (Exception $e) {
            return "failed";
        }
    }

    public function products($cur = "") {

        $query = $this->db->get('products');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->product </option>";
        }
        return $data;
    }

    public function customerss($cur = "") {

        $query = $this->db->get('customers');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->fullname </option>";
        }
        return $data;
    }

    public function total() {
        $query = $this->db->query('Select Distinct(product_id) from sale');
        return $query->result();
    }

    public function total_shop() {
        $query = $this->db->query('Select Distinct(inv_id) from sale');
        return $query->result();
    }

    public function all() {
        $q = $this->db->query('Select DISTINCT(inv_id) from sale order by inv_id ASC');
        return $q->result();
    }

    public function all_invoice() {
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('invoices');
        return $query->result();
    }

    public function all_invoice_vendor() {
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('invoices_vendor');
        return $query->result();
    }

    public function all_invoices($id) {
        $array = array(
            'inv_id' => $id);
        $this->db->where($array);
        $query = $this->db->get('invoices');
        return $query->result();
    }

    public function invoice_print($id) {
        $this->db->where('inv_id', $id);
        $query = $this->db->get('sale');
        return $query->result();
    }

    public function find($id) {
        $this->db->where('invoice_id', $id);
        $query = $this->db->get('sale');
        return $query->result();
    }

    public function delete($id) {
        $array = array(
            'id' => $id);
        $this->db->where($array);
        $q = $this->db->get('sale');
        $value = $q->row();


        $sale = $value->sale;
        $product = $value->product_id;

        $array = array(
            'product_id' => $product);
        $this->db->where($array);
        $qg = $this->db->get('total_stock');
        $ress = $qg->result_array();
        $pre = $ress[0]['total_stock'];
        $total = $pre + $sale;

        $this->db->where('product_id', $product);
        $sql = $this->db->update(
                'total_stock', [

            'total_stock' => $total,
                ]
        );



        try {
            $this->db->delete('sale', ['id' => $id]);
        }
        catch (Exception $e) {
            return "failed";
        }
    }

    public function delete_sale($id) {
        $array = array(
            'inv_id' => $id);
        $this->db->where($array);
        $q = $this->db->get('sale');
        $res = $q->result();

        foreach ($res as
                $value) {

            $sale = $value->sale;
            $product = $value->product_id;

            $array = array(
                'product_id' => $product);
            $this->db->where($array);
            $qg = $this->db->get('total_stock');
            $ress = $qg->result_array();
            $pre = $ress[0]['total_stock'];
            $total = $pre + $sale;

            $this->db->where('product_id', $product);
            $sql = $this->db->update(
                    'total_stock', [

                'total_stock' => $total,
                    ]
            );

            $array1 = array(
                'invoice_id' => $id);
            $this->db->where($array1);
            $q2 = $this->db->get('invoice');
            $indata = $q2->result_array();

            $shop = $indata[0]['shop_id'];

            $array2 = array(
                'inv_id' => $id);
            $this->db->where($array2);
            $q1 = $this->db->get('invoices');
            $dataall = $q1->result_array();
            $total = $dataall[0]['total'];
            $paid = $dataall[0]['paid'];
            $net = $dataall[0]['net'];
            $discount = $dataall[0]['discount'];
            $date = $dataall[0]['date'];


            AdminLTE::book_total_delete($shop, $total, $paid, $net, $discount, $date);


            $query = $this->db->delete('sale', ['inv_id' => $id]);
            $query = $this->db->delete('invoice', ['invoice_id' => $id]);
            $query = $this->db->delete('invoices', ['inv_id' => $id]);
        }

        if ($query) {
            set_flash_alert('Invoice deleted', 'success');
            redirect("admin/sale/invoice");
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }

    public function bookadd($id, $shop) {

        $discount = $this->input->post('discount', TRUE);

        $bilty = $this->input->post('bilty_discount', TRUE);
        $date = $this->input->post('date', TRUE);
        $total = $this->input->post('total', TRUE);
        $old = $this->input->post('old', TRUE);
        $paid = $this->input->post('paid', TRUE);
        $bank_name = $this->input->post('banking');
        $bank = $this->input->post('bank');
        $comments = $this->input->post('comments');
        $dis_per = $this->input->post('dis_per');
        if (empty($paid)) {
            $paid = 0;
        }
        $type_payment = "";

        $debit = ($total - $discount + $bilty) - $paid;
        $net = ($total - $discount + $bilty) - $paid + $old;

        if ($bank_name == " ") {
            $type_payment = " ";
        }
        else {
            $type_payment = $bank_name . "-" . $bank;
            AdminLTE::bank_entry($bank_name, $paid, 0, $bank, $date, 'cs_' . $shop);
        }


        $dis = "";
        if (!empty($dis_per)) {

            $dis = $dis_per . " %";
        }



        $array = array(
            'inv_id' => $id);
        $this->db->where($array);
        $q = $this->db->get('invoices');
        if ($q->num_rows() === 0) {

            $array = array(
                'shop_id' => $shop);
            $this->db->where($array);
            $q = $this->db->get('book');
            if ($q->num_rows() > 0) {
                $res = $q->result_array();
                $totalamount = $res[0]['total'];
                $totalcredit = $res[0]['credit'];
                $totaldebit = $res[0]['debit'];
                $totaldiscount = $res[0]['discount'];
                $totaldiscountbilty = $res[0]['bilty'];

                $tamount = $total + $totalamount;
                $tdisc = $discount + $totaldiscount;
                $tdiscbilty = $bilty + $totaldiscountbilty;
                $tcredit = $paid + $totalcredit;
                $tdebit = $debit + $totaldebit;


                $this->db->where('shop_id', $shop);
                // Insert user into DB
                $sql = $this->db->update(
                        'book', [

                    'total' => round($tamount, 2),
                    'credit' => round($tcredit, 2),
                    'debit' => round($tdebit, 2),
                    'discount' => round($tdisc, 2),
                    'bilty' => round($tdiscbilty, 2),
                        ]
                );

                $sql = $this->db->insert(
                        'book_detail', [

                    'credit' => $paid,
                    'debit' => $debit,
                    'date' => $date,
                    'shop_id' => $shop,
                    'discount' => $discount,
                    'bilty' => $bilty,
                        ]
                );

                $sql = $this->db->insert(
                        'invoices', [

                    'inv_id' => $id,
                    'total' => $total,
                    'paid' => $paid,
                    'old' => $old,
                    'net' => $net,
                    'bilty' => $bilty,
                    'discount' => $discount,
                    'date' => $date,
                    'type_payment' => $type_payment,
                    'comments' => $comments,
                    'dis_per' => $dis,
                ]);
            }
            else {
                $sql = $this->db->insert(
                        'book_detail', [

                    'credit' => $paid,
                    'debit' => $debit,
                    'date' => $date,
                    'shop_id' => $shop,
                    'discount' => $discount,
                    'bilty' => $bilty,
                        ]
                );
                // Insert user into DB
                $sql = $this->db->insert(
                        'book', [
                    'shop_id' => $shop,
                    'total' => $total,
                    'credit' => $paid,
                    'debit' => $debit,
                    'discount' => $discount,
                    'bilty' => $bilty,
                        ]
                );
                $sql = $this->db->insert(
                        'invoices', [

                    'inv_id' => $id,
                    'total' => $total,
                    'paid' => $paid,
                    'old' => $old,
                    'net' => $net,
                    'bilty' => $bilty,
                    'discount' => $discount
                    ,
                    'date' => $date,
                    'type_payment' => $type_payment,
                    'comments' => $comments,
                    'dis_per' => $dis,
                ]);
            }
        }
    }

}
