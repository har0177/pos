<?php

class Returns_model extends CI_Model {

    public function total() {
        $query = $this->db->query('Select Distinct(product_id) from returns');
        return $query->result();
    }

    public function bookdetails($id) {
        $array = array(
            'shop_id' => $id);
        $this->db->where($array);
        $query = $this->db->get('book');
        return $query->result();
    }

    public function total_shop() {
        $query = $this->db->query('Select Distinct(inv_id) from returns');
        return $query->result();
    }

    public function add_returns($stoc) {

        try {
            $inv = "";
            $newprice = 0;
            for ($x = 0;
                    $x < count($stoc);
                    $x++) {

                $product_id = $stoc[$x]['pid'];

                if (empty($stoc[$x]['stock']) || $stoc[$x]['stock'] == 0) {
                    continue;
                }
                else {
                    $array = array(
                        'product_id' => $product_id,
                        'inv_id' => $stoc[$x]['inv']);
                    $this->db->where($array);
                    $q = $this->db->get('sale');
                    if ($q->num_rows() > 0) {

                        $res = $q->result_array();
                        $pre_sale = $res[0]['sale'];

                        if ($stoc[$x]['stock'] > $pre_sale) {
                            return "greater";
                        }
                        else {
                            $total = $pre_sale - $stoc[$x]['stock'];

// Insert user into DB
                            $sql = $this->db->insert(
                                    'returns', [
                                'product_id' => $res[0]['product_id'],
                                'sale' => $stoc[$x]['stock'],
                                'price' => $stoc[$x]['sprice'],
                                'date' => date("Y-m-d"),
                                'inv_id' => $stoc[$x]['inv'],
                                    ]
                            );


                            $array2 = array(
                                'inv_id' => $stoc[$x]['inv']);
                            $this->db->where($array2);
                            $query = $this->db->get('invoices');
                            $data = $query->result_array();
                            $net = $data[0]['net'];
                            $total_amount = $data[0]['total'];
                            $return_amount = $stoc[$x]['stock'] * $stoc[$x]['sprice'];

                            $this->db->where('inv_id', $stoc[$x]['inv']);
                            $sql = $this->db->update(
                                    'invoices', [
                                'total' => $total_amount - $return_amount,
                                'net' => $net - $return_amount,
                                    ]
                            );

                            
                                $array = array(
                                    'product_id' => $product_id,
                                    'inv_id' => $stoc[$x]['inv']);
                                $this->db->where($array);
                                $sql = $this->db->update(
                                        'sale', [
                                    'sale' => $total,
                                        ]
                                );
                            
                            $array = array(
                                'product_id' => $product_id);
                            $this->db->where($array);
                            $q = $this->db->get('total_stock');
                            if ($q->num_rows() > 0) {
                                $res = $q->result_array();
                                $pre_sale = $res[0]['total_stock'];
                                $total = $pre_sale + $stoc[$x]['stock'];

                                $this->db->where('product_id', $product_id);
                                $sql = $this->db->update(
                                        'total_stock', [
                                    'total_stock' => $total,
                                        ]
                                );
                            }

                            $newprice += $stoc[$x]['stock'] * $stoc[$x]['sprice'];
                        }
                    }
                    else {
                        return "nostock";
                    }
                }

                $inv = $stoc[$x]['inv'];
            }
            $array1 = array(
            'shop_id' => AdminLTE::customers_name($inv));
            $this->db->where($array1);
            $query1 = $this->db->get('book');
            $data1 = $query1->result_array();
            $old = $data1[0]['debit'];
            $total_pre = $data1[0]['total'];
            $totalamount = $old - $newprice;
            $total_new = $total_pre - $newprice;
            $this->db->where('shop_id', AdminLTE::customers_name($inv));
            $sql = $this->db->update(
                    'book', [
                'total' => $total_new,
                'debit' => $totalamount,
                    ]
            );
            return $inv;
        }
        catch (Exception $e) {
            return "failed";
        }
    }

    public function products($cur = "") {

        $query = $this->db->get('products');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->product </option>";
        }
        return $data;
    }

    public function shopkeepers($cur = "") {

        $query = $this->db->get('shopkeeper');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->fullname </option>";
        }
        return $data;
    }

    public function all() {
        $query = $this->db->query('Select Distinct(inv_id) from sale');
        return $query->result();
    }

    public function all_invoice() {
        $query = $this->db->query("Select Distinct(inv_id) from returns");
        return $query->result();
    }

    public function all_invoices($id) {
        $array = array(
            'inv_id' => $id);
        $this->db->where($array);
        $query = $this->db->get('invoices');
        return $query->result();
    }

    public function invoice_print($id) {
        $this->db->where('inv_id', $id);
        $query = $this->db->get('sale');
        return $query->result();
    }

    public function invoice_return($id) {
        $this->db->where('inv_id', $id);
        $query = $this->db->get('returns');
        return $query->result();
    }

    public function book_return($id) {
        $cus_id = AdminLTE::customers_name($id);
        $returns = $this->input->post('return', TRUE);
        $returns_price = $this->input->post('return_price', TRUE);

        foreach ($returns as
                $key =>
                $return) {
            if (empty($return) || $return == 0) {
                continue;
            }
            else {
                $array = array(
                    'product_id' => $key,
                    'inv_id' => $id);
                $this->db->where($array);
                $q = $this->db->get('sale');
                if ($q->num_rows() > 0) {

                    $res = $q->result_array();
                    $pre_sale = $res[0]['sale'];

                    if ($return > $pre_sale) {
                        set_flash_alert('Return is Greater Than Sale of ' . AdminLTE::product_name($key), 'danger');
                        redirect("admin/returns/invoice_printing/" . $id);
                    }
                    else {
                        $total = $pre_sale - $return;

// Insert user into DB
                        $sql = $this->db->insert(
                                'returns', [
                            'product_id' => $key,
                            'sale' => $return,
                            'price' => $returns_price[$key],
                            'date' => date("Y-m-d"),
                            'inv_id' => $id
                                ]
                        );

                        $array2 = array(
                            'inv_id' => $id);
                        $this->db->where($array2);
                        $query = $this->db->get('invoices');
                        $data = $query->result_array();
                        $net = $data[0]['net'];
                        $total_amount = $data[0]['total'];
                        $return_amount = $return * $returns_price[$key];

                        $this->db->where('inv_id', $id);
                        $sql = $this->db->update(
                                'invoices', [
                            'total' => $total_amount - $return_amount,
                            'net' => $net - $return_amount,
                                ]
                        );

                        if ($total == 0) {
                            $this->db->delete('sale', ['product_id' => $key,
                                'inv_id' => $id]);
                        }
                        else {
                            $array = array(
                                'product_id' => $key,
                                'inv_id' => $id);
                            $this->db->where($array);
                            $sql = $this->db->update(
                                    'sale', [
                                'sale' => $total,
                                    ]
                            );
                        }
                        $array = array(
                            'product_id' => $key);
                        $this->db->where($array);
                        $q = $this->db->get('total_stock');
                        if ($q->num_rows() > 0) {
                            $res = $q->result_array();
                            $pre_sale = $res[0]['total_stock'];
                            $total = $pre_sale + $return;

                            $this->db->where('product_id', $key);
                            $sql = $this->db->update(
                                    'total_stock', [
                                'total_stock' => $total,
                                    ]
                            );
                        }


                        $array1 = array(
                            'shop_id' => $cus_id);
                        $this->db->where($array1);
                        $query = $this->db->get('book');
                        $data = $query->result_array();
                        $old = $data[0]['debit'];
                        $total_pre = $data[0]['total'];
                        $totalamount = $old - ($return * $returns_price[$key]);
                        $total_new = $total_pre - ($return * $returns_price[$key]);
                        $this->db->where('shop_id', $cus_id);
                        $sql = $this->db->update(
                                'book', [
                            'total' => $total_new,
                            'debit' => $totalamount,
                                ]
                        );
                    }
                }
                else {
                    set_flash_alert('No Stock Available', 'danger');
                    redirect("admin/returns/invoice_printing/" . $id);
                }
            }
        }
        if ($sql) {
            set_flash_alert('Returns created successfully', 'success');
            return TRUE;
        }
        else {
            set_flash_alert('No Stock Found!', 'danger');
            redirect("admin/returns/invoice_printing/" . $id);
        }
    }

    public function update($id) {
// Load form validation library
        $this->load->library('form_validation');
// define rules
        $rules = [
            [
                'field' => 'product',
                'label' => 'Returns Name',
                'rules' => 'required'
            ],
            [
                'field' => 'shop',
                'label' => 'Shopkeeper Name',
                'rules' => 'required'
            ],
            [
                'field' => 'sale',
                'label' => 'Returns',
                'rules' => 'required'
            ],
            [
                'field' => 'date',
                'label' => 'Date',
                'rules' => 'required'
            ]
        ];

// Set rules
        $this->form_validation->set_rules($rules);
// Check form
        if ($this->form_validation->run() != FALSE) {
            $product = $this->input->post('product', TRUE);
            $shop = $this->input->post('shop', TRUE);
            $sale = $this->input->post('sale', TRUE);
            $date = $this->input->post('date', TRUE);


// Data for db
            $update['product_id'] = $product;
            $update['sale'] = $product;
            $update['date'] = $product;
            $update['shop_id'] = $product;



// Update user into DB
            $this->db->where('id', $id);
            $sql = $this->db->update('returns', $update);
            if ($sql) {
                set_flash_alert('Returns updated successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function find($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('returns');
        return $query->result();
    }

    public function delete($id) {
        $array = array(
            'id' => $id);
        $this->db->where($array);
        $ret = $this->db->get('returns');
        $rest = $ret->result_array();
        $product = $rest[0]['product_id'];
        $sale = $rest[0]['sale'];
        $date = $rest[0]['date'];
        $shop = $rest[0]['shop_id'];


        $array3 = array(
            'product_id' => $product,
            'shop_id' => $shop,
            'date' => $date);
        $this->db->where($array3);

        $qg = $this->db->get('sale');
        $ress = $qg->result_array();
        $pre = $ress[0]['sale'];
        $total = $pre + $sale;

        $array1 = array(
            'product_id' => $product,
            'shop_id' => $shop,
            'date' => $date);
        $this->db->where($array1);
        $this->db->update(
                'sale', [
            'sale' => $total,
                ]
        );

        $array2 = array(
            'product_id' => $product);
        $this->db->where($array2);
        $qgd = $this->db->get('total_stock');
        $ressd = $qgd->result_array();
        $prev = $ressd[0]['total_stock'];
        $total1 = $prev - $sale;

        $this->db->where('product_id', $product);
        $this->db->update(
                'total_stock', [
            'total_stock' => $total1,
                ]
        );


        $query = $this->db->delete('returns', ['id' => $id]);
        if ($query) {
            set_flash_alert('Returns deleted', 'success');
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }

}
