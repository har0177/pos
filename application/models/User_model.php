<?php

class User_model extends CI_Model
{

    public function login()
    {
        $this->load->library('form_validation');

        // Define rules
        $rules = [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ]
        ];
        // check rules
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() != FALSE) {
            $username = $this->input->post('username', TRUE);
            $password = crypt($this->input->post('password'), 'ca');

            $this->db->select('id, username, fullname, status');
            $this->db->where('username', $username);
            $this->db->where('password', $password);
            $query = $this->db->get('user');
            $result = $query->result();
            if (empty($result)) {
                set_flash_alert('Invalid login details');
                return false;
            } else {
                $user = $result[0];
                $this->session->set_userdata([
                    'user_logged' => $user->username,
                    'user_id' => $user->id,
                    'user_name' => $user->fullname,
                    'user_status' => $user->status,
                ]);
                set_flash_alert('logged-in successfully', 'success');
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    public function all()
    {
        $query = $this->db->get('user');
        return $query->result();
    }

    public function create()
    {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],
            [
                'field' => 'email',
                'label' => 'Email address',
                'rules' => 'required|valid_email|is_unique[user.email]'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ],
            [
                'field' => 'confirm_password',
                'label' => 'Retype Password',
                'rules' => 'required|matches[password]'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'required'
            ],
            [
                'field' => 'phone',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
             [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            ],
        ];
        
        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if($this->form_validation->run() != FALSE){
            $fullname    = $this->input->post('fullname',TRUE);
            $username      = $this->input->post('username',TRUE);
            $email          = $this->input->post('email',TRUE);
            $password       = crypt($this->input->post('password',TRUE),'ca');
            $address          = $this->input->post('address',TRUE);
            $phone         = $this->input->post('phone',TRUE);
             $status      = $this->input->post('status',TRUE);
            
            // Insert user into DB
            $sql = $this->db->insert(
                'user',
                [
                    'fullname' => $fullname,
                    'username' => $username,
                    'email' => $email,
                    'password' => $password,
                    'address' => $address,
                    'phone' => $phone,
                    'status' => $status,
                
                    
                ]
                );
            if($sql){
                set_flash_alert('User created successfully', 'success');
                return TRUE;
            }else{
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }
    
    public function update($id)
    {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
         $rules = [
            [
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],
            [
                'field' => 'email',
                'label' => 'Email address',
                'rules' => 'required|valid_email|trim'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ],
            [
               'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[password]'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'required'
            ],
            [
                'field' => 'phone',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
             [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            ],
        ];
        
        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if($this->form_validation->run() != FALSE){
                 $fullname    = $this->input->post('fullname',TRUE);
            $username      = $this->input->post('username',TRUE);
            $email          = $this->input->post('email',TRUE);
            $password       = crypt($this->input->post('password',TRUE),'ca');
            $address          = $this->input->post('address',TRUE);
            $phone         = $this->input->post('phone',TRUE);
            $status      = $this->input->post('status',TRUE);
           
            
            // Data for db
            $update['fullname'] = $fullname;
            $update['username'] = $username;
            $update['email'] = $email;
            if(!empty($this->input->post('password',TRUE))){
                $update['password'] = $password;
            }
            $update['address'] = $address;
            $update['phone'] = $phone;
            $update['status'] = $status;
            
            // Update user into DB
            $this->db->where('id',$id);
            $sql = $this->db->update('user',$update);
            if($sql){
                set_flash_alert('User updated successfully', 'success');
                return TRUE;
            }else{
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }
    
    public function userprofile($id)
    {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
         $rules = [
            [
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],
            [
                'field' => 'email',
                'label' => 'Email address',
                'rules' => 'required|valid_email|trim'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ],
            [
               'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[password]'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'required'
            ],
            [
                'field' => 'phone',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
            
        ];
        
        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if($this->form_validation->run() != FALSE){
                 $fullname    = $this->input->post('fullname',TRUE);
            $username      = $this->input->post('username',TRUE);
            $email          = $this->input->post('email',TRUE);
            $password       = crypt($this->input->post('password',TRUE),'ca');
            $address          = $this->input->post('address',TRUE);
            $phone         = $this->input->post('phone',TRUE);
          
            
            // Data for db
            $update['fullname'] = $fullname;
            $update['username'] = $username;
            $update['email'] = $email;
            if(!empty($this->input->post('password',TRUE))){
                $update['password'] = $password;
            }
            $update['address'] = $address;
            $update['phone'] = $phone;
          
            // Update user into DB
            $this->db->where('id',$id);
            $sql = $this->db->update('user',$update);
            if($sql){
                set_flash_alert('Profile updated successfully', 'success');
                return TRUE;
            }else{
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }
    
    public function find($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get('user');
        return $query->result();
    }
    
    public function delete($id)
    {
        $query = $this->db->delete('user',['id'=>$id]);
        if($query){
            set_flash_alert('User deleted','success');
        }else{
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
    
    public function activate($id)
    {
        $query = $this->db->update('user',['deleted'=>'0'],['id'=>$id]);
        if($query){
            set_flash_alert('User Activated','success');
        }else{
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
    
    public function deactivate($id)
    {
        $query = $this->db->update('user',['deleted'=>'1'],['id'=>$id]);
        if($query){
            set_flash_alert('User Deactivated','success');
        }else{
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
    
     public function status(){
        return [
                 ''=>'',
            '1' => 'Active',
            '0' => 'Deactive',
           
        ];
    }
    
    
}
