<?php

class Customer_model extends CI_Model
{

    
    public function all()
    {
         $this->db->order_by('id', 'ASC');
        $query = $this->db->get('customers');
        return $query->result();
    }
    
    public function bank_details($id)
    {$this->db->where('shop_id', 'cs_'.$id);
         $this->db->order_by('id', 'ASC');
        $query = $this->db->get('bank_details');
        return $query->result();
    }
    
     public function book_details($id)
    {
         
        $query = $this->db->query('Select * from book_detail where shop_id = ? and YEAR(date) = "'.date("Y").'"', array($id));
        return $query->result();
    }
    
    
      public function all_invoice($id) {
          $this->db->order_by('id', 'ASC');
           $query = $this->db->get('invoices');
           $data = array();
           foreach ($query->result() as
                   $value) {
               $customer = AdminLTE::customers_name($value->inv_id);
               if($customer != $id){
                   continue;
               }else{
                   $data[] = $value->inv_id;
               }
           }
        return $data;
    }
    
   

    public function create()
    {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
           
           
            [
                'field' => 'phone',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
             [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            ],
        ];
        
        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if($this->form_validation->run() != FALSE){
            $fullname    = $this->input->post('fullname',TRUE);
            
            $phone         = $this->input->post('phone',TRUE);
             $status      = $this->input->post('status',TRUE);
               $email     = $this->input->post('email',TRUE);
                 $address      = $this->input->post('address',TRUE);
            
            // Insert user into DB
            $sql = $this->db->insert(
                'customers',
                [
                    'fullname' => $fullname,
                     'email' => $email,
                     'address' => $address,
                   
                    'phone' => $phone,
                    'status' => $status,
                
                    
                ]
                );
            if($sql){
                set_flash_alert('Customer created successfully', 'success');
                return TRUE;
            }else{
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }
    
    public function update($id)
    {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
         $rules = [
            [
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
           
            [
                'field' => 'phone',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
             [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            ],
        ];
        
        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if($this->form_validation->run() != FALSE){
                 $fullname    = $this->input->post('fullname',TRUE);
            $phone         = $this->input->post('phone',TRUE);
            $status      = $this->input->post('status',TRUE);
              $email     = $this->input->post('email',TRUE);
                 $address      = $this->input->post('address',TRUE);
            
            // Data for db
            $update['fullname'] = $fullname;
            $update['phone'] = $phone;
            $update['status'] = $status;
            $update['email'] = $email;
            $update['address'] = $address;
            
            // Update user into DB
            $this->db->where('id',$id);
            $sql = $this->db->update('customers',$update);
            if($sql){
                set_flash_alert('Customer updated successfully', 'success');
                return TRUE;
            }else{
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }
    
  
    
    public function find($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get('customers');
        return $query->result();
    }
    
    public function delete($id)
    {
        $query = $this->db->delete('customers',['id'=>$id]);
        if($query){
            set_flash_alert('Customer deleted','success');
        }else{
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
    
    public function activate($id)
    {
        $query = $this->db->update('customers',['deleted'=>'0'],['id'=>$id]);
        if($query){
            set_flash_alert('Customer Activated','success');
        }else{
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
    
    public function deactivate($id)
    {
        $query = $this->db->update('customers',['deleted'=>'1'],['id'=>$id]);
        if($query){
            set_flash_alert('Customer Deactivated','success');
        }else{
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
    
     public function status(){
        return [
                 ''=>'',
            '1' => 'Active',
            '0' => 'Deactive',
           
        ];
    }
    
    
}
