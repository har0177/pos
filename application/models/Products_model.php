<?php

class Products_model extends CI_Model {

    var $table = "products p";
    var $select_column = array(
        "p.id",
        "p.name",
        "p.cat_id",
        "p.pprice",
        "p.sprice",
        "p.hprice");
    var $order_column = array(
        null,
        "p.name",
        "p.pprice",
        "p.sprice",
        "p.hprice",
        null);

    function make_query() {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        $this->db->join('category c', 'c.id = p.cat_id', 'right');
        $search = $_POST["search"]["value"];
        if (isset($search)) {
            $this->db->like("p.name", $search);
            $this->db->or_like("c.name", $search);
            $this->db->or_like("p.pprice", $search);
            $this->db->or_like("p.hprice", $search);
            $this->db->or_like("p.sprice", $search);
        }

        if (isset($_POST["order"])) {
            $this->db->order_by($this->order_column[$_POST["order"]["0"]["column"]], $_POST["order"]["0"]["dir"]);
        }
        else {
            $this->db->order_by("p.id", "DESC");
        }
    }
    
    function make_datatables(){
        $this->make_query();
        if($_POST["length"] != -1){
            $this->db->limit($_POST["length"], $_POST["start"]);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_filtered_data(){
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
        
    }
    
    function get_all_data(){
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function all() {
        $this->db->order_by('cat_id', 'ASC');
        $query = $this->db->get('products');
        return $query->result();
    }

    public function create() {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'name',
                'label' => 'Product Name',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $name = $this->input->post('name', TRUE);
            $pprice = $this->input->post('pprice', TRUE);
            $sprice = $this->input->post('sprice', TRUE);
            $hprice = $this->input->post('hprice', TRUE);
            $category = $this->input->post('category', TRUE);
            $cat_new = $this->input->post('cat_new', TRUE);

            if (!empty($cat_new)) {
                // Insert user into DB
                $sql = $this->db->insert(
                        'category', [
                    'name' => $cat_new,
                        ]
                );
                $cat_id = $this->db->insert_id();

                // Insert user into DB
                $sql = $this->db->insert(
                        'products', [
                    'name' => $name,
                    'cat_id' => $cat_id,
                    'pprice' => $pprice,
                    'sprice' => $sprice,
                    'hprice' => $hprice,
                        ]
                );
            }
            else {

                // Insert user into DB
                $sql = $this->db->insert(
                        'products', [
                    'name' => $name,
                    'cat_id' => $category,
                    'pprice' => $pprice,
                    'sprice' => $sprice,
                    'hprice' => $hprice,
                        ]
                );
            }


            if ($sql) {
                set_flash_alert('Product created successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function update($id) {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'name',
                'label' => 'Fullname',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $name = $this->input->post('name', TRUE);
            $category = $this->input->post('category', TRUE);
            $pprice = $this->input->post('pprice', TRUE);
            $sprice = $this->input->post('sprice', TRUE);
            $hprice = $this->input->post('hprice', TRUE);

            // Data for db
            $update['name'] = $name;
            $update['cat_id'] = $category;
            $update['pprice'] = $pprice;
            $update['sprice'] = $sprice;
            $update['hprice'] = $hprice;



            // Update user into DB
            $this->db->where('id', $id);
            $sql = $this->db->update('products', $update);

            if ($sql) {
                set_flash_alert('Product updated successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function find($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('products');
        return $query->result();
    }

    public function delete($id) {
        $query = $this->db->delete('products', ['id' => $id]);
        if ($query) {
            set_flash_alert('Product deleted', 'success');
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }

}
