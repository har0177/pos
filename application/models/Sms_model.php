<?php

class Sms_model
        extends CI_Model {

    public function singlesms() {
        $return[' '] = "Select Mobile Number";
        $this->db->where('status', '1');
        $query = $this->db->get('student');
        foreach ($query->result_array() as
                $row) {
            if (empty($row['f_contact'])) {
                continue;
            }
            $return[$row['f_contact']] = $row['f_contact'];
        }
        return $return;
    }

    public function create() {

        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'sender',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
            [
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {

            $phone = $this->input->post('sender', TRUE);
            $message = $this->input->post('message', TRUE);

           AdminLTE::sms($phone, $message);
            $this->db->insert('logs', [
                'contact' => $phone,
                'msg' => $message,
                'date' => date("Y-m-d H:i:s"),
                'from_user' => $this->session->user_id,
            ]);
        }
        return FALSE;
    }

    public function single() {

        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'mobile',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
            [
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {

            $phone = $this->input->post('mobile', TRUE);
            $message = $this->input->post('message', TRUE);

           AdminLTE::sms($phone, $message);
            $this->db->insert('logs', [
                'contact' => $phone,
                'msg' => $message,
                'date' => date("Y-m-d H:i:s"),
                'from_user' => $this->session->user_id,
            ]);
        }
        return FALSE;
    }

    public function bulk() {

        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'sender[]',
                'label' => 'Phone Number',
                'rules' => 'required'
            ],
            [
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {


            $message = $this->input->post('message', TRUE);
            $sender = $this->input->post('sender', TRUE);


            foreach ($sender as
                    $value) {
               

                    AdminLTE::sms($value, $message);
                    $this->db->insert('logs', [
                        'contact' => $value,
                        'msg' => $message,
                        'date' => date("Y-m-d H:i:s"),
                        'from_user' => $this->session->user_id,
                    ]);
                
            }
        }
        return FALSE;
    }

    public function vendor() {

        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {


            $message = $this->input->post('message', TRUE);

           
            $array = array('status' => 1);
            $this->db->where($array);
            $query = $this->db->get('vendor');
            foreach ($query->result_array() as
                    $row) {

              
                $value = $row['phone'];
                if ($value == 0 || $value == "") {
                    continue;
                }
             

                    AdminLTE::sms($value, $message);
                    $this->db->insert('logs', [
                        'contact' => $value,
                        'msg' => $message,
                        'date' => date("Y-m-d H:i:s"),
                        'from_user' => $this->session->user_id,
                    ]);
                
            }
        }
        return FALSE;
    }

    public function customer() {

        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {


            $message = $this->input->post('message', TRUE);


            $array = array('status' => 1);
            $this->db->where($array);
            $query = $this->db->get('customers');
            foreach ($query->result_array() as
                    $row) {
               
                $value = $row['phone'];
                if ($value == 0 || $value == "") {
                    continue;
                }
             

                    AdminLTE::sms($value, $message);
                    $this->db->insert('logs', [
                        'contact' => $value,
                        'msg' => $message,
                        'date' => date("Y-m-d H:i:s"),
                        'from_user' => $this->session->user_id,
                    ]);
               
            }
        }
        return FALSE;
    }

     public function all_sms() {
        $this->db->where(array(
            'from_user' => $this->session->user_id));

        $query = $this->db->get('sms_temp');
        return $query->result();
    }

    public function add_sms() {
        // Load form validation library
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'message',
                'label' => 'sms',
                'rules' => 'required'
            ],
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $sms = $this->input->post('message', TRUE);
            $sql = $this->db->insert('sms_temp', [
                'sms' => $sms,
                'from_user' => $this->session->user_id,
                    ]
            );

            if ($sql) {
                set_flash_alert('SMS created successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function edit_sms($id) {
        $this->load->library('form_validation');
        // define rules
        $rules = [
            [
                'field' => 'message',
                'label' => 'sms',
                'rules' => 'required'
            ]
        ];

        // Set rules
        $this->form_validation->set_rules($rules);
        // Check form
        if ($this->form_validation->run() != FALSE) {
            $sms = $this->input->post('message', TRUE);

            // Data for db
            $update['sms'] = $sms;
            $array = array(
                'id' => $id);
            $this->db->where($array);
            $q = $this->db->get('sms_temp');


            // Update user into DB
            $this->db->where('id', $id);
            $sql = $this->db->update('sms_temp', $update);

            if ($sql) {
                set_flash_alert('SMS updated successfully', 'success');
                return TRUE;
            }
            else {
                set_flash_alert(implode(': ', $this->db->error()));
            }
        }
        return FALSE;
    }

    public function find_sms($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('sms_temp');
        return $query->result();
    }

    public function delete_sms($id) {
        $query = $this->db->delete('sms_temp', ['id' => $id]);
        if ($query) {
            set_flash_alert('SMS deleted', 'success');
        }
        else {
            set_flash_alert(implode(': ', $this->db->error()));
        }
    }
}
