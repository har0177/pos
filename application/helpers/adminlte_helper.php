<?php

class AdminLTE {

     public static function sale_count($id, $table, $from, $to) {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(sale) as total, sprice, price from ' . $table . ' where product_id = "' . $id . '" AND DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '"');

        foreach ($q->result() as
                $r) {

            echo "<td>" . $r->total . "</td>";
            echo "<td>" . round(($r->price - $r->sprice), 2) . "</td>";
            echo "<td>" . round($r->total * ($r->price - $r->sprice), 2) . "</td>";
        }
    }
	
	  public static function sale_returns($pid, $invoice) {
        $CI = & get_instance();
        $q = $CI->db->query("Select SUM(sale) as sale from returns where product_id = $pid and inv_id = $invoice")->row();

        if (!empty($q->sale)) {

            return $q->sale;
        }
    }

    public static function customerbalance() {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(total) as total, SUM(discount) as discount, SUM(credit) as credit, SUM(debit) as debit from book');

        foreach ($q->result() as
                $r) {

            echo "<td>1</td><th>Customer</th><td>" . round($r->total, 2). "</td><td>" . round($r->discount, 2) . "</td><td>" . round($r->credit, 2) . "</td><td>" . round($r->debit, 2) . "</td>";
        }
    }
    
     public static function total_show($table, $field) {
        $CI = & get_instance();
        $q = $CI->db->query("Select SUM($field) as total from $table");
$total = 0;
        foreach ($q->result() as
                $r) {

            $total = $r->total;
        }
        return $total;
    }

    public static function vendorbalance() {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(total) as total, SUM(discount) as discount, SUM(credit) as credit, SUM(debit) as debit from book_vendor');

        foreach ($q->result() as
                $r) {

            echo "<td>2</td><th>Vendor</th><td>" . round($r->total, 2). "</td><td>" . round($r->discount, 2) . "</td><td>" . round($r->credit, 2) . "</td><td>" . round($r->debit, 2) . "</td>";
        }
    }
    

        public static function sale_count_inv($id, $table, $from, $to) {
        $CI = & get_instance();
        $q = $CI->db->query('Select * from ' . $table . ' where inv_id = "' . $id . '" AND DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '"');
        $total = 0;
        $sale = 0;
        $bilty = AdminLTE::bilty_sale($id);
        foreach ($q->result() as
                $r) {

            $sale += $r->sale;
            $total += $r->sale * ($r->price - $r->sprice);
        }
        echo "<td>" . $sale . "</td>";
        echo "<td>" . round($bilty, 2) . "</td>";
        echo "<td>" . round($total - $bilty, 2) . "</td>";
    }

    public static function sale_count_all($id, $table) {
        $CI = & get_instance();
        $q = $CI->db->query('Select * from ' . $table . ' where inv_id = "' . $id . '"');
        $total = 0;
        $sale = 0;
        $bilty = AdminLTE::bilty_sale($id);
        foreach ($q->result() as
                $r) {

            $sale += $r->sale;
            $total += $r->sale * ($r->price - $r->sprice);
        }
        echo "<td>" . $sale . "</td>";
         echo "<td>" . round($bilty, 2) . "</td>";
        echo "<td>" . round($total - $bilty, 2) . "</td>";
    }
	 public static function bilty_sale($id) {
        $CI = & get_instance();


        $query = $CI->db->query('Select discount from invoices where inv_id ="' . $id . '"');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['discount'];
        }
        else {
            return FALSE;
        }
    }
	
	public static function bilty_month($date) {
        $CI = & get_instance();


        $query = $CI->db->query('Select SUM(discount) as discount from book_detail where MONTH(date) ="' . $date . '"');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['discount'];
        }
        else {
            return FALSE;
        }
    }

    public static function profit_month($month) {
        $CI = & get_instance();
        $q = $CI->db->query("Select * from sale where MONTH(date) = $month");
        $total = 0;
        foreach ($q->result() as
                $r) {
          
            $total += $r->sale * ($r->price - $r->sprice);
        }

        return round($total, 2);
        
    }

    public static function profit() {
        $CI = & get_instance();
        $q = $CI->db->query('Select * from sale');
        $total = 0;
       
        foreach ($q->result() as
                $r) {
           
            $total += $r->sale * ($r->price - $r->sprice);
        }
         return round($total, 2);
    }
    
    public static function vendor_discount_month($month) {
        $CI = & get_instance();
        $year = date("Y");
        $q = $CI->db->query("Select SUM(discount) as disc from invoices_vendor where MONTH(date) = $month and YEAR(date) = $year");
        $total = 0;
       
        foreach ($q->result() as
                $r) {
      
            $total += $r->disc;
        }

         return round($total, 2);
        }

    public static function expenses() {
        $CI = & get_instance();
        $q = $CI->db->query('Select * from expenses');
        $total = 0;

        foreach ($q->result() as
                $r) {

            $total += $r->amount;
        }
         return round($total, 2);
    }

    public static function expenses_month($month) {
        $CI = & get_instance();
        $q = $CI->db->query("Select * from expenses where MONTH(date) = $month");
        $total = 0;

        foreach ($q->result() as
                $r) {

            $total += $r->amount;
        }
        return round($total, 2);
    }

    public static function deposit($id) {
        $CI = & get_instance();
        $q = $CI->db->query("Select SUM(deposit) as total from bank_details where bank_id = $id");

$total = 0;
        foreach ($q->result() as
                $r) {

            $total =  $r->total;
        }
         return round($total, 2);
    }

    public static function withdraw($id) {
        $CI = & get_instance();
        $q = $CI->db->query("Select SUM(withdraw) as total from bank_details where bank_id = $id");


       $total = 0;
        foreach ($q->result() as
                $r) {

            $total =  $r->total;
        }
         return round($total, 2);
    }

    public static function stock_count_date($id, $table, $from, $to) {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(stock) as total from ' . $table . ' where product_id = "' . $id . '" AND DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '"');

        foreach ($q->result() as
                $r) {

            echo $r->total;
        }
    }

    public static function stock_count($id) {
        $CI = & get_instance();
        $q = $CI->db->query('Select total_stock as total from total_stock where product_id = "' . $id . '"');
        if ($q->num_rows() > 0) {
            foreach ($q->result() as
                    $r) {
                if (!empty($r->total)) {
                    return $r->total;
                }
                else {
                    return 0;
                }
            }
        }
        else {
            return 0;
        }
    }

    public static function checkbox_products($id) {
        $CI = & get_instance();

        $CI->db->order_by('cat_id', 'ASC');
        $CI->db->where('cat_id', $id);
        $query = $CI->db->get('products');
        foreach ($query->result() as
                $r) {
            $count = AdminLTE::stock_count($r->id);
            $label = "";
            if ($count <= 0) {
                continue;
            }
            else {

                echo "<div class='col-xs-12'  style='line-height: 30px'>"
                . "<input type='checkbox' $label name='product[$r->id]' value='$r->id'/> <b>$r->name - " . AdminLTE::cat_name($r->cat_id)
                . "</b>"
                . ""
                . "<br><input type='number' $label name='sale[$r->id]' class='col-xs-8 col-sm-6'/>    &nbsp; <span style='font-size: 12px;'>Stock Available: " . AdminLTE::stock_count($r->id) . "</span></div>";
            }
        }
        return;
    }

   
    public static function sale_count1($id, $table) {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(sale) as total from ' . $table . ' where product_id = "' . $id . '"');

        foreach ($q->result() as
                $r) {

            echo $r->total;
        }
    }

    public static function category_name($product_id) {
        $CI = & get_instance();
        $q = $CI->db->query('Select cat_id as total from products where id = "' . $product_id . '"');

        foreach ($q->result() as
                $r) {

            return $r->total;
        }
    }

    public static function name($id) {
        $CI = & get_instance();
        $q = $CI->db->query('Select fullname from customers where id = "' . $id . '"');

        foreach ($q->result() as
                $r) {

            echo $r->fullname;
        }
    }

    public static function sale_count_shop($id, $table) {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(sale) as total from ' . $table . ' where inv_id = "' . $id . '"');

        foreach ($q->result() as
                $r) {

            echo $r->total;
        }
    }

    public static function profit_date($date) {
        $CI = & get_instance();
        $q = $CI->db->query('Select price, sprice, sale, inv_id from sale where date ="' . $date . '"');
        $total = 0;

        foreach ($q->result() as
                $r) {

            $total += $r->sale * ($r->price - $r->sprice);
        }
        
       return round($total, 2);
    }

    public static function expanse_date($date) {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(amount) as amount from expenses where date ="' . $date . '"');
        $total = 0;

        foreach ($q->result() as
                $r) {

            $total += $r->amount;
        }
        return round($total, 2);
    }

     public static function vendor_discount($date) {
        $CI = & get_instance();
        $q = $CI->db->query('Select SUM(discount) as disc from invoices_vendor where date ="' . $date . '"');
        $total = 0;

        foreach ($q->result() as
                $r) {

            $total += $r->disc;
        }
        return round($total, 2);
    }
    
    public static function table_data_onefield($table, $Onefield = Null, $arraywhere = NULL) {
        $CI = & get_instance();
        $CI->db->select($Onefield);
        $array = $arraywhere;
        $CI->db->where($array);
        $query = $CI->db->get($table);

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0][$Onefield];
        }
        else {
            return FALSE;
        }
    }

    public static function products($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('products');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->name" . " - " . AdminLTE::cat_name($r->cat_id) . " </option>";
        }
        return $data;
    }

    public static function products_sale($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('products');
        foreach ($query->result() as
                $r) {
            $count = AdminLTE::stock_count($r->id);

            if ($count <= 0) {
                continue;
            }
            else {
                $c = $cur == $r->id ? "selected=''" : "";
                $data .= "<option value='$r->id' $c> $r->name" . " - " . AdminLTE::cat_name($r->cat_id) . " </option>";
            }
        }
        return $data;
    }

    public static function vendors($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('vendor');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->fullname </option>";
        }
        return $data;
    }

    public static function banks($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('banks');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->bank_name </option>";
        }
        return $data;
    }

    public static function category($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('category');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->name </option>";
        }
        return $data;
    }

    public static function Shopkeepers($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('customers');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->fullname </option>";
        }
        return $data;
    }

    public static function Invoices($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('invoice');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected=''" : "";
            $data .= "<option value='$r->id' $c> $r->invoice_id </option>";
        }
        return $data;
    }

    public static function fetch($id) {
        $CI = & get_instance();
        $q = $CI->db->get($id);
        return $q->result();
    }

    public static function customers($id) {
        $CI = & get_instance();
        $CI->db->select('fullname');
        $array = array(
            'status' => 1,
            'id' => $id);
        $CI->db->where($array);

        $query = $CI->db->get('customers');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['fullname'];
        }
        else {
            return FALSE;
        }
    }

    public static function bank_name($id) {
        $CI = & get_instance();
        $CI->db->select('bank_name');
        $array = array(
            'id' => $id);
        $CI->db->where($array);

        $query = $CI->db->get('banks');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['bank_name'];
        }
        else {
            return FALSE;
        }
    }

    public static function customers_name($id) {
        $CI = & get_instance();
        $CI->db->select('shop_id');
        $array = array(
            'invoice_id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('invoice');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['shop_id'];
        }
        else {
            return FALSE;
        }
    }

    public static function customers_phone($id) {
        $CI = & get_instance();
        $CI->db->select('phone');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('customers');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['phone'];
        }
        else {
            return FALSE;
        }
    }

    public static function biltyno($id) {
        $CI = & get_instance();
        $CI->db->select('biltyno');
        $array = array(
            'inv_id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('stock');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['biltyno'];
        }
        else {
            return FALSE;
        }
    }
    
      public static function billno_sale($id) {
        $CI = & get_instance();
        $CI->db->select('billno');
        $array = array(
            'inv_id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('sale');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['billno'];
        }
        else {
            return FALSE;
        }
    }

   

    public static function vendor_name($id) {
        $CI = & get_instance();
        $CI->db->select('fullname');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('vendor');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['fullname'];
        }
        else {
            return FALSE;
        }
    }

    public static function sprice($id) {
        $CI = & get_instance();
        $CI->db->select('sprice');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('products');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['sprice'];
        }
        else {
            return FALSE;
        }
    }
	
	public static function hprice($id) {
        $CI = & get_instance();
        $CI->db->select('hprice');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('products');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['hprice'];
        }
        else {
            return FALSE;
        }
    }

    public static function pprice($id) {
        $CI = & get_instance();
        $CI->db->select('pprice');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('products');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['pprice'];
        }
        else {
            return FALSE;
        }
    }

    public static function discount($date) {
        $CI = & get_instance();

        
        $query = $CI->db->query('Select SUM(discount) as discount from book_detail where date ="' . $date . '"');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['discount'];
        }
        else {
            return FALSE;
        }
    }
    
     public static function discount_month($date) {
        $CI = & get_instance();

         $year = date("Y");
        $query = $CI->db->query('Select SUM(discount) as discount from book_detail where YEAR(date) = "' . $year . '" and MONTH(date) ="' . $date . '"');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['discount'];
        }
        else {
            return FALSE;
        }
    }
    
     public static function discount_sale($id) {
        $CI = & get_instance();

        
        $query = $CI->db->query('Select SUM(discount) as discount from invoices where inv_id ="' . $id . '"');
        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return  $result[0]['discount'];
        }
        else {
            return FALSE;
        }
    }

    public static function debit($id) {
        $CI = & get_instance();
        $CI->db->select('debit');
        $array = array(
            'shop_id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('book');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['debit'];
        }
        else {
            return 0;
        }
    }

    public static function debit_vendor($id) {
        $CI = & get_instance();
        $CI->db->select('debit');
        $array = array(
            'shop_id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('book_vendor');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['debit'];
        }
        else {
            return 0;
        }
    }

    public static function cat_name($id) {
        $CI = & get_instance();
        $CI->db->select('name');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('category');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['name'];
        }
        else {
            return FALSE;
        }
    }

    public static function product_name($id) {
        $CI = & get_instance();
        $CI->db->select('name');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('products');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['name'];
        }
        else {
            return FALSE;
        }
    }

 
    public static function fetchall($table, $arr = NULL, $range = NULL, $where = NULL, $groupby = NULL, $order = NULL, $gJoin = NULL) {
        $CI = & get_instance();
        if ($range) {
            $CI->db->select($range, false);
        }
        else {
            $CI->db->select('*', false);
        }
        if ($arr) {
            $CI->db->join($arr['table'], $table . '.' . $arr['id'] . ' = ' . $arr['table'] . '.' . 'id', 'left outer');
        }
        if ($gJoin) {
            $CI->db->join($gJoin['table'], $table . '.' . $gJoin['fID'] . '=' . $gJoin['table'] . '.' . $gJoin['sID'], 'left');
        }
        if ($where) {
            $CI->db->where($where, '', FALSE);
        }
        if ($groupby) {
            $CI->db->group_by($groupby);
        }

        if ($order) {
            $CI->db->order_by($order['id'], $order['order']);
        }
        $records = $CI->db->get($table)->result_array();
        //echo $CI->db->last_query(); exit;
        //echo "<!--".$CI->db->last_query()."-->";
        return $records;
    }

    

    public static function bank_entry($bank, $deposit, $withdraw, $cheque, $date, $shop) {
        $CI = & get_instance();
        $CI->db->insert(
                'bank_details', [
            'bank_id' => $bank,
            'deposit' => $deposit,
            'withdraw' => $withdraw,
            'cheque' => $cheque,
            'date' => $date,
            'shop_id' => $shop
                ]
        );
    }



    public static function sms($no, $msg) {


        $id = "";
        $pass = "";
        $mask = "";
        $lang = "English";
// Prepare data for POST request
        $data = "id=" . $id . "&pass=" . $pass . "&mask=" . $mask . "&to=" . $no . "&lang=" . $lang . "&msg=" . $msg;

        //  http://outreach.pk/api/sendsms.php/sendsms/url?id=rchyouthins&pass=webnaxtor1&mask=YDI&to=923339471086&lang=English&msg=Hello&type=xml
// Send the POST request with cURL

        $ch = curl_init('http://www.outreach.pk/api/sendsms.php/sendsms/url');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responce = curl_exec($ch);
        curl_close($ch);
        if ($responce) {
            set_flash_alert($responce, 'success');
        }
    }

    public static function sms_balance() {

        $id = "";
        $pass = "";
        $url = "http://outreach.pk/api/sendsms.php/balance/status?id=$id&pass=$pass";
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $response = curl_exec($ch);
        curl_close($ch);
//Write out the response
        echo "Remaining Balance:" . $response;
    }

    public static function expense_names($cur = "") {
        $CI = & get_instance();
        $query = $CI->db->get('expense_names');
        foreach ($query->result() as
                $r) {
            $c = $cur == $r->id ? "selected = ''" : "";
            $data .= "<option value = '$r->id' $c> $r->exp_name </option>";
        }
        return $data;
    }

    public static function exp_name($id) {
        $CI = & get_instance();
        $CI->db->select('exp_name');
        $array = array(
            'id' => $id);
        $CI->db->where($array);
        $query = $CI->db->get('expense_names');

        if ($query->num_rows() == 1) {
            $result = $query->result_array();
            return $result[0]['exp_name'];
        }
        else {
            return FALSE;
        }
    }

}
