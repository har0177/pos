<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ims extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	
	 public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        // Set Template parts
        $this->template->baseView('master/app');
        $this->template->assign('user', get_user_info());
        $this->template->assign('heading', 'Users');
    }
    
	public function index()
	{
	
        // Check existing session
        if ($this->session->has_userdata('user_logged')) {
            redirect('/admin');
        }

        $login = $this->user->login();
        if ($login) {
            redirect('/admin');
        }
        // Load template data
        $this->template->title('Login');
        $this->template->display('user/login');
    
    
	}

	
}
