<?php

class Expiry extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */
        
        $this->load->model('expiry_model', 'expiry');
        // Set Template parts
        $this->template->baseView('master/app');
        $this->template->assign('expiry', get_user_info());
        $this->template->assign('heading', 'Expiry');
    }

  
    public function index()
    {
        auth_user();
     
        $this->template->assign('result', $this->expiry->all_invoice());
        $this->template->addView('expiry/all', 'body');
        $this->template->display();
    }
    
    
      public function invoice_printing($id)
    {
       $result = $this->expiry->invoice_print($id);
       $result1 = $this->expiry->all_invoices($id);
        if(empty($result) && empty($result1)){
            show_404();
        }
        $this->template->assign('r',$result);
        $this->template->assign('totalamount',$result1[0]);
        $this->template->assign('paid',$result1[0]);
        $this->template->assign('old',$result1[0]);
          $this->template->assign('type_payment',$result1[0]);
        $this->template->assign('discount',$result1[0]);
         $this->template->assign('bilty',$result1[0]);
        $this->template->assign('net',$result1[0]);
        $this->template->assign('date',$result1[0]);
        $this->template->assign('invoice',$id);
        $this->template->addView('expiry/invoice', 'body');
        $this->template->display();
    }
    
     public function printing($id)
    {
     
           auth_user();
         if(!auth_admin()){
            show_404();
        }
        if($this->expiry->book_return($id)){
        
            redirect("admin/expiry/invoice/".$id);
        }
    }
    
    public function invoice($id) {
        $result = $this->expiry->invoice_return($id);
             $result1 = $this->expiry->bookdetails(AdminLTE::customers_name($id));
         
              if(empty($result)){
                  set_flash_alert("No Expiry Invoice Found", 'danger');
                  redirect("admin/expiry");
        }
        
        $this->template->assign('r',$result);
        $this->template->assign('date',$result[0]);
        $this->template->assign('total',$result1[0]);
        $this->template->assign('debit',$result1[0]);
        $this->template->assign('credit',$result1[0]);
        
        $this->template->assign('invoice',$id);
        $this->template->addView('expiry/printing', 'body');
        $this->template->display();
    }
    
   
        public function total()
    {
     
     
        $this->template->assign('result', $this->expiry->total());
        $this->template->addView('expiry/total', 'body');
        $this->template->display();
    }
    
     public function total_shop()
    {
     
     
        $this->template->assign('result', $this->expiry->total_shop());
        $this->template->addView('expiry/total_shop', 'body');
        $this->template->display();
    }
    
   
    public function add()
    {
        auth_user();
         if(!auth_admin()){
            show_404();
        }
        
        // Create expiry
        if($this->expiry->create()){
            redirect('admin/expiry');
        }
        
  
        $this->template->addView('expiry/create', 'body');
        $this->template->display();
    }
    
    public function edit($id)
    {
        auth_user();
         if(!auth_admin()){
            show_404();
        }
        
        // Update Data
        if($this->expiry->update($id)){
            redirect('admin/expiry');
        }
       
        // Get expiry data
        $result = $this->expiry->find($id);
        if(empty($result)){
            show_404();
        }
        $this->template->assign('result',$result[0]);
        $this->template->addView('expiry/edit', 'body');
        $this->template->display();
    }
    
    public function delete($id)
    {
        if(!auth_admin()){
            show_404();
        }
        
        $this->expiry->delete($id);
        redirect('admin/expiry');
    }
    
 
}
