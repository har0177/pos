<?php

class Sale extends CI_Controller {

    public function __construct() {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */

        $this->load->model('sale_model', 'sale');
        $this->load->model('products_model', 'products');
        // Set Template parts
        $this->template->baseView('master/app');

        $this->template->assign('heading', 'Sale');
    }

    public function index() {


        $this->template->assign('result', $this->sale->all());
        $this->template->addView('sale/all', 'body');
        $this->template->display();
    }

    public function invoice_vendor() {


        $this->template->assign('result', $this->sale->all_invoice_vendor());
        $this->template->addView('invoice/vendor', 'body');
        $this->template->display();
    }

    public function invoice() {


        $this->template->assign('result', $this->sale->all_invoice());
        $this->template->addView('invoice/all', 'body');
        $this->template->display();
    }

    
    public function invoice_printing($id) {
        $result = $this->sale->invoice_print($id);
        $result1 = $this->sale->all_invoices($id);
        if (empty($result) && empty($result1)) {
            show_404();
        }
        $this->template->assign('r', $result);
        $this->template->assign('billno', $result[0]);
        $this->template->assign('totalamount', $result1[0]);
        $this->template->assign('paid', $result1[0]);
        $this->template->assign('old', $result1[0]);
        $this->template->assign('type_payment', $result1[0]);
        $this->template->assign('discount', $result1[0]);
         $this->template->assign('dis_per', $result1[0]);
        $this->template->assign('net', $result1[0]);
        $this->template->assign('date', $result1[0]);
        $this->template->assign('comments', $result1[0]);
        $this->template->assign('invoice', $id);
        $this->template->addView('sale/invoice', 'body');
        $this->template->display();
    }

    public function invoice_show() {
        $id = $this->input->post("id", TRUE);
        $result = $this->sale->invoice_print($id);
        $result1 = $this->sale->all_invoices($id);
        if (empty($result) && empty($result1)) {
            show_404();
        }

        $data['r'] = $result;
        $data['billno'] = $result[0];
        
        $data['totalamount'] = $result1[0];
        $data['paid'] = $result1[0];
        $data['type_payment'] = $result1[0];
        $data['discount'] = $result1[0];
        $data['dis_per'] = $result1[0];
        $data['old'] = $result1[0];
        $data['net'] = $result1[0];
        $data['date'] = $result1[0];
        $data['comments'] = $result1[0];
        $data['invoice'] = $id;
        $data1 = $this->load->view('sale/printing', $data, TRUE);
        echo json_encode($data1);
    }

    
    public function search() {

        $this->template->addView('sale/search', 'body');
        $this->template->display();
    }
	
	public function search_cus() {

        $this->template->addView('sale/search_cus', 'body');
        $this->template->display();
    }

    public function add() {

$this->template->assign('result', $this->products->all());
        $this->template->addView('sale/create', 'body');
        $this->template->display();
    }

 public function add_holesale() {
$this->template->assign('result', $this->products->all());
      
        $this->template->addView('sale/holesale', 'body');
        $this->template->display();
    }

   
    public function total() {

        $from = $this->input->post('from');
        $to = $this->input->post('to');
        $this->template->assign('from', $from);
        $this->template->assign('to', $to);

        $data = $this->sale->getReport_cus();
        if (empty($data)) {
            redirect('admin/sale/search_cus');
        }
        $this->template->assign('result', $data);
        $this->template->addView("sale/total", 'body');
        $this->template->display();
        // //load the view and saved it into $html variable
        // $html=$this->load->view('fee/print_all', $data, true);
        // //this the the PDF filename that user will get to download
        // $pdfFilePath = "feeSlips.pdf";
        // //load mPDF library
        // $this->load->library('m_pdf');
        // $mpdf = new mPDF('utf-8', 'A4');
        // //generate the PDF from the given html
        // $mpdf->WriteHTML($html);
        // //download it.
        // $mpdf->Output($pdfFilePath, "D");  
    }

   

    public function edit_invoice($id) {

        // Update Data
        if ($this->sale->update($id)) {
            redirect('admin/sale');
        }

        // Get sale data
        $result = $this->sale->find($id);
        if (empty($result)) {
            show_404();
        }
        $this->template->assign('result', $result[0]);
        $this->template->addView('sale/edit', 'body');
        $this->template->display();
    }

    public function delete() {

        $id  = $this->input->post("id", TRUE);
        $status = $this->sale->delete($id);
        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "status" => $status));
    }

    
    public function report() {
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        $this->template->assign('from', $from);
        $this->template->assign('to', $to);

        $data = $this->sale->getReport();
        if (empty($data)) {
            redirect('admin/sale/search');
        }
        $this->template->assign('result', $data);
        $this->template->addView("sale/report", 'body');
        $this->template->display();
        // //load the view and saved it into $html variable
        // $html=$this->load->view('fee/print_all', $data, true);
        // //this the the PDF filename that user will get to download
        // $pdfFilePath = "feeSlips.pdf";
        // //load mPDF library
        // $this->load->library('m_pdf');
        // $mpdf = new mPDF('utf-8', 'A4');
        // //generate the PDF from the given html
        // $mpdf->WriteHTML($html);
        // //download it.
        // $mpdf->Output($pdfFilePath, "D");  
    }

    
    public function update($id) {
        
       $result = $this->sale->invoice_print($id);
        $result1 = $this->sale->all_invoices($id);
        if (empty($result) && empty($result1)) {
            show_404();
        }
        $this->template->assign('re', $result);
        $this->template->assign('totalamount', $result1[0]);
        $this->template->assign('paid', $result1[0]);
        $this->template->assign('old', $result1[0]);
        $this->template->assign('type_payment', $result1[0]);
        $this->template->assign('discount', $result1[0]);
        $this->template->assign('net', $result1[0]);
        $this->template->assign('date', $result1[0]);
        $this->template->assign('invoice', $id);
        
$this->template->assign('result', $this->products->all());

        $this->template->addView('sale/edit', 'body');
        $this->template->display();
    }
    
    
    public function add_sale() {
        $data = $this->input->post("data_table");
        $shop = $this->input->post("shop");
        $cus_new = $this->input->post("shop_new");
        $cus_new_mobile = $this->input->post("mobile");
        $date = $this->input->post("date");
        $status = $this->sale->add_sale($data, $shop, $cus_new, $cus_new_mobile, $date);

        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "status" => $status));
    }
    
     public function update_sale() {
        $data = $this->input->post("data_table");
        $status = $this->sale->update_sale($data);

        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "status" => $status));
    }

    public function oldBalance() {
        $postData = $this->input->post("id", TRUE);
        $data = array(
            'old' => AdminLTE::debit($postData)
        );

        echo json_encode($data);
    }

}
