<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Banks extends CI_Controller {

    public function __construct() {
        parent::__construct();


        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */

        $this->load->model('banks_model', 'bank');
        $this->template->baseView('master/app');

        $this->template->assign('heading', 'Bank Details');
    }

    public function index() {




        $result = $this->bank->all();
        $this->template->assign('result', $result);
        $this->template->addView("banks/all_banks", 'body');
        $this->template->display();
    }

    public function all_details() {




        $result = $this->bank->all_details();
        $this->template->assign('result', $result);
        $this->template->addView("banks/all", 'body');
        $this->template->display();
    }

    public function details($id) {
        $this->template->assign('bank', $id);
        $this->template->addView("banks/details", 'body');
        $this->template->display();
    }

    public function transfer() {


        // Create category
        if ($this->bank->transfer()) {
            redirect('admin/banks/all_details');
        }

        $this->template->addView("banks/transfer", 'body');
        $this->template->display();
    }

    public function edit_stat($id) {

        if ($this->bank->edit_stat($id)) {
            redirect('admin/banks/all_details');
        }
        $result = $this->bank->find_details($id);
        if (empty($result)) {
            show_404();
        }

        $this->template->assign('r', $result[0]);
        $this->template->addView("banks/edit_stat", 'body');
        $this->template->display();
    }

    public function add_payment() {


        // Create category
        if ($this->bank->add_payment()) {
            redirect('admin/banks/all_details');
        }

        $this->index();
    }

    public function add_bank() {

        if ($this->bank->add_bank()) {
            redirect('admin/banks/add_bank');
        }
        redirect("admin/banks");
    }

    public function edit_bank($id) {

        if ($this->bank->edit_bank($id)) {
            redirect('admin/banks/add_bank');
        }
        $result = $this->bank->find_name($id);
        if (empty($result)) {
            show_404();
        }

        $this->template->assign('r', $result[0]);
        $this->template->addView("banks/edit_banks", 'body');
        $this->template->display();
    }

}
