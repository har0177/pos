<?php

class Book extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */
        
        $this->load->model('book_model', 'book');
         $this->load->model('customer_model', 'customer');
           $this->load->model('vendor_model', 'vendor');
        // Set Template parts
        $this->template->baseView('master/app');
   
        $this->template->assign('heading', 'Amount Book');
    }

       public function invoices($id)
    {
        
    
         $result = $this->customer->find($id);
        $this->template->assign('data', $result[0]);
          $this->template->assign('book', $this->customer->book_details($id));
        $this->template->addView('book/all_invoices', 'body');
        $this->template->display();
    }
    
       public function invoices_vendor($id)
    {
        
    
         $result = $this->vendor->find($id);
        $this->template->assign('data', $result[0]);
         $this->template->assign('book', $this->vendor->book_details($id));
        $this->template->addView('book/all_invoices_vendor', 'body');
        $this->template->display();
    }
    
  
    public function index()
    {
     
     
        $this->template->assign('result', $this->book->all());
        $this->template->addView('book/all', 'body');
        $this->template->display();
    }
    
      public function vendor()
    {
        
     
        $this->template->assign('result', $this->book->vendor());
        $this->template->addView('book/vendor', 'body');
        $this->template->display();
    }
    
     public function datewise()
    {
        
     
        $this->template->assign('result', $this->book->datewise());
        $this->template->addView('book/date_all', 'body');
        $this->template->display();
    }
    public function add()
    {
        
        
        
        // Create book
        if($this->book->create()){
            redirect('admin/book');
        }
        
        $this->template->addView('book/create', 'body');
        $this->template->display();
    }
    
    
    public function add_vendor()
    {
        
        
        
        // Create book
        if($this->book->create_vendor()){
            redirect('admin/book/vendor');
        }
        
        $this->template->addView('book/create_vendor', 'body');
        $this->template->display();
    }
    
      public function edit_cheque($id)
    {
        // Update Data
        if($this->book->edit_cheque($id)){
            redirect('admin/book');
        }
       
        // Get book data
        $result = $this->book->find_details($id);
        if(empty($result)){
            show_404();
        }
        $this->template->assign('result',$result);
        $this->template->addView('book/edit_details', 'body');
        $this->template->display();
    }
    
    
    public function edit($id)
    {
        
         
        
        // Update Data
        if($this->book->update($id)){
            redirect('admin/book');
        }
       
        // Get book data
        $result = $this->book->find($id);
        if(empty($result)){
            show_404();
        }
        $this->template->assign('result',$result[0]);
        $this->template->addView('book/edit', 'body');
        $this->template->display();
    }
    
    public function edit_vendor($id)
    {
        
        
        
        // Update Data
        if($this->book->update_vendor($id)){
            redirect('admin/book/vendor');
        }
       
        // Get book data
        $result = $this->book->find_vendor($id);
        if(empty($result)){
            show_404();
        }
        $this->template->assign('result',$result[0]);
        $this->template->addView('book/edit_vendor', 'body');
        $this->template->display();
    }
    
    public function delete($id)
    {
        
        
        $this->book->delete($id);
        redirect('admin/book');
    }
    
    public function activate($id)
    {
        $this->book->activate($id);
        redirect('admin/book');
    }
    
    public function deactivate($id)
    {
        $this->book->deactivate($id);
        redirect('admin/book');
    }
}
