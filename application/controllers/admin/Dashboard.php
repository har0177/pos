<?php

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */
        
 
        /* if(!auth_admin()){
          show_404();
          } */
        $this->load->model('dashboard_model', 'dashboard');
         $this->template->baseView('master/app');
  
        $this->template->assign('heading', 'Dashboard');
    }

    public function index() {

        
        $this->template->addView("dashboard/all", 'body');
        $this->template->display();
    }
    
     public function reports() {
         $shop = $this->dashboard->shop();
        $this->template->assign('shop', $shop);

        $products = $this->dashboard->products();
        $this->template->assign('products', $products);
        
      $category = $this->dashboard->category();
        $this->template->assign('category', $category);
        
         $vendor = $this->dashboard->vendor();
        $this->template->assign('vendor', $vendor);
        
        
        $this->template->assign('result', $this->dashboard->top5sale());
   
         $this->template->addView("dashboard/reports", 'body');
        $this->template->display();
     }
     
      public function search() {
         $this->template->addView("dashboard/search", 'body');
        $this->template->display();
     }

}
