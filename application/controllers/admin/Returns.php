<?php

class Returns extends CI_Controller {

    public function __construct() {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */

        $this->load->model('returns_model', 'returns');
        // Set Template parts
        $this->template->baseView('master/app');

        $this->template->assign('heading', 'Returns');
    }

    public function index() {

        $this->template->assign('result', $this->returns->all_invoice());
        $this->template->addView('returns/all', 'body');
        $this->template->display();
    }

    public function invoice_printing($id) {
        $result = $this->returns->invoice_print($id);
        $result1 = $this->returns->all_invoices($id);
        if (empty($result) && empty($result1)) {
            show_404();
        }
        $this->template->assign('r', $result);
        $this->template->assign('totalamount', $result1[0]);
        $this->template->assign('paid', $result1[0]);
        $this->template->assign('old', $result1[0]);
        $this->template->assign('type_payment', $result1[0]);
        $this->template->assign('discount', $result1[0]);
        $this->template->assign('bilty', $result1[0]);
        $this->template->assign('net', $result1[0]);
        $this->template->assign('date', $result1[0]);
        $this->template->assign('invoice', $id);
        $this->template->addView('returns/invoice', 'body');
        $this->template->display();
    }

    public function printing($id) {

        if ($this->returns->book_return($id)) {

            redirect("admin/returns/invoice/" . $id);
        }
    }

    public function invoice($id) {
        $result = $this->returns->invoice_return($id);

        if (empty($result)) {
            set_flash_alert("No Return Invoice Found", 'danger');
            redirect("admin/returns");
        }
        $result1 = $this->returns->bookdetails(AdminLTE::customers_name($id));

        $this->template->assign('r', $result);
        $this->template->assign('date', $result[0]);
        $this->template->assign('total', $result1[0]);
        $this->template->assign('debit', $result1[0]);
        $this->template->assign('credit', $result1[0]);

        $this->template->assign('invoice', $id);
        $this->template->addView('returns/printing', 'body');
        $this->template->display();
    }

    public function total() {


        $this->template->assign('result', $this->returns->total());
        $this->template->addView('returns/total', 'body');
        $this->template->display();
    }

    public function total_shop() {


        $this->template->assign('result', $this->returns->total_shop());
        $this->template->addView('returns/total_shop', 'body');
        $this->template->display();
    }

    public function add() {

        $this->template->addView('returns/create', 'body');
        $this->template->display();
    }

    public function edit($id) {


        // Update Data
        if ($this->returns->update($id)) {
            redirect('admin/returns');
        }

        // Get returns data
        $result = $this->returns->find($id);
        if (empty($result)) {
            show_404();
        }
        $this->template->assign('result', $result[0]);
        $this->template->addView('returns/edit', 'body');
        $this->template->display();
    }

    public function invoice_show() {
        $id = $this->input->post("id", TRUE);

        $result = $this->returns->invoice_return($id);

        if (empty($result)) {
            set_flash_alert("No Return Invoice Found", 'danger');
            redirect("admin/returns");
        }
        $result1 = $this->returns->bookdetails(AdminLTE::customers_name($id));

        $this->template->assign('r', $result);
        $this->template->assign('date', $result[0]);
        $this->template->assign('total', $result1[0]);
        $this->template->assign('debit', $result1[0]);
        $this->template->assign('credit', $result1[0]);

        $data['r'] = $result;

        $data['total'] = $result1[0];
        $data['debit'] = $result1[0];
        $data['credit'] = $result1[0];
        $data['date'] = $result[0];
        $data['invoice'] = $id;
        $data1 = $this->load->view('returns/printing', $data, TRUE);
        echo json_encode($data1);
    }

    public function delete($id) {


        $this->returns->delete($id);
        redirect('admin/returns');
    }

    public function add_return() {
        $data = $this->input->post("data_table");
        
        $status = $this->returns->add_returns($data);

        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "status" => $status));
    }

}
