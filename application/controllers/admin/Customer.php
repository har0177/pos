<?php

class Customer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */
        
        $this->load->model('customer_model', 'customer');
        // Set Template parts
        $this->template->baseView('master/app');
    
        $this->template->assign('heading', 'Customers');
    }

  
      public function invoices()
    {
        
     $id = $this->input->post("id", TRUE);
         $result = $this->customer->find($id);
      
          $data1 = array(
            'data' => $result[0],
            'result' => $this->customer->all_invoice($id),
              'book' => $this->customer->book_details($id)
        );

         $data = $this->load->view('customer/all_invoices', $data1, TRUE);
    
        echo json_encode($data);
    }
    
    
    public function index()
    {
        
     $status = $this->customer->status();
       $this->template->assign('status',$status);
        $this->template->assign('result', $this->customer->all());
        $this->template->addView('customer/all', 'body');
        $this->template->display();
    }
    
   
    public function add()
    {
        
     
        
        // Create customer
        if($this->customer->create()){
            redirect('admin/customer');
        }
         
        $this->template->addView('customer/all', 'body');
        $this->template->display();
    }
    
    public function edit($id)
    {
        
        // Update Data
        if($this->customer->update($id)){
            redirect('admin/customer');
        }
         $status = $this->customer->status();
       $this->template->assign('status',$status);
        // Get customer data
        $result = $this->customer->find($id);
        if(empty($result)){
            show_404();
        }
        $this->template->assign('result',$result[0]);
        $this->template->addView('customer/edit', 'body');
        $this->template->display();
    }
    
    public function delete($id)
    {
        
        
        $this->customer->delete($id);
        redirect('admin/customer');
    }
    
    public function activate($id)
    {
        $this->customer->activate($id);
        redirect('admin/customer');
    }
    
    public function deactivate($id)
    {
        $this->customer->deactivate($id);
        redirect('admin/customer');
    }
}
