<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Sms
        extends CI_Controller {

    public function __construct() {
        parent::__construct();

        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */
        
            $this->template->baseView('master/app');
        
        $this->load->model('sms_model', 'sms');
        $this->template->assign('user', get_user_info());
        $this->template->assign('heading', 'Send SMS');
    }

    public function index() {
        auth_user();
        $sender = $this->sms->singlesms();
        $this->template->assign('sender', $sender);
        $this->template->addView("sms/sms", 'body');
        $this->template->display();
    }
    
       public function logs()
    {
        $this->template->addView("sms/logs",'body');
        $this->template->display();           
    }

    public function create() {
        auth_user();
        if ($this->sms->create()) {
            redirect('admin/sms');
        }
        $sender = $this->sms->singlesms();
        $this->template->assign('sender', $sender);
        $this->template->addView("sms/sms", 'body');
        $this->template->display();
    }

    public function single() {
        auth_user();
        if ($this->sms->single()) {
            redirect('admin/sms');
        }
        $this->template->addView("sms/single", 'body');
        $this->template->display();
    }

    public function bulk() {
        auth_user();
        if ($this->sms->bulk()) {
            redirect('admin/sms/bulk');
        }

        $this->template->addView("sms/bulk", 'body');
        $this->template->display();
    }

    public function customer() {
        auth_user();
        if ($this->sms->customer()) {
            redirect('admin/sms/customer');
        }

        $this->template->addView("sms/customer", 'body');
        $this->template->display();
    }
    
    
    public function vendor() {
        auth_user();
        if ($this->sms->vendor()) {
            redirect('admin/sms/vendor');
        }

        $this->template->addView("sms/vendor", 'body');
        $this->template->display();
    }
    
    public function all_sms() {
        auth_user();
        $this->template->assign('heading', "SMS");
        $this->template->assign('result', $this->sms->all_sms());
        $this->template->addView('smstemp/all', 'body');
        $this->template->display();
    }

    public function add_sms() {
        auth_user();

        // Create user
        if ($this->sms->add_sms()) {
            redirect('admin/sms/all_sms');
        }
        $this->template->assign('heading', "SMS");
        $this->template->addView('smstemp/create', 'body');
        $this->template->display();
    }

    public function edit_sms($id) {
        auth_user();


        // Update Data
        if ($this->sms->edit_sms($id)) {
            redirect('admin/sms/all_sms');
        }

        $result = $this->sms->find_sms($id);
        if (empty($result)) {
            show_404();
        }
        $this->template->assign('heading', "SMS");
        $this->template->assign('r', $result[0]);
        $this->template->addView('smstemp/edit', 'body');
        $this->template->display();
    }

    public function delete_sms($id) {

        $this->sms->delete_sms($id);
        redirect('admin/sms/all_sms');
    }



}
