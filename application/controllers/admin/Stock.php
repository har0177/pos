<?php

class Stock extends CI_Controller {

    public function __construct() {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */

        $this->load->model('stock_model', 'stock');
        $this->load->model('products_model', 'products');
        $this->load->model('sale_model', 'sale');
        // Set Template parts
        $this->template->baseView('master/app');

        $this->template->assign('heading', 'Stock');
    }

    public function invoice_show() {
        $id = $this->input->post("id", TRUE);
        $result = $this->stock->invoice_print($id);
        $result1 = $this->stock->all_invoices($id);
        if (empty($result) && empty($result1)) {
            show_404();
        }
        $data['r'] = $result;
        $data['totalamount'] = $result1[0];
        $data['paid'] = $result1[0];
        $data['type_payment'] = $result1[0];
        $data['discount'] = $result1[0];
        $data['old'] = $result1[0];
        $data['bill'] = $result[0];
        $data['biltyno'] = $result[0];
        $data['bilty'] = $result1[0];
        $data['net'] = $result1[0];
        $data['date'] = $result1[0];
        $data['comments'] = $result1[0];
        $data['invoice'] = $id;
        $data1 = $this->load->view('stock/printing', $data, TRUE);
        echo json_encode($data1);
    }

    public function invoice_printing($id) {

        $result = $this->stock->invoice_print($id);
        $result1 = $this->stock->all_invoices($id);
        if (empty($result) && empty($result1)) {
            show_404();
        }
        $this->template->assign('r', $result);
        $this->template->assign('totalamount', $result1[0]);
        $this->template->assign('paid', $result1[0]);
        $this->template->assign('type_payment', $result1[0]);
        $this->template->assign('discount', $result1[0]);
        $this->template->assign('old', $result1[0]);
        $this->template->assign('bill', $result[0]);
        $this->template->assign('biltyno', $result[0]);
        $this->template->assign('bilty', $result1[0]);
        $this->template->assign('net', $result1[0]);
        $this->template->assign('date', $result1[0]);
        $this->template->assign('comments', $result1[0]);
        $this->template->assign('invoice', $id);
        $this->template->addView('stock/invoice', 'body');
        $this->template->display();
    }

    public function printing($id) {
        $result = $this->stock->invoice_print($id);
        $bilty = $this->input->post('bilty_discount');
        $this->stock->bookadd($id);
        if (empty($result)) {
            show_404();
        }

        $old = $this->input->post('old');
        $bill = $this->input->post('bill');
        $biltyno = $this->input->post('biltyno');
        $paid = $this->input->post('paid');
        if (empty($paid)) {
            $paid = 0;
        }
        $bank_name = $this->input->post('banking');
        $bank = $this->input->post('bank');
        $name = AdminLTE::bank_name($bank_name);
        $type_payment = "";
        if ($bank_name == "hand") {
            $type_payment = "By Hand";
        }
        else {
            $type_payment = $name . " - " . $bank;
        }
        $this->template->assign('bank', $type_payment);
        $this->template->assign('old', $old);

        $discount_type = $this->input->post('dis');
        $bilty_type = $this->input->post('bilty');
        $discount = $this->input->post('discount');



        $this->template->assign('paid', $paid);
        $this->template->assign('discount', $discount);
        $this->template->assign('bilty', $bilty);
        $this->template->assign('bill', $bill
        );
        $this->template->assign('biltyno', $biltyno
        );
        $this->template->assign('dis_type', $discount_type);
        $this->template->assign('bil_type', $bilty_type);
        $this->template->assign('r', $result);
        $this->template->assign('date', $result[0]);
        $this->template->assign('invoice', $id);
        $this->template->addView('stock/printing', 'body');
        $this->template->display();
    }

    public function print_invoice($id) {
        $result = $this->stock->invoice_print($id);
        if (empty($result)) {
            show_404();
        }
        $this->template->assign('r', $result);
        $this->template->assign('date', $result[0]);
        $this->template->assign('bill', $result[0]);
        $this->template->assign('biltyno', $result[0]);
        $this->template->assign('invoice', $id);
        $this->template->addView('stock/print', 'body');
        $this->template->display();
    }

    public function index() {


        $this->template->assign('result', $this->stock->all());
        $this->template->addView('stock/all', 'body');
        $this->template->display();
    }

    public function total() {


        $this->template->assign('result', $this->stock->total());
        $this->template->addView('stock/total', 'body');
        $this->template->display();
    }

    public function add() {

$this->template->assign('result', $this->products->all());
      

        // Create stock
        if ($this->stock->create()) {
            redirect('admin/stock');
        }


        $this->template->addView('stock/create', 'body');
        $this->template->display();
    }

    public function edit($id) {



        // Update Data
        if ($this->stock->update($id)) {
            redirect('admin/stock');
        }

        // Get stock data
        $result = $this->stock->find($id);
        if (empty($result)) {
            show_404();
        }
        $this->template->assign('result', $result[0]);
        $this->template->addView('stock/edit', 'body');
        $this->template->display();
    }

    public function search() {

        $this->template->addView('stock/search', 'body');
        $this->template->display();
    }

    public function delete($id) {


        $this->stock->delete($id);
        redirect('admin/stock');
    }

    public function report() {
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        $this->template->assign('from', $from);
        $this->template->assign('to', $to);

        $data = $this->stock->getReport();
        if (empty($data)) {
            redirect('admin/stock/search');
        }
        $this->template->assign('result', $data);
        $this->template->addView("stock/report", 'body');
        $this->template->display();
    }
    
    public function lowstock() {
        
        $this->template->addView("stock/lowstock", 'body');
        $this->template->display();
    }

    public function add_stock() {
        $data = $this->input->post("data_table");
        $shop = $this->input->post("shop");
        $cus_new = $this->input->post("shop_new");
        $cus_new_mobile = $this->input->post("mobile");
        $date = $this->input->post("date");
        $status = $this->stock->add_stock($data, $shop, $cus_new, $cus_new_mobile, $date);

        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "status" => $status));
    }

    public function oldBalance() {
        $postData = $this->input->post("id", TRUE);
        $data = array(
            'old' => AdminLTE::debit_vendor($postData)
        );

        echo json_encode($data);
    }

}
