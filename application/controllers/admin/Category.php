<?php

class Category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */
        
        $this->load->model('category_model', 'category');
        // Set Template parts
        $this->template->baseView('master/app');
       
        $this->template->assign('heading', 'Category');
    }

  
    public function index()
    {
        
     
        $this->template->assign('result', $this->category->all());
        $this->template->addView('category/all', 'body');
        $this->template->display();
    }
    
   
    public function add()
    {
        
         
        
        // Create category
        if($this->category->create()){
            redirect('admin/category');
        }
        
        $this->index();
    }
    
       
    
    public function edit($id)
    {
        
       
        
        // Update Data
        if($this->category->update($id)){
            redirect('admin/category');
        }
       
        // Get category data
        $result = $this->category->find($id);
        if(empty($result)){
            show_404();
        }
        $this->template->assign('result',$result[0]);
        $this->template->addView('category/edit', 'body');
        $this->template->display();
    }
    
    public function delete($id)
    {
        
        
        $this->category->delete($id);
        redirect('admin/category');
    }
    
 
}
