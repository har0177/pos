<?php

class Vendor extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */
        
        $this->load->model('vendor_model', 'vendor');
        // Set Template parts
        $this->template->baseView('master/app');
     
        $this->template->assign('heading', 'Vendors');
    }

    
  
    
     public function invoices()
    {
        
     $id = $this->input->post("id", TRUE);
         $result = $this->vendor->find($id);
      
          $data1 = array(
            'data' => $result[0],
            'result' => $this->vendor->all_invoice($id),
            
              'book' => $this->vendor->book_details($id)
        );

         $data = $this->load->view('vendor/all_invoices', $data1, TRUE);
    
        echo json_encode($data);
    }
    
    
  
    public function index()
    {
        
        $status = $this->vendor->status();
       $this->template->assign('status',$status);
        $this->template->assign('result', $this->vendor->all());
        $this->template->addView('vendor/all', 'body');
        $this->template->display();
    }
    
   
    public function add()
    {
      
        
        // Create vendor
        if($this->vendor->create()){
            redirect('admin/vendor');
        }
       $this->index();
    }
    
    public function edit($id)
    {
      
        
        // Update Data
        if($this->vendor->update($id)){
            redirect('admin/vendor');
        }
         $status = $this->vendor->status();
       $this->template->assign('status',$status);
        // Get vendor data
        $result = $this->vendor->find($id);
        if(empty($result)){
            show_404();
        }
        $this->template->assign('result',$result[0]);
        $this->template->addView('vendor/edit', 'body');
        $this->template->display();
    }
    
    public function delete($id)
    {
        
        
        $this->vendor->delete($id);
        redirect('admin/vendor');
    }
    
    public function activate($id)
    {
        $this->vendor->activate($id);
        redirect('admin/vendor');
    }
    
    public function deactivate($id)
    {
        $this->vendor->deactivate($id);
        redirect('admin/vendor');
    }
}
