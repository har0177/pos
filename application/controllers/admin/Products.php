<?php

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();
        auth_user('admin/user/login');
        /* if(!auth_admin()){
          show_404();
          } */

        $this->load->model('products_model', 'products');
        // Set Template parts
        $this->template->baseView('master/app');

        $this->template->assign('heading', 'Products');
    }

    
    public function p_name() {
        $postData = $this->input->post("id", TRUE);
        $data = array(
            'pprice' => AdminLTE::pprice($postData),
            'sprice' => AdminLTE::sprice($postData),
			'hprice' => AdminLTE::hprice($postData),
            'count' => AdminLTE::stock_count($postData)
        );

        echo json_encode($data);
    }

    public function index() {

        $this->template->assign('result', $this->products->all());
        $this->template->addView('products/all', 'body');
        $this->template->display();
    }
     function fetch_data(){
          $this->load->model('products_model', 'products');
        $fetch_data = $this->products->make_datatables();
        $data = array();
        $i = 1;
        foreach ($fetch_data as
                $row) {
            $url = site_url('admin/products/edit/' . $row->id);
            $sub_array = array();
            $sub_array[] = $i;
            $sub_array[] = AdminLTE::cat_name($row->cat_id) . " - ". $row->name;
            $sub_array[] = $row->pprice;
            $sub_array[] = $row->sprice;
            $sub_array[] = $row->hprice;
            $sub_array[]  = ' <a class="green" href="'.$url.'"> <i class="ace-icon fa fa-pencil bigger-130"></i></a>';
            $data[] = $sub_array;
            $i++;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->products->get_all_data(),
            "recordsFiltered" => $this->products->get_filtered_data(),
            "data" => $data
            
        );
        
        echo json_encode($output);
    }

    
     function p_data(){
          $this->load->model('products_model', 'products');
        $p_data = $this->products->make_datatables();
        $data = array();
        $i = 1;
        foreach ($p_data as
                $row) {
            $sub_array = array();
            $sub_array[] = $i;
             $sub_array[] = $row->id;
            $sub_array[] = AdminLTE::cat_name($row->cat_id) . " - ". $row->name;
            $sub_array[] = '<input style="width: 80px" class="input price'.$row->id.'" type="number" value="'.$row->sprice.'" min="1" disabled=""/>';
            $sub_array[] = AdminLTE::stock_count($row->id);
            $sub_array[] = '<input class="form-control colorful stock'.$row->id.'" type="number" value="1" min="1" />';
            $sub_array[]  = ' <button class="btn btn-success addtocart" data-id="'.$row->id.'" data-name="'.AdminLTE::cat_name($row->cat_id) . " - ". $row->name.'" data-stock="'.AdminLTE::stock_count($r->id).'" type="submit">Add</button>';
            $data[] = $sub_array;
            $i++;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->products->get_all_data(),
            "recordsFiltered" => $this->products->get_filtered_data(),
            "data" => $data
            
        );
        
        echo json_encode($output);
    }

    

    public function add() {


        // Create products
        if ($this->products->create()) {
            redirect('admin/products');
        }
        $this->index();
    }

    public function edit($id) {



        // Update Data
        if ($this->products->update($id)) {
            redirect('admin/products');
        }

        // Get products data
        $result = $this->products->find($id);
        if (empty($result)) {
            show_404();
        }
        $this->template->assign('result', $result[0]);
        $this->template->addView('products/edit', 'body');
        $this->template->display();
    }

    public function delete($id) {


        $this->products->delete($id);
        redirect('admin/products');
    }

}
