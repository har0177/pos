<div class="page-header hidden-print">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
        <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->

<script>
   function save(x) {
      
    
        $.ajax({
            url: "<?php echo site_url('admin/customer/invoices'); ?>",
            type: 'POST',
            data: {id: x},
            dataType: 'json',
            success: function (data) {
                $('.invoice').html(data);
            }
        });

    };
    
   
</script>


<div class="row">
    <div class="col-xs-12 ">
       
        <fieldset class="hidden-print">
                <legend> Add Customer</legend>
                <?php echo form_open('admin/customer/add', ['class' => 'form-horizontal']); ?>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Full Name:</label>
                    <div class="col-xs-12 col-sm-3">
                        <div class="clearfix">
                            <input type="text" id="name" required="" name="fullname" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
               
            
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="phone">Email Address:</label>

                    <div class="col-xs-12 col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="ace-icon fa fa-envelope"></i>
                            </span>

                            <input type="email" id="phone" name="email" class="col-xs-12 col-sm-12"/>
                        </div>
                    </div>
                </div>



                <div class="space-2"></div>



                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="phone">Phone Number:</label>

                    <div class="col-xs-12 col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="ace-icon fa fa-phone"></i>
                            </span>

                            <input type="tel" id="phone" required="" name="phone" class="col-xs-12 col-sm-12"/>
                        </div>
                    </div>
               
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="phone">Address:</label>

                    <div class="col-xs-12 col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="ace-icon fa fa-globe"></i>
                            </span>

                            <input type="tel" id="phone" required="" name="address" class="col-xs-12 col-sm-12"/>
                        </div>
                    </div>
                </div>



                <div class="space-2"></div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>

                    <div class="col-xs-12 col-sm-3">
                        <?php
                        $data = array(
                            'data-placeholder' => "Select User Status",
                            'class' => "chosen-select form-control",
                            'id' => 'form-field-select-4',
                            'tabindex' => '-1',
                            'required' => ''
                        );

                        //$options = $tmp;
                        echo form_dropdown('status', $status, set_value('status', 1), $data);
                        ?>
                    </div>
                
                    <div class="col-xs-12 col-sm-3 col-sm-offset-3">
                        <label>
                            <input type="submit" name="submit" value="Add Customer" class="btn btn-sm btn-success">
                        </label>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </fieldset>
        <br>
     
        <div class="col-xs-12 col-md-6 col-sm-12 col-lg-6 hidden-print">

            <div class="table-header">
                Manage <?php echo $heading; ?> 
            </div>
            <!-- div.table-responsive -->
            <!-- div.dataTables_borderWrap -->
            <div>
                <div class="clearfix">
                    <div class="pull-right tableTools-container"></div>
                </div>
                <table id="dyntable" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Full Name</th>

                            <th>Phone</th>
                            <th>Debit</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $i = 1;
                        $total = 0;
                        foreach ($result as
                                $r) {
                            $total += AdminLTE::debit($r->id);
                            ?>
                            <tr>
                                <td>
    <?php echo $i ?>
                                </td>
                                <td>
                                    <a  href="#" onclick="save(<?php echo $r->id; ?>)">
    <?php echo $r->fullname ?>
                                    </a>

                                </td>
                                <td><?php echo $r->phone ?></td>
                                <td><?php echo AdminLTE::debit($r->id); ?></td>

                                <td><?php
                                    if ($r->status == '1') {
                                        echo "<span class='label label-large label-success'>Active</span>";
                                    }
                                    else {
                                        echo "<span class='label label-large label-inverse'>Deactive</span>";
                                    }
                                    ?>	</td>
                                <td>
                                    <div class="hidden-sm action-buttons">
                                        <a class="green" href="<?php echo site_url('admin/customer/edit/' . $r->id) ?>">
                                            <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>

                                                                                                                                        <!--<a class="red" href="<?php echo site_url('admin/user/delete/' . $r->id) ?>">
                                                                                                                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                                                                        </a> -->
                                    </div>

                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                            <tr>
                            <th>Total</th>
                        <th></th>
                      
                        <th></th>
                           <th><?php echo $total ?></th>
                        <th></th>
                        <th></th>
                        </tr>
                    </tbody>
                </table>
            </div>


        </div>
           <div class="col-xs-12 col-md-6 col-sm-12 col-lg-6">
<p style="text-align: center" class="hidden-print">
               Click on a Customer Name to Show Details Here
           </p>
        <div class="invoice"></div>
           </div>
    </div>
</div>