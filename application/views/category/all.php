<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
            <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
           <div class="col-xs-12 col-md-5 col-sm-12 col-lg-5">
            
               <fieldset>
                   <legend>Add Category</legend>
                            <?php echo form_open('admin/category/add', ['class' => 'form-horizontal']); ?>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Category Name:</label>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="name" required="" name="name" class="col-xs-12 col-sm-12" />
                                    </div>
                                </div>
                            </div>

                           
                            <div class="hr hr-dotted"></div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                    <label>
                                        <input type="submit" name="submit" value="Add Category" class="btn btn-lg btn-success">
                                    </label>
                                </div>
                            </div>
<?php echo form_close(); ?>
                        </fieldset>
                  
        </div>
        <div class="col-xs-12 col-md-7 col-sm-12 col-lg-7">

        <div class="table-header">
            Manage <?php echo $heading; ?> 
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
           <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                          <th>Sr. No</th>
                        <th>Category Name</th>
                         
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as $r) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $i ?>
                            </td>
                            <td>
                                <?php echo $r->name ?>
                            </td>
                           
                            <td>
                                <div class="hidden-sm action-buttons">
                                    <a class="green" href="<?php echo site_url('admin/category/edit/' . $r->id) ?>">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                                                                                                                    <!--<a class="red" href="<?php echo site_url('admin/user/delete/' . $r->id) ?>">
                                                                                                                                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                                                                    </a> -->
                                </div>

                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div> </div>
</div>