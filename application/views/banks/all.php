<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
        <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->

<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 col-md-5 col-sm-12 col-lg-5">

            <fieldset>
                <legend>Deposit / Withdraw Payment</legend>
                <?php echo form_open('admin/banks/add_payment', ['class' => 'form-horizontal']); ?>
               
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="shop">Bank Name</label>

                    <div class="col-xs-12 col-sm-8">
                        <select required name="bank"  class="chosen-select form-control" id="form-field-select-4">

                            <option value="" >Please Select Bank </option>


                            <?php echo AdminLTE::banks(); ?>


                        </select>
                    </div>
                </div>
                <div class="space-2"></div>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4">Date:</label>

                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="text" id="name" required="" name="date" class="col-xs-12 col-sm-12 datepicker" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4" for="email">Deposit Amount:</label>
                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="number" name="deposit" required="" value="0" id="total" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                <div class="hr hr-dotted"></div>
                <div class="space-2"></div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="address">Withdraw Amount:</label>

                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="number" id="address" required="" name="withdraw" value="0" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                <div class="hr hr-dotted"></div>
                <div class="space-2"></div>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="address">Cheque / Receipt:</label>

                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="text" id="address" required="" name="cheque" placeholder="Cheque / Receipt No" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                <div class="hr hr-dotted"></div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                        <label>
                            <input type="submit" name="submit" value="Add Bank Payment" class="btn btn-lg btn-success">
                        </label>
                    </div>
                </div>
                </form>
            </fieldset>
        </div>
        <div class="col-xs-12 col-md-7 col-sm-12 col-lg-7">

            <div class="table-header">
                Manage Bank Statements 
            </div>
            <!-- div.table-responsive -->
            <!-- div.dataTables_borderWrap -->
            <div>
                <div class="clearfix">
                    <div class="pull-right tableTools-container"></div>
                </div>
                <table id="dyntable" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Bank Name</th>
                            <th>Deposit</th>
                            <th>Withdraw</th>
                            <th>Total</th>
                            <th>Cheque / Receipt</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $i = 1;
                        $deposit = 0;
                        $withdraw = 0;
            
                        foreach ($result as
                                $r) {
                            
                            $deposit += $r->deposit;
                             $withdraw += $r->withdraw;
                       
                            ?>
                            <tr>
                                <td>
    <?php echo $i ?>
                                </td>
                                <td>
    <?php echo AdminLTE::bank_name($r->bank_id) ?>
                                </td>

                                <td>
    <?php echo $r->deposit; ?>
                                </td>
                                <td>
    <?php echo $r->withdraw ?>
                                </td>
                                <td>
                                    <?php echo $deposit - $withdraw ?>
                                </td>
                                <td>
    <?php echo $r->cheque ?>
                                </td>
                                <td>
    <?php echo $r->date ?>
                                </td>
                                 <td>
                                <div class="hidden-sm action-buttons">
                                    <a class="green" title="Update Payment Details" href="<?php echo site_url('admin/banks/edit_stat/' . $r->id) ?>">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                            <!--                                    <a class="red" title="Delete Expense Name" href="<?php echo site_url('admin/school/delete_batch/' . $r->batch_id) ?>" onclick="return confirm('Are You Sure Want to Delete it?');">
                                                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                </a> -->
                                </div>

                            </td>

                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                            <tr>
                            
                                <th>
                                    
                                </th>
                                    <th>Total</th>
                                <th>
                                    <?php echo $deposit ?>
                                </th>
                                <th>
                                    <?php echo $withdraw ?>
                                </th>
                                <th>      <?php echo $deposit - $withdraw ?> </th>
                                <th></th>
                                     <th></th>
                                     <th></th>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div></div>
</div>