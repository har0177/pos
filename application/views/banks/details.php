  

<div class="row">
    <div class="col-xs-12">
         <div class="widget-box hidden-print">
             <h2 class="text-center" style="color: black">
                Search Date Wise Statement of <?php echo AdminLTE::bank_name($bank) ?>
            </h2>
            <div class="widget-body">
                <div class="widget-main">

                    <div id="fuelux-wizard-container">

                        <div class="step-content pos-rel">

                            <?php echo form_open('', ['class' => 'form-horizontal']); ?>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="status">From Date: </label>

                                <div class="col-xs-12 col-sm-3">
                                    <input type="text" name="from" value="<?php echo date('Y-m-d'); ?>" class="form-control datepicker"/>
                                </div>
                                <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="status">To Date: </label>

                                <div class="col-xs-12 col-sm-3">
                                    <input type="text" name="to" value="<?php echo date('Y-m-d'); ?>" class="form-control datepicker"/>
                                </div>
                                <div class="col-xs-12 col-sm-2">
                                    <input type="submit" name="submit" value="Search" class="btn btn-sm btn-success">


                                </div>
                            </div>


                            <?php echo form_close(); ?>

                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>
            </div>
        </div>
         <?php
        if (isset($_POST["submit"])) {
            
            $from = $this->input->post('from', TRUE);

            $to = $this->input->post('to', TRUE);

            $q1 = $this->db->query('Select bank_id, date, SUM(deposit) as deposit, SUM(withdraw) as withdraw from bank_details where bank_id= "' . $bank . '" and DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '" group by date order by date ASC');
            
      $opeining = $this->db->query('Select SUM(deposit - withdraw) as open from bank_details where bank_id= "' . $bank . '" and DATE(date) < "' . $from . '"')->row();
                  
            if ($q1->num_rows() > 0) {
            
            
                ?>
        <div class="table-header">
            Statement of <?php echo AdminLTE::bank_name($bank) ?> From <?php echo dateformatesformysql_fata($from). " to " . dateformatesformysql_fata($to) ?> 
        </div>
      
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
        
                        <table id="" class="table table-striped table-bordered table-hover display">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>

                                    <th>Deposit</th>

                                    <th>Withdraw</th>
                                    <th>Remaining</th>

                                    <th>Date</th>


                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td colspan="3">
                                        Opening Balance
                                    </td>
                                   
                                    <td colspan="2">
                                        <?php if(empty($opeining->open)){
                                            echo 0;
                                        }else{
                                            echo $opeining->open;
                                        } ?>
                                    </td>
                                   
                                </tr>
    <?php
    $i = 1;
 $total = $opeining->open;
$dp = 0;
$wd = 0;
   
    foreach ($q1->result() as
            $r) {

        $total += ($r->deposit - $r->withdraw);
        $dp += $r->deposit;
        $wd += $r->withdraw;
        ?>
                                    <tr>
                                        <td>
                                    <?php echo $i ?>
                                        </td>

                                        <td title=" <?php echo $r->deposit
                            ?>">
        <?php echo $r->deposit;
        ?>

                                        </td>

                                        <td title=" <?php echo $r->withdraw
        ?>">
        <?php echo $r->withdraw; ?>
                                        </td>
                                        <td title=" <?php echo $total
        ?>">
        <?php echo $total;
        ?>

                                        </td>


                                        <td>
        <?php echo dateformatesformysql_fata($r->date); ?>
                                        </td>

                                    </tr>
        <?php
        $i++;
    }
    ?>
                                  <tr>
                                    <td colspan="3">
                                        Closing Balance
                                    </td>
                                   
                                    <td colspan="2">
                                        <?php echo $total ?>
                                    </td>
                                   
                                </tr>

                            </tbody>
                        </table>

        </div>
        
        <?php 
            }else{
    set_flash_alert("No Record Found!", "danger");
            }
        }
        ?>
    </div>
</div>