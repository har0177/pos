<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
        <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->

<div class="row">
    <div class="col-xs-12">
        
            <fieldset>
                <legend>Update Payment</legend>
                <?php echo form_open('', ['class' => 'form-horizontal']); ?>
               
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="shop">Bank Name</label>

                    <div class="col-xs-12 col-sm-8">
                        <select required name="bank"  class="chosen-select form-control" id="form-field-select-4">

                            <option value="" >Please Select Bank </option>


                            <?php echo AdminLTE::banks($r->bank_id); ?>


                        </select>
                    </div>
                </div>
                <div class="space-2"></div>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4">Date:</label>

                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="text" id="name" required="" name="date" class="col-xs-12 col-sm-12 datepicker" value="<?php echo $r->date; ?>" />
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4" for="email">Deposit Amount:</label>
                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="number" name="deposit" required="" value="<?php echo $r->deposit; ?>" id="total" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                <div class="hr hr-dotted"></div>
                <div class="space-2"></div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="address">Withdraw Amount:</label>

                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="number" id="address" required="" name="withdraw" value="<?php echo $r->withdraw; ?>" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                <div class="hr hr-dotted"></div>
                <div class="space-2"></div>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="address">Cheque / Receipt:</label>

                    <div class="col-xs-12 col-sm-8">
                        <div class="clearfix">
                            <input type="text" id="address" value="<?php echo $r->cheque; ?>" required="" name="cheque" placeholder="Cheque / Receipt No" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                <div class="hr hr-dotted"></div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                        <label>
                            <input type="submit" name="submit" value="Update Bank Payment" class="btn btn-lg btn-success">
                        </label>
                    </div>
                </div>
                </form>
            </fieldset>
        </div>
        
</div>
</div>