<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Report of Profit from Sale

        <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->

<script>

    $(document).ready(function () {

        $('.table').DataTable({
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

               
                
                 // Total over all pages
                totalb = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotalb = api
                        .column(4, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(4).footer()).html(
                        'Rs.' + pageTotalb + ' (Total Rs. ' + totalb + ')'
                        );
						
						  // Total over all pages
                totalb = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotalb = api
                        .column(5, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(5).footer()).html(
                        'Rs.' + pageTotalb + ' (Total Rs. ' + totalb + ')'
                        );
            }
        });
          $('.table tbody').on('click', 'tr', function () {

            $(this).toggleClass('selected');
            var totalSUM = 0;
            $("tbody tr.selected").each(function () {
                var getValue = $(this).find("td:eq(5)").html().replace("$", "");
                var filteresValue = getValue.replace(/\,/g, '');
                totalSUM += Number(filteresValue)
                console.log(filteresValue);
            });

            console.log($('tfoot tr > th').eq(1)
                    .html('Rs.' + totalSUM));


            //alert( 'Column sum is: '+ table.column( 5 ).data().sum() );
        });
      
    });
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Report of Profit 
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
          
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Invoice No</th>
                        <th>Customer Name</th>
                        <th> Sale</th>
						<th> Discount</th>
                        <th> Profit</th>




                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as
                            $r) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $i ?>

                            </td>
                            <td>
                                <?php echo $r->inv_id ?>

                            </td>
                            <td>
                                <?php echo AdminLTE::customers(AdminLTE::customers_name($r->inv_id)) ?>

                            </td>
                            <?php AdminLTE::sale_count_all($r->inv_id, "sale"); ?>



                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th> SUM Selected</th>
                        <th> Rs.??? </th>
                        <th colspan="2" style="text-align:right">Total:</th>
                    <th></th>
					<th></th>
                        
                     
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>
