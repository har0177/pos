<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage Total Customers <?php echo $heading; ?> 
        <a href="<?php echo site_url('admin/sale/add'); ?>" class="btn btn-sm btn-success pull-right">  
            <i class="ace-icon fa fa-plus-square"></i> Add New</a>
                 <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage Total Customers <?php echo $heading; ?> 
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
           <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                 <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Customer Name</th>
                        <th>Total Sale</th>
                      
                      
                      
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as $r) {
                        ?>
                        <tr>
                            <td>
    <?php echo $i ?>
                            </td>
                            <td>
                                <?php  echo AdminLTE::customers(AdminLTE::customers_name($r->inv_id));
                                ?>
                               
                            </td>
                            <td>
                                <?php AdminLTE::sale_count_shop($r->inv_id, "sale", $from, $to); ?>
                            </td>
                     
                           
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>