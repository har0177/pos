<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Report of <?php echo $heading; ?> 

    </h1>
</div><!-- /.page-header -->
<script>
    $(document).ready(function () {
        var myTable = $('.table').DataTable({
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

				// Total over all pages
                total = api
                        .column(1)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotal = api
                        .column(1, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(1).footer()).html(
                        'Rs.' + pageTotal + ' (Total Rs. ' + total + ')'
                        );
						
                // Total over all pages
                total = api
                        .column(2)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotal = api
                        .column(2, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(2).footer()).html(
                        'Rs.' + pageTotal + ' (Total Rs. ' + total + ')'
                        );

                // Total over all pages
                totalb = api
                        .column(3)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotalb = api
                        .column(3, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(3).footer()).html(
                        'Rs.' + pageTotalb + ' (Total Rs. ' + totalb + ')'
                        );
            }
        });
        


        $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

        new $.fn.dataTable.Buttons(myTable, {
            buttons: [
                {
                    "extend": "csv",
                    "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                    "className": "btn btn-white btn-primary btn-bold",
                    footer: true
                },
                {
                    "extend": "pdf",
                    "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                    "className": "btn btn-white btn-primary btn-bold",
                    footer: true,
                    message: "Monthly Report of Sale <?php echo dateformatesformysql_fata($from) . " - " . dateformatesformysql_fata($to) ?>"
                },
                {
                    "extend": "print",
                    "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                    "className": "btn btn-white btn-primary btn-bold",
                    autoPrint: true,
                    footer: true,
                    message: "<h1 class='center'>Monthly Report of Sale <?php echo dateformatesformysql_fata($from) . " - " . dateformatesformysql_fata($to) ?></h1>",
                }

            ]
        });
        myTable.buttons().container().appendTo($('.tableTools-container'));





    });



</script>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Report of  <?php echo $heading; ?> 
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>From Date</th>
                        <th></th>
                        <th></th>
                        <th>To Date</th>


                    </tr>
                    <tr>
                        <th>
                            <?php echo $from; ?>
                        </th>
                        <th></th>
                        <th></th>
                        <th>
                            <?php echo $to; ?>
                        </th>
                    </tr>


                    <tr>
                        <th>Product Name</th>
                        <th>Total Sale</th>
                        <th>Profit</th>
                        <th>Total Profit</th>



                    </tr>

                </thead>
                <tbody>



                    <?php
                    foreach ($result as
                            $r) {
                        ?>
                        <tr>
                            <td>
                                <?php echo AdminLTE::product_name($r->product_id);
                                ?>

                            </td>
                            <?php AdminLTE::sale_count($r->product_id, "sale", $from, $to); ?>






                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th> </th>
                        <th></th>
                        <th></th>
                        <th></th>


                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>