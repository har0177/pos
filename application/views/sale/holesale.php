<div class="page-header hidden-print">
    <h1>
        <i class="ace-icon fa fa-user"></i>
        Add <?php echo $heading; ?> (WholeSaler)
        <a href="<?php echo base_url() . "admin/sale/add_holesale" ?>" class="btn btn-sm btn-success pull-right hidden-print">
            <i class="ace-icon fa fa-arrow-circle-o-down"></i> Add New Sale</a>
        <a href="<?php echo site_url('admin/sale'); ?>" class="btn btn-sm btn-primary pull-right hidden-print">
            <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<style>

    .panel-heading.collapsed .fa-chevron-down,
    .panel-heading .fa-chevron-right {
        display: none;

    }

    .panel-info > .panel-heading {
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }


    .panel-heading.collapsed .fa-chevron-right,
    .panel-heading .fa-chevron-down {
        display: inline-block;

    }

    i.fa {
        cursor: pointer;
        margin-right: 5px;
    }

    .collapsed ~ .panel-body {
        padding: 0;
    }

</style>
<script type="text/javascript">
     var number = 1;
    $(document).ready(function () {

        $('#customer').change(function () {
            var id = $(this).val();
            $.ajax({
                url: "<?php echo site_url('admin/sale/oldBalance'); ?>",
                method: "POST",
                data: {id: id},
                async: true,
                dataType: 'json',
                success: function (data) {
                    document.getElementById('old').innerHTML = parseFloat(data.old);
                    payamount();
                }
            });
            return false;
        });
        $('#save').click(function () {

            if ($('#customer option[value]:selected').text() == '') {
                if ($("#cus_new").val() == "") {
                    $('#cus_new').focus();
                    swal("Please Select Customer", "", "warning");
                } else if ($("#billno").val() == 0) {
                    $('#billno').focus();
                    swal("Please Enter Bill No", "", "warning");
                } else {
                    add_data();
                }
            } else if ($("#billno").val() == 0) {
                $('#billno').focus();
                swal("Please Enter Bill No", "", "warning");
            } else {
                add_data();
            }

        });
        function add_data() {

            var table_data = [];
            $('#table tr').each(function (row, tr) {

                if ($(tr).find('td:eq(0)').text() == "") {

                } else {


                    var sub = {
                        'id': $(tr).find('td:eq(0)').text(),
                        'stock': $(tr).find('td:eq(2)').text(),
                        'sprice': $(tr).find('td:eq(3)').text(),
                        'date': $('#date').val(),
                        'billno': $('#billno').val()

                    };
                    table_data.push(sub);
                }
            }
            );
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Save Invoice!'
            },
            function () {

                var discount_last = 0;
                var dis_per = "";

                var discount = $("#disc").val();
                var dis = $('#discount').val();

                var bilty_discount = $('#bilty_discount').val();
                var price = $('#price').text();
                if (discount === "per") {
                    var amount_disc = (parseFloat(price) * parseFloat(dis)) / 100;
                    discount_last = amount_disc;
                    dis_per = dis;
                } else {

                    discount_last = $('#discount').val();

                }

                var data = {
                    "data_table": table_data,
                    'shop_new': $("#cus_new").val(),
                    'shop': $("#customer").val(),
                    'mobile': $("#mobile").val(),
                    'date': $("#date").val(),
                    'discount': discount_last,
                    'bilty_discount': bilty_discount,
                    'billno': $('#billno').val(),
                    'total': $("#price").text(),
                    'old': $("#old").text(),
                    'paid': $("#paid").val(),
                    'banking': $("#cash").val(),
                    'bank': $("#bank").val(),
                    'comments': $("#comments").val(),
                    'dis_per': dis_per,
                };
                $.ajax({
                    data: data,
                    url: "<?php echo site_url('admin/sale/add_sale'); ?>",
                    crossOrigin: false,
                    type: "POST",
                    dataType: 'json',
                    success: function (result) {
                        if (result.status == "failed") {
                            swal("Error Saving", "", "warning");
                        } else {
                            swal({
                                title: 'Successfully Saved',
                                text: " ",
                                type: 'success'
                            },
                            function () {
                                $.ajax({
                                    url: "<?php echo site_url('admin/sale/invoice_show'); ?>",
                                    type: 'POST',
                                    data: {id: result.status},
                                    dataType: 'json',
                                    success: function (data) {
                                        $('.step-content').hide();
                                        $('.invoice').html(data);
                                    }
                                });
                            });
                        }
                    }
                });
            }
            );
        }


       
        $('.addtocart').on('click', function () {
            var id = $(this).data('id');
            var product = $(this).data('name');
            var sprice = $('.price' + id).text();
            var count = $('.stockin' + id).text();
            var stock = $('.stock' + id).val();
            if (parseFloat(count) <= 0){
            $('.stock' + id).val(count);
                    $('.stock' + id).focus();
                    bootoast({
                    message: 'Product ' + product + ' is out of stocked',
                            type: 'danger'
                    });
            } else if (parseFloat(stock) > parseFloat(count)) {

            $('.stock' + id).val(count);
                    $('.stock' + id).focus();
                    bootoast({
                    message: 'Stock Entered is Greater Than Available Stock',
                            type: 'danger'
                    });
            } else {

            let table = $('#table');
                    let tr = table.find('tr.p' + id);
                    if (tr.length > 0) {

            var vall = tr.find('td:eq(2)').text();
                    var total = parseFloat(vall) + parseFloat(stock);
                    if (parseFloat(total) > parseFloat(count)) {
            $('.stock' + id).val(1);
                    $('.stock' + id).focus();
                    bootoast({
                    message: 'Stock Entered is Greater Than Available Stock',
                            type: 'danger'
                    });
                    tr.find('td:eq(2)').text(parseFloat(vall));
            } else {
            $('.stock' + id).val(1);
                    tr.find('td:eq(2)').text(parseFloat(total));
            }

            totalamount();
            } else {
            let row = $('<tr>');
                    row.attr('class', 'p' + id);
            row.append('<td>' + id + '</td>');
            row.append('<td>' + product + '</td>');
            row.append('<td>' + stock + '</td>');
            row.append('<td>' + sprice + '</td>');
            row.append("<td><a class='red' href='#' onclick='deleteRow(this);'><i class='ace-icon fa fa-trash bigger-130'></i></a></td>");
            row.append('<td>' + number + '</td>');
            row.appendTo(table);
            $('.stock' + id).val(1);
            totalamount();
            number++;
        }

        }
    });
    });</script>

<script>

            function add() {

                $('#exp_add_field').css("display", "block");
                $('#exp_min_btn').css("display", "block");
                $('#exp_add_btn').css("display", "none");
            }

    function subtract() {

        $('#exp_add_field').css("display", "none");
        $('#exp_add_btn').css("display", "block");
        $('#exp_min_btn').css("display", "none");
    }



    function totalamount() {

        var table = document.getElementsByTagName("table")[1];
        var sumVal = 0;
        for (var z = 1; z < table.rows.length; z++) {
            var t = parseFloat(table.rows[z].cells[2].innerHTML) * parseFloat(table.rows[z].cells[3].innerHTML);
            sumVal = sumVal + parseFloat(t);
        }
        document.getElementById('price').innerHTML = parseFloat(sumVal);
        payamount();
    }

    cus_new = function () {
        document.getElementById('old').innerHTML = 0;
    }



    function val() {

        $('#paid').css("display", "block");
        $('#bank').css("display", "block");
    }

    payamount = function () {
        var discount = $("#disc").val();
        var dis = $('#discount').val();
        var price = $('#price').text();
        var old = $('#old').text();
        var paid = $('#paid').val();

        var bilty_discount = $('#bilty_discount').val();
        if (paid == "") {
            paid = 0;
        }
        if (discount === "per") {
            var amount_disc = (parseFloat(price) * parseFloat(dis)) / 100;

            var total = (parseFloat(price) - parseFloat(amount_disc)) + parseFloat(old) + parseFloat(bilty_discount);
            document.getElementById('show').innerHTML = parseFloat(total) - parseFloat(paid);
            document.getElementById('this_amount').innerHTML = parseFloat(price) - parseFloat(amount_disc) + parseFloat(bilty_discount);
        } else {
            var total = parseFloat(price) - parseFloat(dis) + parseFloat(old) + parseFloat(bilty_discount);
            document.getElementById('show').innerHTML = parseFloat(total) - parseFloat(paid);
            document.getElementById('this_amount').innerHTML = parseFloat(price) - parseFloat(dis) + parseFloat(bilty_discount);
        }

    }

    function deleteRow(btn) {


        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        },
        function () {
            $(btn).closest("tr").remove();
            totalamount();
            number--;
        });
    }

</script>
<div class="row">
    <div class="col-xs-12">

        <div class="widget-body">
            <div class="widget-main">
                <div id="fuelux-wizard-container">
                    <div class="step-content pos-rel form-horizontal hidden-print">

                        <div class="form-group" id="exp_add_btn">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="product">Customer
                                Name</label>

                            <div class="col-xs-12 col-sm-3">
                                <select name="customer" class="chosen-select form-control" id="customer">

                                    <option>Please Select Customer</option>


                                    <?php echo AdminLTE::Shopkeepers(); ?>


                                </select>
                            </div>

                            <div class="col-xs-12 col-sm-1">
                                <a href="#" onclick="add()" class="btn btn-sm btn-success">
                                    <i class="fa fa-plus-circle"></i></a>

                            </div>


                        </div>
                        <div class="form-group" id="exp_add_field" style="display: none">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">Customer Name:</label>

                            <div class="col-xs-12 col-sm-3">
                                <div class="clearfix">
                                    <input type="text" name="cus_new" onblur="cus_new()" id="cus_new"
                                           placeholder="Please Enter Customer Name" class="col-xs-12 col-sm-12"/>
                                </div>
                            </div>

                            <label class="control-label col-xs-12 col-sm-2 no-padding-right">Mobile No:</label>

                            <div class="col-xs-12 col-sm-3">
                                <div class="clearfix">
                                    <input type="number" name="cus_new_mobile" id="mobile"
                                           placeholder="Please Enter Mobile No" class="col-xs-12 col-sm-12"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-1">

                                <a href="#" onclick="subtract()" id="exp_min_btn" class="btn btn-sm btn-danger">
                                    <i class="fa fa-minus-circle"></i></a>
                            </div>
                        </div>
                        <div class="form-group">

                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">Bill No:</label>
                            <div class="col-xs-12 col-sm-3">
                                <input required="" type="text" name="billno" value="1" id="billno" placeholder="Bill Number"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">Date:</label>

                            <div class="col-xs-12 col-sm-3">
                                <div class="clearfix">
                                    <input type="text" id="date" required="" name="date"
                                           class="col-xs-12 col-sm-12 datepicker"
                                           value="<?php echo date('Y-m-d'); ?>"/>
                                </div>
                            </div>


                        </div>
                        <div class="hr hr-dotted"></div>

                        <div class="col-sm-7">
                            <div class="table-header">
                                Products
                            </div>
                            <table id="dyntable" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr. No</th>
                                        <th>ID</th>
                                        <th>Name</th>


                                        <th> W Price</th>
                                        <th> In Stock </th>


                                        <th>Add</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($result as
                                            $r) {
                                        ?>
                                        <tr> <td>
                                                <?php echo $i ?>
                                            </td>
                                            <td>
                                                <?php echo $r->id ?>
                                            </td>
                                            <td>
                                                <?php echo AdminLTE::cat_name($r->cat_id) . " - " . $r->name ?>
                                            </td>



                                            <td contenteditable="true" class="price<?php echo $r->id ?>">
                                                <?php echo $r->hprice; ?>


                                            </td>

                                            <td contenteditable="true" class="stockin<?php echo $r->id ?>">
                                                <?php echo AdminLTE::stock_count($r->id); ?>
                                            </td>
                                            <td>

                                                <input class="form-control  colorful stock<?php echo $r->id ?>" type="number" value="1" min="1" />

                                            </td>
                                            <td>

                                                <button class="btn btn-success addtocart" data-id="<?php echo $r->id ?>" data-name="<?php echo AdminLTE::cat_name($r->cat_id) . " - " . $r->name ?>"  type="submit">Add</button>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <?php echo form_open('', [ 'class' => 'form-horizontal']); ?>
                            <div class="table-header">
                                Manage <?php echo $heading; ?>
                            </div>
                            <!-- div.table-responsive -->
                            <!-- div.dataTables_borderWrap -->
                            <div>
                                <div class="clearfix">
                                    <div class="pull-right tableTools-container"></div>
                                </div>

                                <table id="table" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th> ID</th>
                                            <th> Name</th>
                                            <th>Stock</th>
                                            <th> Price</th>
                                            <th>Action</th>
                                            <th>Sr No</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">

                                    </tbody>
                                </table>
                                <table id="table1" class="table table-striped table-hover">

                                    <tr>
                                        <td colspan="2" rowspan="10">
                                        </td>
                                        <th> Total Price

                                        </th>
                                        <td><h6 style="text-align: center; font-family: 'Times'" id="price"></h6></td>
                                    </tr>


                                    <tr>

                                        <th>
                                            Old Balance
                                        </th>
                                        <td><h6 style="text-align: center; font-family: 'Times'" id="old"></h6></td>
                                    </tr>
                                    <tr>
                                        <th>
                                    <div class="col-xs-12 col-sm-7">
                                        <select onchange="val_dis()" id="disc" name="dis" class="chosen-select">
                                            <option value="rupee"> Discount by Rs.</option>
                                            <option value="per"> Discount by %</option>

                                        </select>
                                    </div>
                                    </th>
                                    <td><input id="discount" value="0" onblur="payamount()" type="text"
                                               name="discount" class="form-control">

                                    </td>

                                    </tr>
                                    <tr>
                                        <th>
                                            Bilty (Rs.)
                                        </th>
                                        <td><input id="bilty_discount" value="0" onblur="payamount()"
                                                   type="text" name="bilty_discount" class="form-control"></td>

                                    </tr>


                                    <tr>

                                        <th>
                                            Total Amount
                                        </th>
                                        <td><h6 style="text-align: center; font-family: 'Times'" id="this_amount"></h6>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>

                                            <select onchange="val()" id="cash" name="banking" class="chosen-select">
                                                <option value=" "> Choose Paid Option</option>

                                                <?php echo AdminLTE::banks(); ?>
                                            </select>
                                        </th>
                                        <td><input id="paid" value="" onblur="payamount()"
                                                   placeholder="Enter Paid Amount" type="text" name="paid"
                                                   class="form-control" style="display: none;"><input id="bank"
                                                   placeholder="Cheque No / Receipt NO" value="1"
                                                   type="text"
                                                   name="bank"
                                                   class="form-control"
                                                   style="display: none;">
                                        </td>

                                    </tr>
                                    <tr>

                                        <th>
                                            Net Amount
                                        </th>
                                        <td><h6 style="text-align: center; font-family: 'Times'" id="show"></td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Comments
                                        </th>
                                        <td><textarea rows='5' style="text-align: left" id="comments" name="comments"
                                                      placeholder="Comments" maxlength="612"
                                                      class='form-control'></textarea></td>

                                    </tr>

                                </table>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                    <label>
                                        <input type="button" id="save" class="btn btn-lg btn-success"
                                               value="Proceed to Sale">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>


                <div class="invoice"></div>
            </div><!-- /.widget-main -->

        </div>


    </div><!-- /.col -->

</div>