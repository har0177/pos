
<a href="javascript:window.history.go(-1)" class="btn btn-large btn-primary hidden-print pull-left"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>

<style>
    @media print{
        .table{
            font-size: 16px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        .table th{
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }
    }
</style>



<style>
    .panel-heading.collapsed .fa-chevron-down,
    .panel-heading .fa-chevron-right {
        display: none;

    }

    .panel-info>.panel-heading {
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }



    .panel-heading.collapsed .fa-chevron-right,
    .panel-heading .fa-chevron-down {
        display: inline-block;

    }

    i.fa {
        cursor: pointer;
        margin-right: 5px;
    }

    .collapsed ~ .panel-body {
        padding: 0;
    }

</style>
<script type="text/javascript">
    $(document).ready(function () {
        totalamount();

        $('.addtocart').on('click', function () {
            var id = $(this).data('id');
            var product = $(this).data('name');
            var sprice = $('.price' + id).text();
            var count = $('.stockin' + id).text();
            var stock = $('.stock' + id).val();
            if (parseFloat(count) <= 0){
            $('.stock' + id).val(count);
                    $('.stock' + id).focus();
                    bootoast({
                    message: 'Product ' + product + ' is out of stocked',
                            type: 'danger'
                    });
            } else if (parseFloat(stock) > parseFloat(count)) {

            $('.stock' + id).val(count);
                    $('.stock' + id).focus();
                    bootoast({
                    message: 'Stock Entered is Greater Than Available Stock',
                            type: 'danger'
                    });
            } else {

            let table = $('#table');
                    let tr = table.find('tr.p' + id);
                    if (tr.length > 0) {

            var vall = tr.find('td:eq(2)').find('input').val();
                    var total = parseFloat(vall) + parseFloat(stock);
                    if (parseFloat(total) > parseFloat(count)) {
            $('.stock' + id).val(1);
                    $('.stock' + id).focus();
                    bootoast({
                    message: 'Stock Entered is Greater Than Available Stock',
                            type: 'danger'
                    });
                    tr.find('td:eq(2)').find('input').val(parseFloat(vall));
            } else {
            $('.stock' + id).val(1);
                    tr.find('td:eq(2)').find('input').val(parseFloat(total));
            }

            totalamount();
            } else {
            let row = $('<tr>');
                    row.attr('class', 'p' + id);
            row.append('<td>' + id + '</td>');
            row.append('<td>' + product + '</td>');
            row.append('<td><input type="number" value="' + stock + '" onblur="totalamount()" class="form-control col-sm-3"></td>');
            row.append('<td>' + sprice + '</td>');
            row.append("<td><a class='red' href='#' onclick='deleteRow_new(this);'><i class='ace-icon fa fa-trash bigger-130'></i></a></td>");
            row.appendTo(table);
            $('.stock' + id).val(1);
            totalamount();
        }

        }
    });
    });
            function add_data() {

                var table_data = [];
                $('#table tr').each(function (row, tr) {

                    if ($(tr).find('td:eq(0)').text() == "") {

                    } else {


                        var sub = {
                            'pid': $(tr).find('td:eq(0)').text(),
                            'stock': $(tr).find('td:eq(2)').find('input').val(),
                            'sprice': $(tr).find('td:eq(3)').text(),
                            'inv': $('#inv').val()

                        };
                        table_data.push(sub);
                    }
                }
                );
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Save Invoice!'},
                function () {

                    var data = {
                        "data_table": table_data,
                        'total': $("#price").text(),
                        'old': $("#old").text(),
                        'paid': $("#paid").val(),
                        'discount': $("#discount").val(),
                        'bilty_discount': $("#bilty_discount").val(),
                    };
                    $.ajax({
                        data: data,
                        url: "<?php echo site_url('admin/sale/update_sale'); ?>",
                        crossOrigin: false,
                        type: "POST",
                        dataType: 'json',
                        success: function (result) {
                            if (result.status == "failed") {
                                swal("Error Saving", "", "warning");
                            } else {
                                swal({
                                    title: 'Successfully Saved',
                                    text: " ",
                                    type: 'success'},
                                function () {
                                    $.ajax({
                                        url: "<?php echo site_url('admin/sale/invoice_show'); ?>",
                                        type: 'POST',
                                        data: {id: result.status},
                                        dataType: 'json',
                                        success: function (data) {
                                            $('.step-content').hide();
                                            $('.invoice').html(data);
                                        }
                                    });
                                });
                            }
                        }
                    });
                }
                );
            }


    function totalamount() {

        var table = document.getElementsByTagName("table")[2];
        var sumVal = 0;
        for (var z = 1; z < table.rows.length; z++) {
            var t = parseFloat(table.rows[z].cells[2].children[0].value) * parseFloat(table.rows[z].cells[3].innerHTML);
            sumVal = sumVal + parseFloat(t);
        }
        document.getElementById('price').innerHTML = parseFloat(sumVal);
        payamount();
    }





    payamount = function () {

        var dis = $('#discount').val();
        var bilty_discount = $('#bilty_discount').val();
        var price = $('#price').text();
        var old = $('#old').text();
        var paid = $('#paid').val();
        var total = parseFloat(price) - parseFloat(dis) + parseFloat(old) + parseFloat(bilty_discount);
        document.getElementById('show').innerHTML = parseFloat(total) - parseFloat(paid);
        document.getElementById('this_amount').innerHTML = parseFloat(price) - parseFloat(dis) + parseFloat(bilty_discount);
    }

    function deleteRow(value, btn) {


        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'},
        function () {

            $.ajax({
                data: {id: value},
                url: "<?php echo site_url('admin/sale/delete'); ?>",
                crossOrigin: false,
                type: "POST",
                dataType: 'json',
                success: function (result) {
                    if (result.status == "failed") {
                        swal("Error Deleting", "", "warning");
                    } else {
                        swal({
                            title: 'Successfully Deleted!',
                            text: " ",
                            type: 'success'});
                        $(btn).closest("tr").remove();
                        totalamount();
                    }
                }
            });
            totalamount();
        });
    }

    function deleteRow_new(btn) {


        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'},
        function () {
            $(btn).closest("tr").remove();
            totalamount();
        });
    }

</script>
<div class="row">
    <div class="col-xs-12">

        <div class="widget-body">
            <div class="widget-main">
                <div id="fuelux-wizard-container">
                    <div class="step-content pos-rel form-horizontal hidden-print" >

                        <div class="row">


                            <h2 style="text-align: center">NOOR CORPORATION</h2>
                            <h3 style="text-align: center">Abasin Market No.2 Mingora Swat</h3>
                            <h4 style="text-align: center">Mobile No: 0346 - 9426741 </h4>



                            <div class="col-sm-4 col-xs-5" style="border: 1px solid black;">
                                <h3 style="border-bottom: 1px solid black ">Bill To</h3>  
                                <p>

                                    <?php echo AdminLTE::customers(AdminLTE::customers_name($invoice)); ?>

                                    <br>
                                    <br>

                                    <br>
                                    <br>
                                </p>    
                            </div>

                            <div class="col-sm-8 col-xs-7">
                                <h1 style="text-align: center;">Invoice</h1>  
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>Invoice ID</th>
                                        <th>
                                            Bilty No
                                        </th>
                                        <th>

                                            Date
                                        </th>
                                    </tr>
                                    <tr>
                                        <td><?php echo $invoice; ?><input type="hidden" id="inv" value="<?php echo $invoice ?>"></td>

                                        <td>
                                            <?php echo AdminLTE::billno_sale($invoice); ?></td>

                                        <td>
                                            <?php echo date('d/m/Y', strtotime($date->date)); ?> 
                                        </td>
                                    </tr>
                                </table>
                            </div>       
                        </div>
                        <br>

                        <div class="hr hr-dotted"></div>

                        <div class="col-sm-7">
                            <div class="table-header">
                                Products
                            </div>
                            <table id="dyntable" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr. No</th>
                                        <th>ID</th>
                                        <th>Name</th>


                                        <th> R Price</th>
                                        <th> In Stock </th>


                                        <th>Add</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($result as
                                            $rr) {
                                        ?>
                                        <tr> <td>
                                                <?php echo $i ?>
                                            </td>
                                            <td>
                                                <?php echo $rr->id ?>
                                            </td>
                                            <td>
                                                <?php echo AdminLTE::cat_name($rr->cat_id) . " - " . $rr->name ?>
                                            </td>



                                            <td contenteditable="true" class="price<?php echo $rr->id ?>">
                                                <?php echo $rr->sprice; ?>


                                            </td>

                                            <td contenteditable="true" class="stockin<?php echo $rr->id ?>">
                                                <?php echo AdminLTE::stock_count($rr->id); ?>
                                            </td>
                                            <td>

                                                <input class="form-control  colorful stock<?php echo $rr->id ?>" type="number" value="1" min="1" />

                                            </td>
                                            <td>

                                                <button class="btn btn-success addtocart" data-id="<?php echo $rr->id ?>" data-name="<?php echo AdminLTE::cat_name($rr->cat_id) . " - " . $rr->name ?>"  type="submit">Add</button>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <?php echo form_open('', ['class' => 'form-horizontal']); ?>
                            <div class="table-header">
                                Manage <?php echo $heading; ?> 
                            </div>
                            <!-- div.table-responsive -->
                            <!-- div.dataTables_borderWrap -->
                            <div>
                                <div class="clearfix">
                                    <div class="pull-right tableTools-container"></div>
                                </div>

                                <table id="table" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr><th>Product ID</th>
                                            <th>Product Name</th>
                                            <th>Stock</th>
                                            <th> Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                        <?php
                                        $i = 1;
                                        foreach ($re as
                                                $value) {
                                            ?>
                                            <tr class="p<?php echo $value->product_id; ?>">

                                                <td>
                                                    <?php echo $value->product_id ?>
                                                </td>
                                                <td>
                                                    <?php echo AdminLTE::cat_name(AdminLTE::category_name($value->product_id)) . " - " . AdminLTE::product_name($value->product_id); ?>
                                                </td>


                                                <td>

                                                    <input id="return" value="<?php echo $value->sale ?>"  type="number" name="return[<?php echo $value->product_id ?>]" onblur='totalamount()' class="form-control col-sm-3">

                                                </td>
                                                <td>
                                                    <?php echo $value->price ?>
                                                </td>
                                                <td><a class='red' href='#' onclick='deleteRow(<?php echo $value->id; ?>, this)'><i class='ace-icon fa fa-trash bigger-130'></i></a>
                                                </td>


                                            </tr>
                                            <?php
                                        }
                                        ?>


                                    </tbody>

                                </table>
                                <table id="table1" class="table table-striped table-hover">

                                    <tr>
                                        <td colspan="2" rowspan="10">
                                        </td>
                                        <th> Total Price

                                        </th>
                                        <td  ><h6 style="text-align: center; font-family: 'Times'" id="price"></h6></td>
                                    </tr>
                                    <tr>

                                        <th >
                                            Old Balance
                                        </th >
                                        <td>  <h6 style="text-align: center; font-family: 'Times'" id="old"><?php echo $old->old ?></h6></td>
                                    </tr>
                                    <tr>                        
                                        <th>

                                            Discount
                                        </th>
                                        <td  ><input id="discount" value="<?php echo $discount->discount; ?>" onblur="payamount()" type="number" name="discount" class="form-control">

                                        </td>

                                    </tr>
                                     <tr>                        
                                        <th>

                                            Bilty
                                        </th>
                                        <td  ><input id="bilty_discount" value="<?php echo $discount->bilty; ?>" onblur="payamount()" type="number" name="bilty_discount" class="form-control">

                                        </td>

                                    </tr>





                                    <tr>

                                        <th >
                                            Total Amount
                                        </th >
                                        <td>  <h6 style="text-align: center; font-family: 'Times'" id="this_amount"></h6></td>
                                    </tr>


                                    <tr>                        
                                        <th>

                                            Paid Amount
                                        </th>
                                        <td><input id="paid" value="<?php echo $paid->paid ?>" onblur="payamount()" placeholder="Enter Paid Amount"  type="number" name="paid" class="form-control"></td>
                                    </tr>
                                    <tr>

                                        <th >
                                            Net Amount
                                        </th >
                                        <td>  <h6 style="text-align: center; font-family: 'Times'" id="show"></td>
                                    </tr>


                                </table>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                    <label>
                                        <input type="button" id="save" onclick="add_data();" class="btn btn-lg btn-success" value="Update Invoice">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>


                <div class="invoice"></div>
            </div><!-- /.widget-main -->

        </div>


    </div><!-- /.col -->

</div>