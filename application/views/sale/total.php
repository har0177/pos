<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Date Wise Report of Profit from Sale

        <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->

<script>
    $(document).ready(function () {
       var myTable = $('.table').DataTable({
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotal = api
                        .column(5, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(5).footer()).html(
                        'Rs.' + pageTotal + ' (Total Rs. ' + total + ')'
                        );
                
                 // Total over all pages
                totalb = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotalb = api
                        .column(4, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(4).footer()).html(
                        'Rs.' + pageTotalb + ' (Total Rs. ' + totalb + ')'
                        );
            }
        });
          $('.table tbody').on('click', 'tr', function () {

            $(this).toggleClass('selected');
            var totalSUM = 0;
            $("tbody tr.selected").each(function () {
                var getValue = $(this).find("td:eq(5)").html().replace("$", "");
                var filteresValue = getValue.replace(/\,/g, '');
                totalSUM += Number(filteresValue)
                console.log(filteresValue);
            });

            console.log($('tfoot tr > th').eq(1)
                    .html('Rs.' + totalSUM));


            //alert( 'Column sum is: '+ table.column( 5 ).data().sum() );
        });
 
    
                $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                new $.fn.dataTable.Buttons(myTable, {
                    buttons: [
                        {
                            "extend": "csv",
                            "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            
                            footer: true
                        },
                        {
                            "extend": "copy",
                            "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                            
                            footer: true,
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "excel",
                            "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                            
                            footer: true,
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "pdf",
                            "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            
                            footer: true,
                              message: "<h1 class='center'>Date Wise Report of Sale</h1>"
                        },
                        {
                            "extend": "print",
                            "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            autoPrint: true,
                            footer: true,
                            message: "<h1 class='center'>Date Wise Report of Sale</h1>",
                            
                          
                        }
                
                    ]
                });
                myTable.buttons().container().appendTo($('.tableTools-container'));


              


   });


    
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Date Wise Report of Profit 
           
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <h2>From Date: <?php echo $from ?> &nbsp; To Date: <?php echo $to ?></h2>
                <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                <th>Sr No</th>
                <th>Invoice No</th>
                <th>Customer Name</th>
                <th> Sale</th>
                 <th>Discount</th>
                <th> Profit</th>
            



                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($result as
                        $r) {
                    ?>
                        <tr>
                            <td>
                                <?php echo $i ?>

                            </td>
                            <td>
                                <?php echo $r->inv_id ?>

                            </td>
                            <td>
                                <?php echo AdminLTE::customers(AdminLTE::customers_name($r->inv_id)) ?>

                            </td>
                            <?php AdminLTE::sale_count_all($r->inv_id, "sale", $from, $to); ?>



                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
                <tfoot>
                      <tr>
                        <th> SUM Selected</th>
                        <th> Rs.??? </th>
                        <th colspan="2" style="text-align:right">Total:</th>
                    <th></th>
                        
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>
