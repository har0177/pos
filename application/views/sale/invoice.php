<button onclick="window.print();" class=" hidden-print btn btn-success btn-large">
    <i class="ace-icon fa fa-print"></i> Print
</button>
<a href="javascript:window.history.go(-1)" class="btn btn-large btn-primary hidden-print pull-left"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
<script type="text/javascript">
    window.onload = function () {
        window.print();
    }
</script>
<style>
    @media print{
        .table{
            font-size: 16px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        .table th{
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }


    }



    table tbody td, th{
        background: 0 0;
        border: 1px solid #aaa;;
        white-space: nowrap;
        text-align: center;
        font-size: 1em;
        font-weight: bold;
    }


    table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa;
        font-weight: bold;
    }

    table tfoot tr:first-child td {
        border-top: none;
    }

    table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.3em;
        border-top: 1px solid #3989c6
    }

    table tfoot tr td:first-child {
        border: none;
    }

</style>

<a href="<?php echo site_url('admin/sale/add'); ?>" class="btn btn-success hidden-print pull-right">Add New Sale</a>
<div class="row" >
    <img class="img-responsive center-block" style="height: 120px;" id="imageshow" src="<?php echo site_url('assets/images/banner.jpg'); ?>" />
    <br>
    <div class="col-sm-4 col-xs-5" style="border: 1px solid black;">
        <h3 style="border-bottom: 1px solid black ">Bill To</h3>  
        <p>

<?php echo AdminLTE::customers(AdminLTE::customers_name($invoice)); ?>

            <br>

        </p>    
    </div>

    <div class="col-sm-8 col-xs-7"> 
        <table class="table table-responsive table-bordered">
            <tr>
                <th>Invoice ID</th>
                <th>
                    Bill No
                </th>
                <th>

                    Date
                </th>
            </tr>
            <tr>
                <td><?php echo $invoice; ?></td>

                <td>
                    <?php echo $billno->billno; ?></td>

                <td>
<?php echo date('d/m/Y', strtotime($date->date)); ?> 
                </td>
            </tr>
        </table>
    </div>       
</div>

<br> 
<table class="table table-responsive table-condensed" id="table1">

    <thead>


        <tr>

            <th>Sr.No</th>         
            <th>Products</th>
            <th>Quantity</th>
            <th>Price Each</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $total = 0;
        $return_amount = 0;
        $i = 1;
        foreach ($r as
                $value) {
            ?>
            <tr>

                <th><?php echo $i ?></th>
                <th>
                    <?php echo AdminLTE::cat_name(AdminLTE::category_name($value->product_id)) . " - " . AdminLTE::product_name($value->product_id); ?>
                </th>
                <td>
                    <?php
                    echo $value->sale . " ";
                    if (!empty(AdminLTE::sale_returns($value->product_id, $invoice))) {
                        echo "<span style='text-decoration: line-through dotted red'>(" . AdminLTE::sale_returns($value->product_id, $invoice) . ")</span>";
                        $return_amount += AdminLTE::sale_returns($value->product_id, $invoice) * $value->price;
                    }
                    ?>
                </td>
                <td>
    <?php echo $value->price; ?>
                </td>
                <td>
            <?php echo $value->sale * $value->price; ?>
                </td>

            </tr>
            <?php
            $i++;
            $total += $value->sale * $value->price;
        }
        ?>

    </tbody>

    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td>Gross Total</td>
            <td>Rs. <?php echo $totalamount->total + $return_amount; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Discount </td>
            <td  >Rs. - <?php echo $discount->discount; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Bilty </td>
            <td  >Rs. <?php echo $discount->bilty; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Old Balance</td>
            <td> Rs.  <?php echo $old->old; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Paid</td>
            <td> <?php
        if ($type_payment->type_payment == " " && $paid->paid > 0) {
            echo "Rs. " . $paid->paid;
        }
        else {
            if(!empty($type_payment->type_payment)){
            echo AdminLTE::bank_name(explode("-", $type_payment->type_payment)[0]) . " - " . explode("-", $type_payment->type_payment)[1] . " - Rs. " . $paid->paid;
        }
        }
        ?></td>
        </tr>
        <?php
        if (!empty($return_amount)) {
            ?>
            <tr>
                <td colspan="3"></td>
                <td>Return Amount</td>
                <td> Rs.  - <?php echo $return_amount; ?></td>
            </tr>
    <?php
}
?>
        <tr>
            <td colspan="3"></td>
            <td>Net Total</td>
            <td>Rs. <?php echo $net->net; ?></td>
        </tr>

    </tfoot>
</table>

<?php if (!empty($comments->comments)) { ?> 


    <span><p style="font-size: 12px"><b>Comments:</b>
    <?php echo $comments->comments ?></p></span>


<?php } ?>
<p style="font-size: 10px" class=" pull-left">NOTE: Damaged item will not be return. Thank You - Please come again.

</p>
<span class="text-center foot pull-right" style="font-size: 10px; font-weight: bold">Developed By Xpertz Dev </span>

