<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage Invoices 
          <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<script>
    $(document).ready(function () {
       var myTable = $('.table').DataTable({
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                        .column(8)
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Total over this page
                pageTotal = api
                        .column(8, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            var t = intVal(a) + intVal(b); return t.toFixed(2);
                        }, 0);

                // Update footer
                $(api.column(8).footer()).html(
                        'Rs.' + pageTotal + ' (Total Rs. ' + total + ')'
                        );
            }
        });
 
    
                $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                new $.fn.dataTable.Buttons(myTable, {
                    buttons: [
                        {
                            "extend": "csv",
                            "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            columns: ':not(:first):not(:last)'
                        },
                        {
                            "extend": "copy",
                            "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "excel",
                            "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "pdf",
                            "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                              message: "<h1 class='center'>Date Wise Report of Sale</h1>"
                        },
                        {
                            "extend": "print",
                            "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            autoPrint: true,
                            message: "<h1 class='center'>Date Wise Report of Sale</h1>"
                          
                        }
                    ]
                });
                myTable.buttons().container().appendTo($('.tableTools-container'));


              


   });


    
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage Invoices
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
           <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table class="table table-striped table-bordered table-hover">
                 <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Invoice No</th>
                        <th>Customer</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Old</th>
                           <th>Discount / Bilty</th>
                             <th>Payment</th>
                        <th>Net Amount</th>
                     
                         <th>Date</th>
                      
                        <th>Invoices</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as $r) {
                        ?>
                        <tr>
                                <td>
                                <?php echo $i ?>
                            </td>
                            <td>
                                <?php echo $r->inv_id ?>
                            </td>
                            
                            <td>
                                <?php echo AdminLTE::customers(AdminLTE::customers_name($r->inv_id)); ?>
                            </td>
                             <td>
                                <?php echo $r->total ?>
                            </td>
                             <td>
                                <?php echo $r->paid ?>
                            </td>
                             <td>
                                <?php echo $r->old ?>
                            </td>
                             <td>
                                <?php echo $r->discount . " / ". $r->bilty ?>
                            </td>
                             <td>
                                <?php 
                                if($r->type_payment == " "){
                                    echo "";
                                }else{
                                echo AdminLTE::bank_name(explode("-", $r->type_payment)[0]). " - ". explode("-", $r->type_payment)[1]; }?>
                            </td>
                             <td>
                                <?php echo $r->net ?>
                            </td>
                            <td>
                                <?php echo $r->date ?>
                            </td>
                     
                            <td>
                                <div class="hidden-sm action-buttons">
                                    <a title="Invoice Print"   href="<?php echo site_url('admin/sale/invoice_printing/' . $r->inv_id) ?>">
                                        <i class="ace-icon fa fa-print bigger-130"></i>
                                    </a>
                                    <a title="Edit Invoice" class="orange" href="<?php echo site_url('admin/sale/update/' . $r->inv_id) ?>">
                                        <i class="ace-icon fa fa-pencil-square-o bigger-130"></i>
                                    </a>
                                    
                                    <a title="Return Sale" class="green" href="<?php echo site_url('admin/returns/invoice_printing/' . $r->inv_id) ?>">
                                        <i class="ace-icon fa fa-undo bigger-130"></i>
                                    </a>
                                    
                                                                                                 <!--<a class="red" href="<?php echo site_url('admin/user/delete/' . $r->id) ?>">
                                                                                                                                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                                                                    </a> -->
                                </div>

                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                        
                </tbody>
                <tfoot>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>Total</th>
                <th></th>
                <th></th>
                
                </tfoot>
            </table>
        </div>
    </div>
</div>