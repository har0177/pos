<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage Invoices 
     <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage Invoices
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Bill No</th>
                        <th>Invoice No</th>
                        <th>Vendor</th>
                         <th>Total</th>
                        <th>Paid</th>
                        <th>Old</th>
                           <th>Discount</th>
                              <th>Bilty</th>
                                <th>Payment Type</th>
                        <th>Net Amount</th>
                   
                     
                         <th>Date</th>
                      
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as
                            $r) {
                        ?>
                        <tr>
                            <td>
    <?php echo $i?>
                            </td>
                            <td>
                                  <?php  echo $r->bill;
                                ?>
                               
                            </td>
                            <td>
    <?php echo $r->inv_id ?>
                            </td>

                             <td>
                                <?php echo AdminLTE::vendor_name(AdminLTE::customers_name($r->inv_id)); ?>
                            </td>
                           <td>
                                <?php echo $r->total ?>
                            </td>
                             <td>
                                <?php echo $r->paid ?>
                            </td>
                             <td>
                                <?php echo $r->old ?>
                            </td>
                              <td>
                                <?php echo $r->discount ?>
                            </td>
                            <td>
                                <?php echo $r->bilty ?>
                            </td>
                            <td>
                                <?php 
                                 if($r->type_payment == " "){
                                    echo "";
                                }else if($r->type_payment == "By Hand"){
                                   echo $r->type_payment;
                                }else{
                                echo AdminLTE::bank_name(explode("-", $r->type_payment)[0]). " - ". explode("-", $r->type_payment)[1]; }?>
                            </td>
                             <td>
                                <?php echo $r->net ?>
                            </td>
                     
                            <td>
                                <?php echo $r->date ?>
                            </td>

                            <td>
                                <div class="hidden-sm action-buttons">
                                    <a class="green" href="<?php echo site_url('admin/stock/invoice_printing/' . $r->inv_id) ?>">
                                        <i class="ace-icon fa fa-print bigger-130"></i>
                                    </a>
                                                                                                 <!--<a class="red" href="<?php echo site_url('admin/user/delete/' . $r->id) ?>">
                                                                                                                                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                                                                    </a> -->
                                </div>

                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>