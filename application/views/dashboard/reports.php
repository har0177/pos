
<div class="page-header">
    <h1>    <i class="ace-icon fa fa-dashboard"></i>
        <?php echo "Reports of Noor Corporation" ?>
        <?php
        echo date("d F, Y");
        ?>  </h1>
</div><!-- /.page-header -->


<div class="row">
    <div class="col-xs-12">

        <div id="timeline-1">
            <div class="row">

                <div class="col-xs-6 col-sm-6">
                    <div class="table-header">
                        Total Assets
                    </div>
                    <table id="" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr.No</th>

                                <th>Type</th>

                                <th>Debit</th>
                                <th>Total</th>


                            </tr>
                        </thead>
                        <?php
                        $liability = 0;
                        $assets = 0;

                        $q = $this->db->query('Select SUM(debit) as debit, SUM(discount) as discount from book');

                        $cus_debit = 0;
                        $cus_discount = 0;
                        foreach ($q->result() as
                                $r) {

                            $cus_debit = $r->debit;
                            $cus_discount = $r->discount;
                        }

                        $liability += $cus_discount;

                        $q1 = $this->db->query('Select SUM(debit) as debit, SUM(discount) as discount from book_vendor');
                        $vendor_debit = 0;
                        $vendor_discount = 0;
                        foreach ($q1->result() as
                                $r) {

                            $vendor_debit = $r->debit;
                            $vendor_discount = $r->discount;
                        }
                        $liability += $vendor_debit;
                        ?>

                        <tbody>

                            <tr>
                                <?php echo "<td>1</td><th>Vendor</th><td title='" . $vendor_debit . "'>" . convert_number($vendor_debit) . "</td>"; ?>
                                <td style="vertical-align: middle" title="<?php echo $liability ?>"><strong><?php echo convert_number($liability) ?></strong></td>
                            </tr>


                            <?php
                            $assets += $vendor_discount;
                            $assets += $cus_debit;

                            $st = $this->db->query('Select * from total_stock');
                            $totalstock = 0;
                            foreach ($st->result() as
                                    $stt) {
                                $totalstock += $stt->total_stock * AdminLTE::pprice($stt->product_id);
                            }
                            $assets += $totalstock;


                            $q2 = $this->db->query('Select SUM(deposit - withdraw) as bankbalance from bank_details')->row();


                            $assets += $q2->bankbalance;
                            ?>
                            <tr>
                            <?php
                            echo "<td>2</td><th>Customer</th><td title='" . $cus_debit . "'>" . convert_number($cus_debit) . "</td>";
                            ?>
                                <td rowspan="3" style="vertical-align: middle" title="<?php echo $assets ?>"><strong><?php echo convert_number($assets) ?></strong></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <th>Total Stock</th>

                                <td title="<?php echo $totalstock ?>"><?php echo convert_number($totalstock) ?></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <th>Bank Balance</th>

                                <td title="<?php echo $q2->bankbalance ?>"><?php echo convert_number($q2->bankbalance) ?></td>
                            </tr>

                            <tr>
                                <td>4</td>
                                <th>Total Assets</th>

                                <td colspan="2" title="<?php echo $assets - $liability ?>"><strong><?php echo convert_number($assets - $liability) ?></strong></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                
                <div class="col-sm-6 text-center col-xs-6">

                    <div id="users" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">

                    <div class="table-header">
                         Banks Payment
                    </div>

                    <table id="" class="table table-striped table-bordered table-hover">
                       
                         
                                <?php
                                $banks = $this->db->query('Select * from banks');
foreach ($banks->result() as
        $bank_name) {
    $amount = $this->db->query("SELECT SUM(deposit - withdraw) as amount from bank_details where bank_id = $bank_name->id")->row();
                                ?>
                               <tr>
                                <th><?php echo $bank_name->bank_name ?></th>
                                <td title="<?php echo $amount->amount ?>"><?php echo  convert_number($amount->amount) ?></td>
                               </tr>

        <?php } ?>
                          
                       

                     
                    </table>


                </div>
                <div class="col-sm-8">
                    <div id="expenses" style="min-width: 300px; height: 400px; margin: 0 auto"></div>

                </div>

            </div>
            <div class="row">
                <div class="col-sm-6">

                    <div class="table-header">
                        Profit / Loss Data
                    </div>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container"></div>
                    </div>
                    <table id="dyntable" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr.No</th>

                                <th>Profit</th>
                                <th>Discount</th>
                                <th>Expanse</th>
                                <th>Revenue</th>
                                <th>Date</th>


                            </tr>
                        </thead>

                        <tbody>
<?php
$i = 1;
$totalp = 0;
$totale = 0;
$discount = 0;
foreach ($result as
        $r) {
    ?>
                                <tr>
                                    <td>
                                <?php echo $i ?>
                                    </td>


                                    <td title="<?php echo AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date); ?>">
    <?php
    $totalp += AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date);

    echo convert_number(AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date));
    ?>
                                    </td>
                                    <td title="<?php echo AdminLTE::discount($r->date); ?>">
                                        <?php
                                        $discount += AdminLTE::discount($r->date);
                                        echo convert_number(AdminLTE::discount($r->date));
                                        ?>
                                    </td>
                                    <td title="<?php echo AdminLTE::expanse_date($r->date); ?>">
                                        <?php
                                        $totale += AdminLTE::expanse_date($r->date);
                                        echo convert_number(AdminLTE::expanse_date($r->date));
                                        ?>
                                    </td>
                                    <td title="<?php echo AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date) - AdminLTE::discount($r->date) - AdminLTE::expanse_date($r->date); ?>">
                                        <?php echo convert_number(AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date) - AdminLTE::discount($r->date) - AdminLTE::expanse_date($r->date)) ?>

                                    </td>
                                    <td>
    <?php echo dateformatesformysql_fata($r->date) ?>
                                    </td>

                                </tr>
    <?php
    $i++;
}
?>

                            <tr>
                                <th>Total</th>
                                <th title="<?php echo $totalp; ?>"><?php echo convert_number($totalp); ?></th>
                                <th title="<?php echo $discount; ?>"><?php echo convert_number($discount) ?></th>
                                <th title="<?php echo $totale; ?>"><?php echo convert_number($totale); ?></th>
                                <th title="<?php echo $totalp - $totale - $discount; ?>">
<?php echo convert_number($totalp - $totale - $discount); ?>
                                </th>
                                <th></th>
                            </tr>
                        </tbody>
                    </table>
                </div>   


<?php
$q = $this->db->query('Select * from banks');
$total = 0;
$dp = 0;
$wd = 0;
foreach ($q->result() as
        $bank_name) {
    ?>
                    <div class="col-sm-6">

                        <div class="table-header">
    <?php echo $bank_name->bank_name ?>
                        </div>

                        <table id="" class="table table-striped table-bordered table-hover display">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>

                                    <th>Deposit</th>

                                    <th>Withdraw</th>
                                    <th>Remaining</th>

                                    <th>Date</th>


                                </tr>
                            </thead>

                            <tbody>
    <?php
    $i = 1;
    $date = date("m");
    $year = date("Y");
    $bank = $bank_name->id;
    $q1 = $this->db->query("Select bank_id, date, SUM(deposit) as deposit, SUM(withdraw) as withdraw from bank_details where bank_id = $bank  and Year(date) = $year and MONTH(date) = $date group by date");

    foreach ($q1->result() as
            $r) {

        $total += ($r->deposit - $r->withdraw);
        $dp += $r->deposit;
        $wd += $r->withdraw;
        ?>
                                    <tr>
                                        <td>
                                    <?php echo $i ?>
                                        </td>

                                        <td title=" <?php echo $r->deposit
                            ?>">
        <?php echo convert_number($r->deposit);
        ?>

                                        </td>

                                        <td title=" <?php echo $r->withdraw
        ?>">
        <?php echo convert_number($r->withdraw); ?>
                                        </td>
                                        <td title=" <?php echo $total
        ?>">
        <?php echo convert_number($total);
        ?>

                                        </td>


                                        <td>
        <?php echo $r->date ?>
                                        </td>

                                    </tr>
        <?php
        $i++;
    }
    ?>
                                <tr>

                                    <td>
                                        <b>Total</b>
                                    </td>
                                    <td title="<?php echo $dp; ?>">
                                        <b>  <?php echo convert_number($dp); ?></b>
                                    </td>

                                    <td title="<?php echo $wd; ?>">
                                        <b>  <?php echo convert_number($wd); ?></b>
                                    </td>
                                    <td title="<?php echo $dp - $wd; ?>"><b> <?php echo convert_number($dp - $wd); ?></b></td>
                                    <td></td>
                                </tr>

                            </tbody>
                        </table>


                    </div>
    <?php
    $total = 0;
    $dp = 0;
    $wd = 0;
}
?>

                
            </div>
        </div>




    </div>

    <script>
        $(function () {

            Highcharts.chart('users', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ' Customers / Vendors / Category / Products Chart'
                },
                tooltip: {
                    headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {y}',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                        name: 'Total',
                        colorByPoint: true,
                        data: [{
                                name: 'Customers',
                                y: <?php
                            if ($shop == 0) {
                                echo 0;
                            }
                            else {
                                echo $shop;
                            }
?>
                            }, {
                                name: 'Vendor',
                                y: <?php
                            if ($vendor == 0) {
                                echo 0;
                            }
                            else {
                                echo $vendor;
                            }
?>
                            }, {
                                name: 'Products',
                                y: <?php
                            if ($products == 0) {
                                echo 0;
                            }
                            else {
                                echo $products;
                            }
?>
                            }, {
                                name: 'Catgeory',
                                y: <?php
                            if ($category == 0) {
                                echo 0;
                            }
                            else {
                                echo $category;
                            }
?>
                            }]
                    }]
            });
        });</script>


    <script>
        $(function () {

            Highcharts.chart('expenses', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Revenue / Expenses Detail of  <?php echo date("Y") ?>'
                },
                xAxis: {
                    categories:
<?php
$year = date("Y");
$qc = $this->db->query("SELECT MONTHNAME(date) as date, MONTH(date) as month from sale where YEAR(date) = $year group by MONTH(date)");


echo json_encode(array_columnn($qc->result(), 'date'), JSON_NUMERIC_CHECK);
?>,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Expenses Detail'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.1,
                        borderWidth: 0
                    },
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: 'Rs. {y}'
                        }
                    }
                },
                series: [
<?php
$dated = array_columnn($qc->result(), 'month');
$profit = array();
$expanse = array();
$rv = array();
$vn_disc = array();

foreach ($dated as
        $dd) {

    $profit[] = round(AdminLTE::profit_month($dd), 2);
    $expanse[] = round(AdminLTE::expenses_month($dd), 2);
    $vn_disc[] = round(AdminLTE::vendor_discount_month($dd), 2);
    $rv[] = round(AdminLTE::profit_month($dd) + AdminLTE::vendor_discount_month($dd) - AdminLTE::expenses_month($dd) - AdminLTE::discount_month($dd), 2);
}
?>
                    {
                        name: 'Profit',
                        data: <?php echo json_encode($profit, JSON_NUMERIC_CHECK) ?>
                    }, {
                        name: 'Expense',
                        data: <?php echo json_encode($expanse, JSON_NUMERIC_CHECK) ?>,
                    },
                    {
                        name: 'Vendor Discount',
                        data: <?php echo json_encode($vn_disc, JSON_NUMERIC_CHECK) ?>,
                    }, {
                        name: 'Revenue',
                        data: <?php echo json_encode($rv, JSON_NUMERIC_CHECK) ?>

                    },
<?php ?>



                ]




            });
        });</script>





</div>
</div>






