
<div class="page-header hidden-print">
    <h1>    <i class="ace-icon fa fa-dashboard"></i>
       <?php echo "Search Record" ?>
         </h1>
</div><!-- /.page-header -->


<div class="row">
    <div class="col-xs-12">

           
			
              
<div class="widget-box hidden-print">
             <h2 class="text-center" style="color: black">
                Search Date Wise 
            </h2>
            <div class="widget-body">
                <div class="widget-main">

                    <div id="fuelux-wizard-container">

                        <div class="step-content pos-rel">

                            <?php echo form_open('', ['class' => 'form-horizontal']); ?>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="status">From Date: </label>

                                <div class="col-xs-12 col-sm-3">
                                    <input type="text" name="from" value="<?php echo date('Y-m-d'); ?>" class="form-control datepicker"/>
                                </div>
                                <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="status">To Date: </label>

                                <div class="col-xs-12 col-sm-3">
                                    <input type="text" name="to" value="<?php echo date('Y-m-d'); ?>" class="form-control datepicker"/>
                                </div>
                                <div class="col-xs-12 col-sm-2">
                                    <input type="submit" name="submit" value="Search" class="btn btn-sm btn-success">


                                </div>
                            </div>


                            <?php echo form_close(); ?>

                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>
            </div>
        </div>
                     <?php
        if (isset($_POST["submit"])) {
            
            $from = $this->input->post('from', TRUE);

            $to = $this->input->post('to', TRUE);

             $result = $this->db->query('Select date from sale where DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '" UNION Select date from expenses where DATE(date) BETWEEN "' . $from . '"  AND "' . $to . '" group by date order by date ASC');
             
            if ($result->num_rows() > 0) {
            ?>
                      <div class="table-header">
                          Profit / Loss Data From <?php echo dateformatesformysql_fata($from). " to " . dateformatesformysql_fata($to) ?>
                    </div>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container"></div>
                    </div>
                       <table id="" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr.No</th>

                                <th>Profit</th>
                                <th>Discount</th>
                                <th>Expanse</th>
                                <th>Revenue</th>
                                <th>Date</th>


                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $i = 1;
                            $totalp = 0;
                            $totale = 0;
                            $discount = 0;
                            foreach ($result->result() as
                                    $r) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $i ?>
                                    </td>


                                    <td>
                                        <?php 
                                        $totalp += AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date);
                                        echo AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date); ?>
                                    </td>
                                    <td>
                                        <?php
                                        $discount += AdminLTE::discount($r->date);
                                        echo convert_number(AdminLTE::discount($r->date));
                                        ?>
                                    </td>
                                    <td>
                                         <?php
                                        $totale += AdminLTE::expanse_date($r->date);
                                        echo convert_number(AdminLTE::expanse_date($r->date));
                                        ?>
                                    </td>
                                    <td>
                                      <?php echo convert_number(AdminLTE::profit_date($r->date) + AdminLTE::vendor_discount($r->date) - AdminLTE::discount($r->date) - AdminLTE::expanse_date($r->date)) ?>
                                    </td>
                                    <td>
                                        <?php echo dateformatesformysql_fata($r->date) ?>
                                    </td>

                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                            <tr>
                                <th>Total</th>
                                <td><?php echo $totalp; ?></td>
                                <td><?php echo $discount ?></td>
                                <td><?php echo $totale; ?></td>
                                <td>
                                    <?php echo $totalp - $totale - $discount; ?>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php
        }}
                ?>
                    
                  
               

           

            </div>
        </div>
       


