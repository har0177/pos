
<div class="page-header">
    <h1>    <i class="ace-icon fa fa-dashboard"></i>
        <?php echo "Dashboard of Noor Corporation" ?>
        <?php
        echo date("d F, Y");
        ?>  </h1>
</div><!-- /.page-header -->


<div class="row">
    <div class="col-xs-12">

        <div id="timeline-1">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <div class="timeline-container">
                        <div class="timeline-label">
                            <span class="label label-primary arrowed-in-right label-lg">
                                <b>Customer / Vendor Information</b>
                            </span>
                        </div>

                        <div class="timeline-items">


                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-success arrowed-in-right label-lg" href="<?php echo site_url('admin/vendor'); ?>"> Vendors </a>

                                </div>

                            </div>

                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-success arrowed-in-right label-lg" href="<?php echo site_url('admin/customer'); ?>"> Customers </a>

                                </div>

                            </div>


                        </div><!-- /.timeline-items -->
                    </div><!-- /.timeline-container -->

                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="timeline-container">
                        <div class="timeline-label">
                            <span class="label label-danger arrowed-in-right label-lg">
                                <b>Category / Products Information</b>
                            </span>
                        </div>

                        <div class="timeline-items">


                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-info arrowed-in-right label-lg" href="<?php echo site_url('admin/category'); ?>"> Categories </a>

                                </div>

                            </div>

                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-info arrowed-in-right label-lg" href="<?php echo site_url('admin/products'); ?>"> Products </a>

                                </div>

                            </div>


                        </div><!-- /.timeline-items -->
                    </div><!-- /.timeline-container -->

                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="timeline-container">
                        <div class="timeline-label">
                            <span class="label label-warning arrowed-in-right label-lg">
                                <b>Stock Information</b>
                            </span>
                        </div>

                        <div class="timeline-items">


                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-danger arrowed-in-right label-lg" href="<?php echo site_url('admin/stock/add'); ?>"> Add Stock </a>

                                </div>

                            </div>

                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-danger arrowed-in-right label-lg" href="<?php echo site_url('admin/stock'); ?>"> All Stocks </a>
                                </div>

                            </div>
                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-danger arrowed-in-right label-lg" href="<?php echo site_url('admin/sale/invoice_vendor'); ?>"> Vendor Invoice </a>
                                </div>

                            </div>


                        </div><!-- /.timeline-items -->
                    </div><!-- /.timeline-container -->

                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="timeline-container">
                        <div class="timeline-label">
                            <span class="label label-success arrowed-in-right label-lg">
                                <b>Sale Information</b>
                            </span>
                        </div>

                        <div class="timeline-items">


                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-warning arrowed-in-right label-lg" href="<?php echo site_url('admin/sale/add'); ?>"> Add Sale (Retailer) </a>

                                </div>

                            </div>

                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-warning arrowed-in-right label-lg" href="<?php echo site_url('admin/sale/add_holesale'); ?>"> Add Sale (Wholesaler) </a>

                                </div>

                            </div>

                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-warning arrowed-in-right label-lg" href="<?php echo site_url('admin/sale'); ?>"> All Sale </a>
                                </div>

                            </div>
                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-warning arrowed-in-right label-lg" href="<?php echo site_url('admin/sale/invoice'); ?>"> Customer Invoice </a>
                                </div>

                            </div>


                        </div><!-- /.timeline-items -->
                    </div><!-- /.timeline-container -->

                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="timeline-container">
                        <div class="timeline-label">
                            <span class="label label-default arrowed-in-right label-lg">
                                <b>Bank Information</b>
                            </span>
                        </div>

                        <div class="timeline-items">


                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-primary arrowed-in-right label-lg" href="<?php echo site_url('admin/banks/all_details'); ?>"> Banks Statements </a>

                                </div>

                            </div>

                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-primary arrowed-in-right label-lg" href="<?php echo site_url('admin/banks/transfer'); ?>"> Transfer Payment </a>

                                </div>

                            </div>

                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-primary arrowed-in-right label-lg" href="<?php echo site_url('admin/book'); ?>"> Customer Payments </a>
                                </div>

                            </div>
                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-primary arrowed-in-right label-lg" href="<?php echo site_url('admin/book/vendor'); ?>"> Vendor Payments </a>
                                </div>

                            </div>


                        </div><!-- /.timeline-items -->
                    </div><!-- /.timeline-container -->

                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="timeline-container">
                        <div class="timeline-label">
                            <span class="label label-warning arrowed-in-right label-lg">
                                <b>Expenses / Return Info</b>
                            </span>
                        </div>

                        <div class="timeline-items">


                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-warning arrowed-in-right label-lg" href="<?php echo site_url('admin/expenses'); ?>"> All Expenses </a>

                                </div>

                            </div>


                            <div class="timeline-item clearfix">
                                <div class="timeline-label">
                                    <a class="label label-warning arrowed-in-right label-lg" href="<?php echo site_url('admin/returns'); ?>"> All Returns </a>
                                </div>



                            </div>



                        </div>




                    </div><!-- /.timeline-container -->

                </div>
             
            </div>
            


</div>
</div>
</div>






