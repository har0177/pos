<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta charset="<?php echo $this->config->item('charset'); ?>">
        <title><?php echo $heading; ?> : Dashboard - Admin Panel</title>
       <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ace.min.css'); ?>" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" />
         <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.custom.min.css'); ?>" />
       
        <link rel="stylesheet" href="<?php echo base_url('assets/css/chosen.min.css'); ?>" />
        <script src="<?php echo base_url('assets/js/ace-extra.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/jquery.min.js'); ?>"></script>
<link href="<?php echo base_url('assets/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/bootstrap-datepicker.min.js')?>"></script>
   <script src="<?php echo base_url(); ?>assets/highcharts.js"></script>
            <script src="<?php echo base_url(); ?>assets/exporting.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/Sweetalerts.css'); ?>">
       <script src="<?php echo base_url(); ?>assets/Sweetalert.js"></script> 
       <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootoast.css'); ?>">
       <script src="<?php echo base_url(); ?>assets/bootoast.js"></script> 
            <style type="text/css" media="print">
                
                @page{
                    size: auto;
                    margin-top: 0mm;
                    margin-bottom: 0mm;
                }
            </style>
<script type="text/javascript">
    $(document).ready(function() {

     //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });
    
    //datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			

    });
    
</script>
        <style>
           
            .profile-user-info-striped .profile-info-value {
    border: 1px dotted #DCEBF7; 
    padding-left: 12px;
}

.profile-info-row:first-child .profile-info-name, .profile-info-row:first-child .profile-info-value {
    
    border: 1px dotted #DCEBF7;
}
            .timeline-label {
    display: block;
    clear: both;
    margin: 0px 0 10px 36px;
}

.profile-user-info-striped {
   border: none;
}
.label-lg {
    padding: .3em .6em .4em;
    font-size: 14px;
    line-height: 1.1;
    font-weight: bold;
    height: 24px;
   
}
            .viewname{
                text-align: center;
                width: 150px;

            }

            .viewname1{
                text-align: center;
                width: 250px;

            }

            .borderless tr th {
                border: none !important;
            }
            .borderless tr td {
                border-top: 1px solid black;
                border-bottom: 1px solid black;
            }

        </style>

    </head>
    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default">
            <div class="navbar-container" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-header pull-left">
                    <a class="navbar-brand">	<small>	<i class="fa fa-book"></i>	    Noor Corporation</small></a>
                </div>
                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li><a href="">  Riaz Ahmad</a></li>
                        <li><a href="<?php echo base_url('admin/user/profile') ?>"><i class="ace-icon fa fa-user"></i>Profile</a></li>
                        <li><a href="<?php echo base_url('admin/user/logout'); ?>"> <i class="ace-icon fa fa-power-off"></i>Logout</a> </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>
        <div class="main-container" id="main-container">
            <div id="sidebar" class="sidebar responsive">
                <ul class="nav nav-list">
                    <li class="active"> 
                        <a href="<?php echo site_url('admin/dashboard'); ?>">  
                            <i class="menu-icon fa fa-tachometer"></i>
                            <span class="menu-text"> Dashboard </span>   
                        </a> <b class="arrow"></b>
                    </li>


                  
                            <li class="active">
                                <a href="<?php echo site_url('admin/vendor'); ?>"> <i class="menu-icon fa fa-briefcase"></i> All Vendors </a>
                                <b class="arrow"></b>
                            </li>
                       
                       
                
                    
                            <li class="active">
                                <a href="<?php echo site_url('admin/customer'); ?>"> <i class="menu-icon fa fa-briefcase"></i> All Customers </a>
                                <b class="arrow"></b>
                            </li>
                        
                    
                   
                            <li class="active">
                                <a href="<?php echo site_url('admin/category'); ?>"> <i class="menu-icon fa fa-clipboard"></i> All Category </a>
                                <b class="arrow"></b>
                            </li>
                          
                        
                   
                            <li class="active">
                                <a href="<?php echo site_url('admin/products'); ?>"> <i class="menu-icon fa fa-clipboard"></i> All Products </a>
                                <b class="arrow"></b>
                            </li>
                         
                    <li class="">  
                        <a href="#" class="dropdown-toggle"> 
                            <i class="menu-icon fa fa-book"></i>
                            <span class="menu-text">   Manage Stock </span> 
                            <b class="arrow fa fa-angle-down"></b></a>
                        <b class="arrow"></b>
                        <ul class="submenu"> 
                            <li class="">
                                <a href="<?php echo site_url('admin/stock'); ?>"> <i class="menu-icon fa fa-book"></i> All Stocks </a>
                                <b class="arrow"></b>
                            </li>
                             <li class="">	
                                <a href="<?php echo site_url('admin/stock/add'); ?>">
                                    <i class="menu-icon fa fa-plus-square"></i>  Add New Stock </a> <b class="arrow"></b></li>
                           
                            <li class="">
                                <a href="<?php echo site_url('admin/stock/total'); ?>"> <i class="menu-icon fa fa-book"></i> Total Stocks </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="<?php echo site_url('admin/stock/search'); ?>"> <i class="menu-icon fa fa-book"></i> Export Stock </a>
                                <b class="arrow"></b>
                            </li>
                            		<li class="">
                                <a href="<?php echo site_url('admin/stock/lowstock'); ?>"> <i class="menu-icon fa fa-book"></i> Low Stock </a>
                                <b class="arrow"></b>
                            </li>
                        </ul> 
                    </li>

                       <li class="">  
                        <a href="#" class="dropdown-toggle"> 
                            <i class="menu-icon fa fa-clipboard"></i>
                            <span class="menu-text">   Manage Sale </span> 
                            <b class="arrow fa fa-angle-down"></b></a>
                        <b class="arrow"></b>
                        <ul class="submenu">  
<!--                            <li class="">
                                <a href="<?php echo site_url('admin/sale'); ?>"> <i class="menu-icon fa fa-clipboard"></i> All Sale </a>
                                <b class="arrow"></b>
                            </li>-->
                             <li class="">	
                                <a href="<?php echo site_url('admin/sale/add'); ?>">
                                    <i class="menu-icon fa fa-plus-square"></i>  Add New Sale </a> <b class="arrow"></b></li>
                            <li class="">
                                <a href="<?php echo site_url('admin/sale/total'); ?>"> <i class="menu-icon fa fa-book"></i> Total Customers Sale </a>
                                <b class="arrow"></b>
                            </li>
							<li class="">
                                <a href="<?php echo site_url('admin/sale/report'); ?>"> <i class="menu-icon fa fa-book"></i> Total Product Sale </a>
                                <b class="arrow"></b>
                            </li>
                            
                          
                        </ul> 
                    </li>

 <li class="">  
                        <a href="#" class="dropdown-toggle"> 
                            <i class="menu-icon fa fa-clipboard"></i>
                            <span class="menu-text">   Manage Invoices </span> 
                            <b class="arrow fa fa-angle-down"></b></a>
                        <b class="arrow"></b>
                        <ul class="submenu"> 
                            <li class="">
                                <a href="<?php echo site_url('admin/sale/invoice'); ?>"> <i class="menu-icon fa fa-clipboard"></i> Customers Invoices </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="<?php echo site_url('admin/sale/invoice_vendor'); ?>"> <i class="menu-icon fa fa-clipboard"></i> Vendor Invoices </a>
                                <b class="arrow"></b>
                            </li>
                        </ul> 
                    </li>


<li class="">
                            <a href="" class="dropdown-toggle">
                                <i class="menu-icon fa fa-wifi"></i>
                                <span class="menu-text">   Manage Banks </span> <b class="arrow fa fa-angle-down"></b></a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li class="">
                                    <a href="<?php echo site_url('admin/banks'); ?>">
                                        <i class="menu-icon fa fa-money"></i> All Banks </a>
                                    <b class="arrow"></b>
                                </li>
                                 <li class="">
                                    <a href="<?php echo site_url('admin/banks/all_details'); ?>">
                                        <i class="menu-icon fa fa-money"></i> Banks Statements </a>
                                    <b class="arrow"></b>
                                </li>
                                <li class="">
                                <a href="<?php echo site_url('admin/book'); ?>"> <i class="menu-icon fa fa-briefcase"></i> Customer Payment </a>
                                <b class="arrow"></b>
                            </li>

                            <li class="">
                                <a href="<?php echo site_url('admin/book/vendor'); ?>"> <i class="menu-icon fa fa-briefcase"></i> Vendor Payment </a>
                                <b class="arrow"></b>
                            </li>
                            </ul>
                        </li>
                       
                                <li class="active">
                                    <a href="<?php echo site_url('admin/expenses'); ?>">
                                        <i class="menu-icon fa fa-money"></i> All Expenses </a>
                                    <b class="arrow"></b>
                                </li>
               
              <li class="">
                        <a href="" class="dropdown-toggle">
                            <i class="menu-icon fa fa-phone-square"></i>
                            <span class="menu-text">   Manage SMS </span> <b class="arrow fa fa-angle-down"></b></a>
                        <b class="arrow"></b>
                        <ul class="submenu">
                            <li class="">
                                <a href="<?php echo site_url('admin/sms/all_sms'); ?>"> <i class="menu-icon fa fa-book"></i> All SMS Templates </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="<?php echo site_url('admin/sms/single'); ?>">
                                    <i class="menu-icon fa fa-phone"></i> Send Single SMS </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="<?php echo site_url('admin/sms/vendor'); ?>">
                                    <i class="menu-icon fa fa-phone"></i> Send Vendor SMS </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="<?php echo site_url('admin/sms/customer'); ?>">
                                    <i class="menu-icon fa fa-phone"></i> Send Customer SMS</a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="<?php echo site_url('admin/sms/logs'); ?>">
                                    <i class="menu-icon fa fa-phone"></i> SMS Logs</a>
                                <b class="arrow"></b>
                            </li>
                        
                        </ul>
                    </li>
               
                            <li class="active">
                                <a href="<?php echo site_url('admin/dashboard/reports'); ?>">
                                    <i class="menu-icon fa fa-pie-chart"></i> Reports </a>
                                <b class="arrow"></b>
                            </li>
							  <li class="active">
                                <a href="<?php echo site_url('admin/dashboard/search'); ?>">
                                    <i class="menu-icon fa fa-pie-chart"></i> Search Reports </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="active">
                                <a href="<?php echo site_url('backup/db_backup'); ?>">
                                    <i class="menu-icon fa fa-database"></i> Backup of DB </a>
                                <b class="arrow"></b>
                            </li>
                       

                    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                        <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                    </div>

            </div>
            <div class="main-content">
                <div class="main-content-inner">
                    <div class="page-content">
                            <?php
                            flash_alert();
                            echo validation_errors('<div class="alert alert-danger">', '</div>');
                            ?>
                     

                        <?php
                        echo $body;
                        ?>
                        <br>

                    </div>
                </div>
            </div>

            <div class="footer hidden-print">
                <div class="footer-inner">
                    <div class="footer-content">
                        <span class="bigger-120">
                            <span class="blue bolder">   Noor Corporation</span>
                            <br>
                            Xpertz Dev &copy; <?php echo date("Y") ?>
                        </span>

                        &nbsp; &nbsp;
                    </div>
                </div>
            </div>


            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <script src="<?php echo base_url('assets/bootstrap.min.js'); ?>"></script>
        <!-- page specific plugin scripts -->
        <script src="<?php echo base_url('assets/jquery-ui.custom.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/bootbox.js'); ?>"></script>

        <script src="<?php echo base_url('assets/js/chosen.jquery.min.js'); ?>"></script>

        <!-- ace scripts -->
        <script src="<?php echo base_url('assets/js/ace-elements.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/ace.min.js'); ?>"></script>

        <script src="<?php echo base_url() ?>assets/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/buttons.flash.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/buttons.html5.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/buttons.print.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/buttons.colVis.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/buttons.pdfmake.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/buttons.vfs_fonts.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.table2excel.min.js"></script>

<script src="<?php echo base_url() ?>assets/bootstrap-number-input.js" ></script>
        <script type="text/javascript">
$('.colorful').bootstrapNumber({
	upClass: 'success',
	downClass: 'danger'
});
            jQuery(function ($) {

                if (!ace.vars['touch']) {
                    $('.chosen-select').chosen({allow_single_deselect: true});
                    $('#form-field-select-4').addClass('tag-input-style');
                    //resize the chosen on window resize

                    $(window)
                            .off('resize.chosen')
                            .on('resize.chosen', function () {
                                $('.chosen-select').each(function () {
                                    var $this = $(this);
                                    $this.next().css({'width': $this.parent().width()});
                                })
                            }).trigger('resize.chosen');
                    //resize chosen on sidebar collapse/expand
                    $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
                        if (event_name != 'sidebar_collapsed')
                            return;
                        $('.chosen-select').each(function () {
                            var $this = $(this);
                            $this.next().css({'width': $this.parent().width()});
                        })
                    });


                    $('#chosen-multiple-style .btn').on('click', function (e) {
                        var target = $(this).find('input[type=radio]');
                        var which = parseInt(target.val());
                        if (which == 2)
                            $('#form-field-select-4').addClass('tag-input-style');
                        else
                            $('#form-field-select-4').removeClass('tag-input-style');
                    });
                }

            });

            jQuery(function ($) {
                //initiate dataTables plugin
                var myTable =
                        $('#dyntable').DataTable({"sPaginationType": "full_numbers",
                    "aaSortingFixed": [[0, 'asc']],
                    "bSort": false,
                });

                
         
    $('table.display').DataTable({"sPaginationType": "full_numbers",
                    "aaSortingFixed": [[0, 'asc']],
                    "bSort": false,
                });



                $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                new $.fn.dataTable.Buttons(myTable, {
                   buttons: [
                         {
                            "extend": "csv",
                            "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                             title: "Noor Corporation" + $('.table-header').text()
                            
                        },
                        {
                            "extend": "copy",
                            "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            title: "Noor Corporation" + $('.table-header').text()
                        },
                        {
                            "extend": "excel",
                            "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            title: "Noor Corporation" + $('.table-header').text()
                        },
                        {
                            "extend": "pdf",
                            "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            title: "Noor Corporation" + $('.table-header').text()
                            
                        },
                        {
                            "extend": "print",
                            "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            title: "Noor Corporation" + $('.table-header').text(),
                            autoPrint: false
                            
                            
                        }
                    ]
                });
                myTable.buttons().container().appendTo($('.tableTools-container'));

                


                $(document).on('click', '#dyntable .dropdown-toggle', function (e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
                });



            });
            jQuery(document).ready(function () {
                // dynamic table

                $('#export').click(function () {
                    //Some code
                    jQuery("#dyntableExport").table2excel({
                        exclude: ".noExl",
                        name: "Excel Document Name",
                        filename: "Backup",
                        fileext: ".xls",
                        exclude_img: true,
                        exclude_links: true,
                        exclude_inputs: true,
                        "sPaginationType": "full_numbers",
                        "aaSortingFixed": [[0, 'asc']],
                        "fnDrawCallback": function (oSettings) {
                            jQuery.uniform.update();
                        }
                    });
                });


            });



        </script>

    </body>

</html>
