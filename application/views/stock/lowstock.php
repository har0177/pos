<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Report of <?php echo $heading; ?> 

        <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Report of  <?php echo $heading; ?>  Less Than 5
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Total Stock</th>
                    </tr>
                </thead>
                <tbody>
                    
                
                    <?php
                    $result = $this->db->query("SELECT * from total_stock where total_stock < 5");
                    foreach ($result->result() as
                            $r) {
                        ?>
                        <tr>
                            <td>
                                <?php echo AdminLTE::cat_name(AdminLTE::category_name($r->product_id)) . " - " . AdminLTE::product_name($r->product_id)
                                ?>

                            </td>
                            <td>
    <?php echo $r->total_stock; ?>
                            </td>



                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>