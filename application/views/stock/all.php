<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
        <a href="<?php echo site_url('admin/stock/add'); ?>" class="btn btn-sm btn-success pull-right">  
            <i class="ace-icon fa fa-plus-square"></i> Add New</a>
                 <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage <?php echo $heading; ?> 
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
           <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                   <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Bill No</th>
                        <th>Vendor Name</th>
                        <th>Product Name</th>
                        <th>Stock</th>
                        <th>Purchase Price</th>
                        <th>Sale Price</th>
                        <th>Date</th>
                      
                      
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as $r) {
                        ?>
                        <tr>
                            <td>
    <?php echo $i ?>
                            </td>
                             <td>
                                  <?php  echo $r->bill;
                                ?>
                               
                            </td>
                            <td>
                                  <?php  echo AdminLTE::vendor_name(AdminLTE::customers_name($r->inv_id));
                                ?>
                               
                            </td>
                            <td>
                                <?php  echo AdminLTE::cat_name(AdminLTE::category_name($r->product_id)). " - " . AdminLTE::product_name($r->product_id)
                                ?>
                               
                            </td>
                            <td>
                                <?php echo $r->stock ?>
                            </td>
                            <td>
                                <?php echo AdminLTE::pprice($r->product_id) ?>
                            </td>
                            <td>
                                <?php echo AdminLTE::sprice($r->product_id) ?>
                            </td>
                            
                             <td>
                                <?php echo dateformatesformysql_fata($r->date) ?>
                            </td>
                     
                          
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>