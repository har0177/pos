<button onclick="window.print();" class=" hidden-print btn btn-success btn-large">
    <i class="ace-icon fa fa-print"></i> Print
</button>
 <style>
         @media print{
            .table{
                 font-size: 16px;
                 font-family: "Arial, Helvetica, sans-serif";
             }
             .table th{
                 font-size: 14px;
                 font-family: "Arial, Helvetica, sans-serif";
             }
         }
     </style>


<div class="row">

      <h2 style="text-align: center">NOOR CORPORATION</h2>
            <h3 style="text-align: center">Abasin Market No.2 Mingora Swat</h3>
            <h4 style="text-align: center">Mobile No: 0346 - 9426741 </h4>
   
    <div class="col-sm-4 col-xs-5" style="border: 1px solid black;">
        <h3 style="border-bottom: 1px solid black ">Bill From</h3>  
        <p>

            <?php echo AdminLTE::vendor_name(AdminLTE::customers_name($invoice)); ?>

            <br>
            <br>

            <br>
            <br>
        </p>    
    </div>

    <div class="col-sm-8 col-xs-7">
        <h1 style="text-align: center;">Invoice</h1>  
        <table class="table table-responsive table-bordered">
            <tr>
                <th>Invoice ID</th>
                <th>
                    Bilty No
                </th>
                <th>
                    Bill
                </th>
                <th>

                    Date
                </th>
            </tr>
            <tr>
                <td><?php echo $invoice; ?></td>
                <td><?php echo $biltyno->biltyno ?></td>
                <td>
                   <?php echo $bill->bill ?></td>

                <td>
                    <?php echo date('d/m/Y', strtotime($date->date)); ?> 
                </td>
            </tr>
        </table>
    </div>       
</div>
<br>


<table class="table table-responsive table-condensed table-bordered" id="table1">

    <thead>


        <tr>
            <th>Sr. No</th>

            <th>Products</th>
            <th>No. of Items</th>
            <th>Price</th>
            <th>Total Item Price</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $total = 0;
        $i = 1;

        foreach ($r as
                $value) {
            ?>
            <tr>

                <th><?php echo $i ?></th>
                <th>
                    <?php echo AdminLTE::cat_name(AdminLTE::category_name($value->product_id)) . " - " . AdminLTE::product_name($value->product_id); ?>
                </th>
                <td>
                    <?php echo $value->stock; ?>
                </td>
                <td>
                    <?php echo $value->price; ?>
                </td>
                <td>
                    <?php echo $value->stock * $value->price; ?>
                </td>

            </tr>
            <?php
            $i++;
            $total += $value->stock * $value->price;
        }
        ?>







       <tr>
                        
                        <td colspan="3">
                            Total Invoice Amount
                        </td>
                      
                        <td>
                            
                        </td>
                        <td  ><?php echo $totalamount->total; ?> </td>
                    </tr>
                           <tr>
                                  <td colspan="3">
                                      Discount <?php if(!empty($bilty->dis_per)){ echo "(" . $bilty->dis_per . ")"; } ?>
                        </td>
                        
                        
                        <td> </td>
                             <td  >- <?php echo $discount->discount; ?></td>
                    </tr>
                     <tr>
                           <td colspan="3">
                               Bilty  
                        </td>
                        <td></td>
                        
                     
                             <td  > <?php echo $bilty->bilty; ?></td>
                    </tr>
        <tr>
            <td colspan="5">
                <br>
                <br>

            </td>
        </tr>
        <tr>
            <td>
                Vendor Old Balance
            </td>
            <td >
                <?php echo $old->old; ?>
            </td>
            <td></td>
            <td>
                Total Amount 
            </td>
            <td  ><?php echo $totalamount->total - $discount->discount; ?></td>

        </tr>
        <tr>
            <td>
                Paid 
            </td>
            
            <td> <?php
                if ($type_payment->type_payment == " ") {
                    echo "";
                }
                else {
                    echo $paid->paid . " By " . AdminLTE::bank_name(explode("-", $type_payment->type_payment)[0]) . " - " . explode("-", $type_payment->type_payment)[1];
                }
                ?></td>
             <td></td>
            <td>
                Net Amount
            </td>
            <td>   <?php echo $net->net; ?></td>

        </tr>
 <tr>
                        <td>
                            Comments
                        </td>
                        <td colspan="5">
                            
                            <?php echo $comments->comments ?>
                        </td>
                    </tr>
    </tbody>

</table>
<h4 class="text-center foot pull-right" style="font-size: 10px">Developed By Xpertz Dev </h4>


</div>
