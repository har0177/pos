<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
  
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage <?php echo $heading; ?> 
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
             <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Product Name</th>
                        <th>Total Expiry</th>
                      
                      
                      
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i =1;
                    foreach ($result as $r) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php  echo AdminLTE::product_name($r->product_id);
                                ?>
                               
                            </td>
                            <td>
                                <?php AdminLTE::sale_count1($r->product_id, "expiry"); ?>
                            </td>
                     
                           
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>