<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage Invoices 
        
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage Invoices
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
           <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                 <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Invoice No</th>
                        <th>Customer</th>
                        
                      
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as $r) {
                        ?>
                        <tr>
                                <td>
                                <?php echo $i ?>
                            </td>
                            <td>
                                <?php echo $r->inv_id ?>
                            </td>
                            
                            <td>
                                <?php echo AdminLTE::customers(AdminLTE::customers_name($r->inv_id)); ?>
                            </td>
                            
                            <td>
                                <div class="hidden-sm action-buttons">
                                    <a class="green" href="<?php echo site_url('admin/expiry/invoice_printing/' . $r->inv_id) ?>">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>
                                                                                                 <!--<a class="red" href="<?php echo site_url('admin/user/delete/' . $r->id) ?>">
                                                                                                                                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                                                                    </a> -->
                                </div>

                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>