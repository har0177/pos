<button onclick="window.print();" class=" hidden-print btn btn-success btn-large">
	<i class="ace-icon fa fa-print bigger-130"></i> Print
</button>
<style>
     .myfoot{
        display: none;
    }
	.table-condensed>thead>tr>th, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>tbody>tr>td, .table-condensed>tfoot>tr>td {
    padding: 2px;
}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    
    line-height: 1.42857143;
    vertical-align: baseline;
 
}
@media print{
.table p, .foot{
font-size: 13px;

}


img{
    height: 80px;
    
}
h4{
    font-size: 10px;
}
h6{
    font-size: 13px;
}

}
</style>


 <div class="row">

    <img class="img-responsive center-block" style="height: 100px;" id="imageshow" src="<?php echo site_url('images/shop.jpg'); ?>" />
			
<table class="table table-responsive table-condensed ">

	<tbody>
<tr>
                <th colspan="4"><h3 style="font-family: times; float: left; text-decoration: underline">Expiry Invoice - Customer</h3> </th>
       
            </tr>

		<tr>
			<th>
                            Customer Name
				<p>
					<?php  echo AdminLTE::customers(AdminLTE::customers_name($invoice)); ?></p>
                        </th>
                        <th>
                            Invoice ID
				<p>
					<?php  echo $invoice; ?></p>
                        </th>
                       
                         <th>
                            Bilty No
				<p>
					<?php  echo AdminLTE::biltyno_sale($invoice); ?></p>
                        </th>
                        <th>
                            Date
				<p>
					<?php  echo date('d/m/Y',strtotime($date->date));?> </p>
                        </th>
                         
		</tr>
	</tbody></table>
 
<table class="table table-responsive table-condensed table-bordered ">

	<thead>


		<tr>
			
                    
                    <th>Products</th>
                    <th>No. of Items Expirys</th>
                    <th>Price</th>
                     <th>Total Item Price</th>
                    </tr>
        </thead>
        <tbody>
            <?php $total = 0; 
            $i = 1;
            foreach ($r as $value) {
                
                         ?>
            <tr>
                        
                     
                        <th>
                            <?php echo AdminLTE::cat_name(AdminLTE::category_name($value->product_id)). " - " . AdminLTE::product_name($value->product_id); ?>
                        </th>
                        <td>
                            <?php echo $value->sale; ?>
                        </td>
                        <td>
                            <?php echo AdminLTE::sprice($value->product_id);?>
                        </td>
                        <td>
                            <?php echo $value->sale * AdminLTE::sprice($value->product_id); ?>
                        </td>
                       
                    </tr>
                     <?php
                     $i++;
                     $total += $value->sale * AdminLTE::sprice($value->product_id);
                     
            }
            
            ?>
                    
                    <tr>
                    <td colspan="1" rowspan="9">
					</td>
                       <th colspan="2">
                            Total Price
                        </th>
                        <td  ><?php echo $total ?> </td>
						</tr>
                                               
                                                 <tr>
                   
                                                     <th colspan="2">
                          Total Amount Remaining
                        </th>
                        <td  ><?php echo $debit->debit ?> </td>
						</tr>
			
                    
                   
	</tbody>

</table>
 <h4 class="text-center foot">Developed and Designed By Xpertz Dev </h4>
      
	  </div>
	 
      
	  