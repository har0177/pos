
<style>

    .myfoot{
        display: none;
    }
    .table-condensed>thead>tr>th, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>tbody>tr>td, .table-condensed>tfoot>tr>td {
        padding: 2px;
    }
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {

        line-height: 1.42857143;
        vertical-align: middle;

    }
    @media print{
        .table p, .foot{
            font-size: 13px;

        }

        #table1 th{
            font-size: 10px;
        }

        #table1 td{
            font-size: 10px;
        }

        img{
            height: 80px;

        }
        h4{
            font-size: 10px;
        }
        h6{
            font-size: 13px;
        }

    }
</style>


<a href="<?php echo site_url('admin/return'); ?>" class="btn btn-success hidden-print pull-right">Back</a>
<div class="row">

    <img class="img-responsive center-block" style="height: 100px;" id="imageshow" src="<?php echo site_url('images/shop.jpg'); ?>" />

    <table class="table table-responsive table-condensed borderless">

        <tbody>
            <tr>
                <th colspan="4"><h3 style="font-family: times; float: left; text-decoration: underline">Expiry Invoice - Customer</h3> </th>

        </tr>

        <tr>
            <th>
                Customer Name
        <p>
            <?php echo AdminLTE::customers(AdminLTE::customers_name($invoice)); ?></p>
        </th>
        <th>
            Invoice ID
        <p>
            <?php echo $invoice; ?></p>
        </th>
        <th>
            Bilty No
        <p>
            <?php echo AdminLTE::biltyno_sale($invoice); ?></p>
        </th>
        <th>
            Date
        <p>
            <?php echo date('d/m/Y', strtotime($date->date)); ?> </p>
        </th>

        </tr>
        </tbody></table>
 <?php echo form_open('admin/expiry/printing/' . $invoice, ['class' => 'form-horizontal']); ?>
       
    <table class="table table-responsive table-condensed table-bordered">

        <thead>


            <tr>


                <th>Products</th>
                <th>No. of Items Expiry</th>
              
            </tr>
        </thead>
        <tbody>
            <?php
            $total = 0;
            $i = 1;
            foreach ($r as
                    $value) {
                ?>
                <tr>


                    <th>
    <?php echo AdminLTE::cat_name(AdminLTE::category_name($value->product_id)) . " - " . AdminLTE::product_name($value->product_id); ?>
                    </th>
                    <td>
                        
   <input id="return" value="<?php echo $value->sale ?>"  type="number" name="return[<?php echo $value->product_id ?>]" class="form-control col-sm-3">
                    </td>
                    

                </tr>
                <?php
              
            }
            ?>

         
        </tbody>

    </table>
         
    <div class="col-xs-12">
        <label>
            <input type="submit" name="submit" value="Save Expiry" class="btn btn-lg btn-success">
        </label>
    </div>

    </form>
   
</div>

