
<br>
<div class="profile-user-info profile-user-info-striped">

    <div class="profile-info-row col-xs-12 col-sm-6">
        <div class="profile-info-name viewname">  Name </div>

        <div class="profile-info-value viewname1">
            <span ><?php echo $data->fullname ?></span>
        </div>
    </div>

    <div class="profile-info-row col-xs-12 col-sm-6">
        <div class="profile-info-name viewname"> Cell</div>

        <div class="profile-info-value viewname1">
            <span ><?php echo $data->phone; ?></span>
        </div>
    </div>


</div>
<br>

<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage Invoices of <?php echo $data->fullname ?>
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Inv No</th>

                        <th>Total</th>
                        <th>Paid</th>
                        <th>Old</th>
						<th>Discount / Bilty</th>
                        <th>Net Amount</th>

                        <th>Date</th>

                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as
                            $rr) {

                        $this->db->where('inv_id', $rr);
                        $query = $this->db->get('invoices_vendor');

                        foreach ($query->result() as
                                $r) {
                            
                            ?>

                            <tr>
                                <td>
                                    <?php echo $i ?>
                                </td>
                                <td>
                                    <?php echo $r->inv_id ?>
                                </td>


                                <td>
                                    <?php echo $r->total ?>
                                </td>
                                <td>
                                    <?php echo $r->paid ?>
                                </td>
                                <td>
                                    <?php echo $r->old ?>
                                </td>
                               
								 <td>
                                    <?php echo $r->discount  . " / ". $r->bilty ?>
                                </td>

                              
                                <td>
                                    <?php echo $r->net ?>
                                </td>

                                <td>
                                    <?php echo $r->date ?>
                                </td>

                                <td>
                                    <div class="hidden-sm action-buttons">
                                        <a class="green hidden-print" href="<?php echo site_url('admin/stock/invoice_printing/' . $r->inv_id) ?>" target="_blank">
                                            <i class="ace-icon fa fa-print bigger-130"></i>
                                        </a>
                                                                                                     <!--<a class="red" href="<?php echo site_url('admin/user/delete/' . $r->id) ?>">
                                                                                                                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                                                                        </a> -->
                                    </div>

                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
             Payment Details
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th >Sr. No</th>
                     
                        <th >Credit</th>>
						<th >Discount / Bilty</th>
					<th >Debit</th>	
                      
                        <th >Date</th>
                       
                    </tr>
                </thead>

               <tbody>
                    <?php
                    $i = 1;
                  
                    foreach ($book as $r) {
                     
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                   
                            <td ><?php echo $r->credit ?></td>
                     
							  <td ><?php echo $r->discount  . " / ". $r->bilty ?></td>
                               <td ><?php echo $r->debit ?></td>
                        
                            <td ><?php echo dateformatesformysql_fata($r->date) ?></td>
                           
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                      
                </tbody>
            </table>
        </div>
    </div>
</div>