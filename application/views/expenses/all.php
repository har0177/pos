<div class="page-header">
    <h1>
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?>

        <a href="<?php echo site_url('admin/expenses/add_expname'); ?>" class="btn btn-sm btn-success pull-right">
            <i class="ace-icon fa fa-plus-square"></i> Add Expense Type</a>
                 <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->

<script>
    function add() {

        $('#exp_add_field').css("display", "block");
        $('#exp_min_btn').css("display", "block");
        $('#exp_add_btn').css("display", "none");

    }
    function subtract() {

        $('#exp_add_field').css("display", "none");
        $('#exp_add_btn').css("display", "block");
        $('#exp_min_btn').css("display", "none");

    }
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 col-md-5 col-sm-12 col-lg-5">

            <?php echo form_open_multipart('admin/expenses/create', ['class' => 'form-horizontal']); ?>
            <fieldset >
                <legend>Expanses Information</legend>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="shop">Bank Name</label>

                    <div class="col-xs-12 col-sm-9">
                        <select required name="bank"  class="chosen-select form-control" id="form-field-select-4">

                            <option value="" >Please Select Bank </option>


                            <?php echo AdminLTE::banks(); ?>


                        </select>
                    </div>
                </div>
                <div class="space-2"></div>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Cheque / Receipt</label>

                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="text" id="address" required=""  name="cheque" placeholder="Cheque / Receipt No" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                <div class="space-2"></div>

                <div class="form-group" id="exp_add_btn">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Expense Types: </label>

                    <div class="col-xs-12 col-sm-7">

                        <select name="exp_name" class="chosen-select form-control" id="form-field-select-4">

                            <option value="" >Please Select Expense Type</option>

                            <?php echo AdminLTE::expense_names(); ?>


                        </select>

                    </div>
                    <div class="col-xs-12 col-sm-2">
                             <a href="#" onclick="add()" class="btn btn-sm btn-success">  
            <i class="fa fa-plus-circle"></i></a>
                    </div>

                </div>
                 <div class="form-group" id="exp_add_field" style="display: none">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right">Expanse Name:</label>

                    <div class="col-xs-12 col-sm-7">
                        <div class="clearfix">
                            <input type="text" name="expense_type_new" placeholder="Please Enter Expanse Type" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                      <div class="col-xs-12 col-sm-2">
                         <a href="#" onclick="subtract()" id="exp_min_btn" class="btn btn-sm btn-danger">  
            <i class="fa fa-minus-circle"></i></a>
                    </div>
                </div>

               
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right">Amount:</label>

                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="number" id="name" required="" name="amount" value="0" class="col-xs-12 col-sm-9" />
                        </div>
                    </div>
                </div>
                <div class="hr hr-dotted"></div>


                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right">Comments:</label>

                    <div class="col-xs-12 col-sm-9">
                        <textarea rows='5' style="text-align: left" id="name" required="" name="comments" placeholder="Comments" maxlength="612" class='form-control'></textarea>

                    </div>
                </div>
                <div class="hr hr-dotted"></div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Date: </label>

                    <div class="col-xs-12 col-sm-9">
                        <input type="text" name="date" value="<?php echo date('Y-m-d'); ?>" class="form-control datepicker"/>
                    </div>
                </div>

                <div class="hr hr-dotted"></div>
                <div class="space-8"></div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                        <label>
                            <input type="submit" name="submit" value="Add Expanses" class="btn btn-lg btn-success">
                        </label>
                    </div>
                </div>
            </fieldset>
            </form>

        </div>
        <div class="col-xs-12 col-md-7 col-sm-12 col-lg-7">

            <div class="table-header">
                Manage <?php echo $heading; ?>
            </div>
            <!-- div.table-responsive -->
            <!-- div.dataTables_borderWrap -->
            <div>
                <div class="clearfix">
                    <div class="pull-right tableTools-container"></div>
                </div>
                <table id="dyntable" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Bank</th>
                            <th>Cheque No</th>
                            <th>Type</th>
                            <th>Amount</th>
                            <th>Comments</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $i = 1;
                        $total = 0;
                        foreach ($result as
                                $r) {
                            $total += $r->amount  ;
                            ?>
                            <tr>
                                <td> <?php echo $i ?></td>
                                <td><?php echo AdminLTE::bank_name($r->bank) ?></td>
                                <td><?php echo $r->rec_no ?></td>
                                <td><?php echo AdminLTE::exp_name($r->exp_id) ?></td>
                                <td><?php echo $r->amount ?></td>
                                <td><?php echo $r->comments ?></td>
                                <td><?php echo dateformatesformysql_fata($r->date); ?></td>


                                <td>
                                    <div class="hidden-sm action-buttons">
                                        <a title="View Expanses Form" class="green" href="<?php echo site_url('admin/expenses/view/' . $r->id) ?>">
                                            <i class="ace-icon fa fa-eye bigger-130"></i>
                                        </a>

        <!--                                    <a title="Update Expanses Form" class="light-grey" href="<?php echo site_url('admin/expenses/edit/' . $r->id) ?>">
                                                <i class="ace-icon fa fa-pencil bigger-130"></i>
                                            </a>-->

        <!--                                    <a title="Delete Expanses Form" class="red" onclick="return confirm('Are You Sure Want to Delete it?')" href="<?php echo site_url('admin/expenses/delete/' . $r->id) ?>">
                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                            </a>-->
                                    </div>

                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                            <tr>
                                <th>Total</th>
                                <th></th>
                                <th></th><th></th><th><?php echo $total ?></th><th></th>
                                <th></th><th></th>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center col-xs-12">
                <div id="reg" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
            </div>

        </div>



        <script>
            $(function () {

                Highcharts.chart('reg', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Expenses Detail of  <?php echo date("Y") ?>'
                    },
                    xAxis: {
                        categories:
<?php
$qc = $this->db->query("SELECT MONTHNAME(date) as date, MONTH(date) as month from expenses group by MONTH(date)");
echo json_encode(array_columnn($qc->result(), 'date'), JSON_NUMERIC_CHECK);
?>,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Expenses Detail'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.1,
                            borderWidth: 0
                        },
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: 'Rs. {y}'
                            }
                        }
                    },
                    series: [
<?php
$qs = $this->db->query("select * from expense_names");
$dated = array_columnn($qc->result(), 'month');
$rupee = array();
foreach ($qs->result() as
        $v) {
    foreach ($dated as
            $dd) {
        $qcc = $this->db->query("SELECT SUM(amount) as total from expenses where exp_id = $v->id and MONTH(date) = $dd and YEAR(date) = '" . date("Y") . "'");
        $va = $qcc->row();
        if ($va->total == NULL) {
            $rupee[] = 0;
        }
        else {
            $rupee[] = $va->total;
        }
    }
    ?>
                            {
                                name: '<?php echo AdminLTE::exp_name($v->id) ?>',
                                data: <?php echo json_encode($rupee, JSON_NUMERIC_CHECK) ?>

                            },
    <?php
    unset($rupee);
}
?>



                    ]




                });
            });


        </script>


    </div>
</div>