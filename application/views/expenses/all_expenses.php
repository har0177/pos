<div class="page-header">
    <h1>
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading ?>
     <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 col-md-5 col-sm-12 col-lg-5">
            <?php echo form_open('admin/expenses/add_expname', ['class' => 'form-horizontal']); ?>
            <fieldset >
                <legend>Add New Expense Type</legend>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right">Expense Type:</label>

                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="text" id="name" placeholder="Expense Type" required="" name="exp_name" class="col-xs-12 col-sm-9" />
                        </div>
                    </div>
                </div>

                <div class="hr hr-dotted"></div>


                <div class="form-group">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                        <label>
                            <input type="submit" name="submit" value="Add Expense Type" class="btn btn-lg btn-success">
                        </label>
                    </div>
                </div>
            </fieldset>
            </form>
        </div>
        <div class="col-xs-12 col-md-7 col-sm-12 col-lg-7">

            <div class="table-header">
                Manage <?php echo $heading ?>
            </div>
            <!-- div.table-responsive -->
            <!-- div.dataTables_borderWrap -->

               <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
  <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Type</th>

                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as
                            $r) {
                        ?>
                        <tr> <td> <?php echo $i ?></td><td><?php echo $r->exp_name ?></td>
                            <td>
                                <div class="hidden-sm action-buttons">
                                    <a class="green" title="Update Expense Type" href="<?php echo site_url('admin/expenses/edit_expname/' . $r->id) ?>">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                            <!--                                    <a class="red" title="Delete Expense Name" href="<?php echo site_url('admin/school/delete_batch/' . $r->batch_id) ?>" onclick="return confirm('Are You Sure Want to Delete it?');">
                                                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                </a> -->
                                </div>

                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>

</div>