

<style>
    @media print{
        .table{
            font-size: 16px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        .table th{
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }
    }
</style>



<style>
    .panel-heading.collapsed .fa-chevron-down,
    .panel-heading .fa-chevron-right {
        display: none;

    }

    .panel-info>.panel-heading {
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }



    .panel-heading.collapsed .fa-chevron-right,
    .panel-heading .fa-chevron-down {
        display: inline-block;

    }

    i.fa {
        cursor: pointer;
        margin-right: 5px;
    }

    .collapsed ~ .panel-body {
        padding: 0;
    }

</style>
<script type="text/javascript">
    function add_data() {

        var table_data = [];
        $('#table tr').each(function (row, tr) {

            if ($(tr).find('td:eq(0)').text() == "") {

            } else {
console.log($(tr).find('td:eq(0)').text());

                var sub = {
                    'pid': $(tr).find('td:eq(0)').text(),
                    'stock': $(tr).find('td:eq(2)').find('input').val(),
                    'sprice': $(tr).find('td:eq(3)').text(),
                    'inv': $('#inv').val()
                };
                table_data.push(sub);
            }
        }
        );
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Save Invoice!'},
        function () {
            var data = {
                "data_table": table_data
            };
            $.ajax({
                data: data,
                url: "<?php echo site_url('admin/returns/add_return'); ?>",
                crossOrigin: false,
                type: "POST",
                dataType: 'json',
                success: function (result) {
                    if (result.status == "failed") {
                        swal("Error Saving", "", "warning");
                    } else if (result.status == "nostock") {
                        swal("No Stock Available", "", "warning");
                    } else if (result.status == "greater") {
                        swal("Stock Greater than Available Stock", "", "warning");
                    } else {
                        swal({
                            title: 'Successfully Saved',
                            text: " ",
                            type: 'success'},
                        function () {
                            $.ajax({
                                url: "<?php echo site_url('admin/sale/invoice_show'); ?>",
                                type: 'POST',
                                data: {id: result.status},
                                dataType: 'json',
                                success: function (data) {
                                    $('.step-content').hide();
                                    $('.invoice').html(data);
                                }
                            });
                        });
                    }
                }
            });
        }
        );
    }

</script>
<div class="row">
    <div class="col-xs-12">

        <div class="widget-body">
            <div class="widget-main">
                <div id="fuelux-wizard-container">
                    <div class="step-content pos-rel form-horizontal hidden-print" >

                        <div class="row">
                            <div class="col-sm-6">

                                <h2 style="text-align: center">NOOR CORPORATION</h2>
                                <h3 style="text-align: center">Abasin Market No.2 Mingora Swat</h3>
                                <h4 style="text-align: center">Mobile No: 0346 - 9426741 </h4>


                                <div class="col-sm-12">
                                    <h1 style="text-align: center;">Invoice</h1>  
                                    <table class="table table-responsive table-bordered">
                                        <tr>
                                            <th>Invoice ID</th>
                                            <th>
                                                Bill No
                                            </th>
                                            <th>

                                                Date
                                            </th>
                                        </tr>
                                        <tr>
                                            <td><?php echo $invoice; ?><input type="hidden" id="inv" value="<?php echo $invoice ?>"></td>

                                            <td>
                                                <?php echo AdminLTE::billno_sale($invoice); ?></td>

                                            <td>
                                                <?php echo date('d/m/Y', strtotime($date->date)); ?> 
                                            </td>
                                        </tr>
                                    </table>
                                </div>       
                            </div>


                            <div class="col-sm-6">
                                <div class="table-header">
                                    Manage <?php echo $heading; ?> 
                                </div>
                                <!-- div.table-responsive -->
                                <!-- div.dataTables_borderWrap -->
                                <div>
                                    <div class="clearfix">
                                        <div class="pull-right tableTools-container"></div>
                                    </div>

                                    <table id="table" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr><th>Product ID</th>
                                                <th>Product Name</th>
                                                <th>Stock</th>
                                                <th> Price</th>
												    <th> In Stock</th>

                                            </tr>
                                        </thead>
                                        <tbody id="tbody">
                                            <?php
                                            $i = 1;
                                            foreach ($r as
                                                    $value) {
                                                ?>
                                                <tr class="p<?php echo $value->product_id; ?>">

                                                    <td>
                                                        <?php echo $value->product_id ?>
                                                    </td>
                                                    <td>
                                                        <?php echo AdminLTE::cat_name(AdminLTE::category_name($value->product_id)) . " - " . AdminLTE::product_name($value->product_id); ?>
                                                    </td>


                                                    <td>

                                                        <input id="return" value="0"  type="number" name="return[<?php echo $value->product_id ?>]" onblur='totalamount()' class="form-control col-sm-3">

                                                    </td>
                                                    <td>
                                                        <?php echo $value->price ?>
                                                    </td>
													 <td>
                                                        <?php echo $value->sale ?>
                                                    </td>



                                                </tr>
                                                <?php
                                            }
                                            ?>


                                        </tbody>

                                    </table>

                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                        <label>
                                            <input type="button" id="save" onclick="add_data()" class="btn btn-lg btn-success" value="Proceed">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="invoice"></div>
                </div><!-- /.widget-main -->

            </div>


        </div><!-- /.col -->

    </div>
</div>