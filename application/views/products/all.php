<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
        <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<script>
    function add() {

        $('#exp_add_field').css("display", "block");
        $('#exp_min_btn').css("display", "block");
        $('#exp_add_btn').css("display", "none");

    }
    function subtract() {

        $('#exp_add_field').css("display", "none");
        $('#exp_add_btn').css("display", "block");
        $('#exp_min_btn').css("display", "none");

    }
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 col-md-5 col-sm-12 col-lg-5">
            <?php echo form_open('admin/products/add', ['class' => 'form-horizontal']); ?>

            <fieldset >
                <legend>Product Information</legend>

                <div class="form-group" id="exp_add_btn">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="product">Category Name</label>

                    <div class="col-xs-12 col-sm-7">
                        <select  name="category"  class="chosen-select form-control" id="form-field-select-4">


                            <?php echo AdminLTE::category(); ?>


                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <a href="#" onclick="add()" class="btn btn-sm btn-success">  
                            <i class="fa fa-plus-circle"></i></a>
                    </div>

                </div>
                <div class="form-group" id="exp_add_field" style="display: none">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right">Category Name:</label>

                    <div class="col-xs-12 col-sm-7">
                        <div class="clearfix">
                            <input type="text" name="cat_new" placeholder="Please Enter Category Name" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <a href="#" onclick="subtract()" id="exp_min_btn" class="btn btn-sm btn-danger">  
                            <i class="fa fa-minus-circle"></i></a>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Product Name:</label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="text" id="name" required="" name="name" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Purchase Price:</label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="number" id="name" min="0" step="any" required="" name="pprice" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Sell Price:</label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="number" id="name" required="" min="0" step="any" name="sprice" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Wholesale Price:</label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="number" id="name" required="" min="0" step="any" name="hprice" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>


                <div class="hr hr-dotted"></div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                        <label>
                            <input type="submit" name="submit" value="Add Products" class="btn btn-lg btn-success">
                        </label>
                    </div>
                </div>

            </fieldset>
            </form>

        </div>
        <div class="col-xs-12 col-md-7 col-sm-12 col-lg-7">

            <div class="table-header">
                Manage <?php echo $heading; ?> 
            </div>
            <!-- div.table-responsive -->
            <!-- div.dataTables_borderWrap -->
            <div>
                <div class="clearfix">
                    <div class="pull-right tableTools-container"></div>
                </div>
                <table id="datatable" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Name</th>

                            <th> P Price</th>
                            <th> R Price</th>
                            <th> W Price</th>


                            <th>Action</th>
                        </tr>
                    </thead>

                 
                </table>
            </div>
        </div></div>
</div>

<script type="text/javascript">
      jQuery(function ($) {
        $( '#datatable' ).DataTable( {
           "processing": true,
           "serverSide": true,
           "order": [],
           "ajax": {
                url: "<?php echo site_url('admin/products/fetch_data'); ?>",
               type: "POST"
               },
               "ColumnDefs":[{
                  "targets": [0,5],
                   "orderable": false
               }]
        });
    });
</script>