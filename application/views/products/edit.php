<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Update <?php echo $heading; ?>
        <a href="<?php echo site_url('admin/products'); ?>" class="btn btn-sm btn-success pull-right">  
            <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-body">
                <div class="widget-main">
                    <div id="fuelux-wizard-container">
                        <div class="step-content pos-rel">
                            <?php echo form_open('', ['class' => 'form-horizontal']); ?>
<div class="form-group">
								<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="product">Category Name</label>

								<div class="col-xs-12 col-sm-3">
								<select required name="category"  class="chosen-select form-control" id="form-field-select-4">
                                                               
                          <option value="" >Please Select Category </option>
                     

                          <?php echo AdminLTE::category($result->cat_id); ?>
                                                              
                          
					             </select>
								</div>
							</div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Product Name:</label>
                                <div class="col-xs-12 col-sm-3">
                                    <div class="clearfix">
                                        <input type="text" id="name" required="" name="name" value="<?php echo $result->name; ?>" class="col-xs-12 col-sm-12" />
                                    </div>
                                </div>
                            </div>
                            
                             <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Purchase Price:</label>
                    <div class="col-xs-12 col-sm-3">
                        <div class="clearfix">
                            <input type="number" id="name" required="" min="0" step="any" value="<?php echo $result->pprice; ?>" name="pprice" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Sell Price:</label>
                    <div class="col-xs-12 col-sm-3">
                        <div class="clearfix">
                            <input type="number" id="name" required="" min="0" step="any" name="sprice" value="<?php echo $result->sprice; ?>" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
				
				 <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Wholesale Price:</label>
                    <div class="col-xs-12 col-sm-3">
                        <div class="clearfix">
                            <input type="number" id="name" required="" min="0" step="any" name="hprice" value="<?php echo $result->hprice; ?>" class="col-xs-12 col-sm-12" />
                        </div>
                    </div>
                </div>
                           

                            <div class="hr hr-dotted"></div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                    <label>
                                        <input type="submit" onclick="return confirm('Are You Sure Want to Update it?');"  name="submit" value="Update Product" class="btn btn-lg btn-success">
                                    </label>
                                </div>
                            </div>
<?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.widget-main -->
        </div><!-- /.widget-body -->
    </div>


</div><!-- /.col -->