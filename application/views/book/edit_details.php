<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Update <?php echo $heading; ?>
        <a href="<?php echo site_url('admin/book'); ?>" class="btn btn-sm btn-success pull-right">  
            <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-body">
                <div class="widget-main">
                    <div id="fuelux-wizard-container">
                        <div class="step-content pos-rel">
                            <?php echo form_open('', ['class' => 'form-horizontal']); ?>
<?php 
foreach ($result as $r) {
?>

                             <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3" for="email">Customer Name:</label>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" disabled="" name="total" required="" value="<?php echo AdminLTE::customers(explode("_", $r->shop_id)[1]); ?>" id="total" class="col-xs-12 col-sm-6" />
                                        <input type="hidden" name="customer"  value="<?php echo explode("_", $r->shop_id)[1]; ?>" id="total" class="col-xs-12 col-sm-6" />
                                      
                                    </div>
                                </div>
                            </div>
                            <div class="space-2"></div>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3" for="email">Total Amount:</label>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" disabled="" name="total" required="" value="<?php echo $r->total ?>" id="total" class="col-xs-12 col-sm-6" />
                                      
                                    </div>
                                </div>
                            </div>

                            <div class="space-2"></div>
 <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Bank Name:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" disabled="" id="address" required="" value="<?php echo AdminLTE::bank_name($r->bank_id) ?>" name="credit" class="col-xs-12 col-sm-6" />
                                          
                                    </div>
                                </div>
                            </div>
                              <div class="space-2"></div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Deposit:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" disabled="" id="address" required="" value="<?php echo $r->deposit ?>" name="credit" class="col-xs-12 col-sm-6" />
                                          
                                    </div>
                                </div>
                            </div>

                            <div class="space-2"></div>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Withdraw:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" id="address" disabled="" required="" value="<?php echo $r->withdraw ?>" name="credit" class="col-xs-12 col-sm-6" />
                                        

                                    </div>
                                </div>
                            </div>
                            
 <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Cheque / Receipt</label>

                                    <div class="col-xs-12 col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="address" required=""  name="cheque" placeholder="Cheque / Receipt No" class="col-xs-12 col-sm-6" value="<?php echo $r->cheque ?>"/>
                                        </div>
                                    </div>
                                </div>
    <div class="space-2"></div>
                    <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3">Date:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" disabled="" id="name" required="" name="date" class="col-xs-12 col-sm-6 datepicker" value="<?php echo $r->date; ?>" />
                                    </div>
                                </div>
                            </div>
    <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Remaining Amount:</label>

                                    <div class="col-xs-12 col-sm-9">
                                        <div class="clearfix">
                                            <input type="number" disabled="" id="address" required=""  name="newcredit" class="col-xs-12 col-sm-6" value="<?php echo$r->remaining ?>"/>
                                        </div>
                                    </div>
                                </div>
<?php } ?>
                            <div class="space-2"></div>
                            <div class="hr hr-dotted"></div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                    <label>
                                        <input type="submit" name="submit" value="Update Amount" class="btn btn-lg btn-success">
                                    </label>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.widget-main -->
        </div><!-- /.widget-body -->
    </div>


</div><!-- /.col -->