<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Update <?php echo $heading; ?>
        <a href="<?php echo site_url('admin/book'); ?>" class="btn btn-sm btn-success pull-right">  
            <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-body">
                <div class="widget-main">
                    <div id="fuelux-wizard-container">
                        <div class="step-content pos-rel">
                            <?php echo form_open('', ['class' => 'form-horizontal']); ?>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="shop">Customer Name</label>

                                <div class="col-xs-12 col-sm-4">
                                    <select required name="shop"  class="chosen-select form-control" id="form-field-select-4">

                                        <option value="" >Please Select Customer </option>


                                        <?php echo AdminLTE::shopkeepers($result->shop_id); ?>


                                    </select>
                                </div>
                            </div>
                            <div class="space-2"></div>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3" for="email">Total Amount:</label>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" disabled="" name="total" required="" value="<?php echo $result->total ?>" id="total" class="col-xs-12 col-sm-6" />
                                         <input type="hidden" id="address" required="" value="<?php echo $result->total ?>" name="totalhidden" class="col-xs-12 col-sm-6" /> 
                                    </div>
                                </div>
                            </div>

                            <div class="space-2"></div>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Credit Amount:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" disabled="" id="address" required="" value="<?php echo $result->credit ?>" name="credit" class="col-xs-12 col-sm-6" />
                                        <input type="hidden" id="address" required="" value="<?php echo $result->credit ?>" name="credithidden" class="col-xs-12 col-sm-6" />      
                                    </div>
                                </div>
                            </div>

                            <div class="space-2"></div>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Debit Amount:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" id="address" disabled="" required="" value="<?php echo $result->debit ?>" name="credit" class="col-xs-12 col-sm-6" />
                                        <input type="hidden" id="address" required="" value="<?php echo $result->debit ?>" name="debithidden" class="col-xs-12 col-sm-6" />

                                    </div>
                                </div>
                            </div>
                            
                            <?php if ($result->total === $result->credit) {
                                ?>
                                <div class="space-2"></div>

                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">New Amount to be Credit:</label>

                                    <div class="col-xs-12 col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="address" disabled="" required=""  value="No Debit Amount: Amount Wasool" class="col-xs-12 col-sm-6" />
                                        </div>
                                    </div>
                                </div>
                                <?php }
                            else {
                                ?>
                                <div class="space-2"></div>

                                

<div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="shop">Bank Name</label>

                                <div class="col-xs-12 col-sm-4">
                                    <select required name="bank"  class="chosen-select form-control" id="form-field-select-4">

                                        <option value="" >Please Select Bank </option>


                                        <?php echo AdminLTE::banks(); ?>


                                    </select>
                                </div>
                            </div>
 <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Cheque / Receipt</label>

                                    <div class="col-xs-12 col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="address" required=""  name="cheque" placeholder="Cheque / Receipt No" class="col-xs-12 col-sm-6" />
                                        </div>
                                    </div>
                                </div>
    <div class="space-2"></div>
                    <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3">Date:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="name" required="" name="date" class="col-xs-12 col-sm-6 datepicker" value="<?php echo date('Y-m-d'); ?>" />
                                    </div>
                                </div>
                            </div>
    <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">New Amount to be Deposited:</label>

                                    <div class="col-xs-12 col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="address" required=""  name="newcredit" class="col-xs-12 col-sm-6" />
                                        </div>
                                    </div>
                                </div>
     <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Discount:</label>

                                    <div class="col-xs-12 col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="address" required=""  name="discount" value="0" class="col-xs-12 col-sm-6" />
                                             <input type="hidden" id="address" required="" value="<?php echo $result->discount ?>" name="dischidden" class="col-xs-12 col-sm-6" />
                                        </div>
                                    </div>
                                </div>
     <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Bilty:</label>

                                    <div class="col-xs-12 col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="address" required=""  name="bilty" value="0" class="col-xs-12 col-sm-6" />
                                             <input type="hidden" id="address" required="" value="<?php echo $result->bilty ?>" name="biltyhidden" class="col-xs-12 col-sm-6" />
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>



                            <div class="space-2"></div>
                            <div class="hr hr-dotted"></div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                    <label>
                                        <input type="submit" name="submit" value="Update Amount" class="btn btn-lg btn-success">
                                    </label>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.widget-main -->
        </div><!-- /.widget-body -->
    </div>


</div><!-- /.col -->