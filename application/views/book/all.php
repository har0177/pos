<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage <?php echo $heading; ?> 
          <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
 <a href="<?php echo site_url('admin/book/add'); ?>" class="btn btn-sm btn-success pull-right">  
          <i class="ace-icon fa fa-plus-square"></i> Add Customer Payment</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage <?php echo $heading; ?>
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">

                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Full Name</th>

                        <th class="hidden-480">Total Amount</th>
                        <th class="hidden-480">Credit</th>
                        <th class="hidden-480">Debit</th>
                        <th class="hidden-480">Discount / Bilty</th>
                      

                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    foreach ($result as
                            $r) {
                        ?>
                        <tr>
                            <td>
    <?php echo $i; ?>
                            </td>
                            <td>
                                  <a  href="<?php echo site_url('admin/book/invoices/' . $r->shop_id) ?>">
    <?php echo AdminLTE::customers($r->shop_id) ?>
                                  </a>
                            </td>
                            <td class="hidden-480"><?php echo $r->total ?></td>
                            <td class="hidden-480"><?php echo $r->credit ?></td>
                            <td class="hidden-480"><?php echo $r->debit ?></td>
                             <td class="hidden-480"><?php echo $r->discount . " / ".  $r->bilty?></td>

                            <td><?php
                                if ($r->total - $r->discount + $r->bilty== $r->credit) {
                                    echo "<span class='label label-large label-success'>Wasool</span>";
                                }
                                else {
                                    echo "<span class='label label-large label-inverse'>Baqaya</span>";
                                }
                                ?>	</td>
                            <?php
                                if ($r->total - $r->discount + $r->bilty == $r->credit) {
                                    echo "<td> - </td>";
                                }else{
                                    ?>
                            <td>
                                <div class="hidden-sm action-buttons">
                                    <a class="green" href="<?php echo site_url('admin/book/edit/' . $r->id) ?>">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                                                                                                                        <!--<a class="red" href="<?php echo site_url('admin/user/delete/' . $r->id) ?>">
                                                                                                                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                                                                                                        </a> -->
                                </div>

                            </td>
                            <?php
                                }
                                ?>
                            
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>