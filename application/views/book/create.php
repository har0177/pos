<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Add <?php echo $heading; ?>
        <a href="<?php echo site_url('admin/book'); ?>" class="btn btn-sm btn-success pull-right">  
            <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-body">
                <div class="widget-main">
                    <div id="fuelux-wizard-container">
                        <div class="step-content pos-rel">
                            <?php echo form_open('', ['class' => 'form-horizontal']); ?>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="shop">Customer Name</label>

                                <div class="col-xs-12 col-sm-4">
                                    <select required name="shop"  class="chosen-select form-control" id="form-field-select-4">

                                        <option value="" >Please Select Customer </option>


                                        <?php echo AdminLTE::shopkeepers(); ?>


                                    </select>
                                </div>
                            </div>
                            
 <div class="space-2"></div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3" for="email">Total Amount:</label>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" name="total" required="" id="total" class="col-xs-12 col-sm-6" />
                                    </div>
                                </div>
                            </div>
                            <div class="hr hr-dotted"></div>
                            <div class="space-2"></div>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">Credit Amount:</label>

                                <div class="col-xs-12 col-sm-9">
                                    <div class="clearfix">
                                        <input type="number" id="address" required="" name="credit" class="col-xs-12 col-sm-6" />
                                    </div>
                                </div>
                            </div>




                            <div class="space-2"></div>
                            <div class="hr hr-dotted"></div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                    <label>
                                        <input type="submit" name="submit" value="Add Amount" class="btn btn-lg btn-success">
                                    </label>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.widget-main -->
        </div><!-- /.widget-body -->
    </div>


</div><!-- /.col -->