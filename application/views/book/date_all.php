<div class="page-header">
    <h1> 
        <i class="ace-icon fa fa-user"></i>
        Manage Data Wise <?php echo $heading; ?> 
        <a href="<?php echo site_url('admin/book/add'); ?>" class="btn btn-sm btn-success pull-right">  
            <i class="ace-icon fa fa-plus-square"></i> Add New</a>
               <a href="javascript:window.history.go(-1)" class="btn btn-sm btn-primary hidden-print pull-right"> <i class="ace-icon fa fa-arrow-circle-o-left"></i> Back</a>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
            Manage Data Wise  <?php echo $heading; ?> 
        </div>
        <!-- div.table-responsive -->
        <!-- div.dataTables_borderWrap -->
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Full Name</th>
                        <th class="hidden-480">Debit</th>
                        <th class="hidden-480">Credit</th>
                        
                         <th class="hidden-480">Discount</th>


                        <th class="hidden-480">Date</th>

                    </tr>
                </thead>

                <tbody>
                    <?php
                    foreach ($result as
                            $r) {
                        ?>
                        <tr>
                            <td>
    <?php echo AdminLTE::customers($r->shop_id) ?>
                            </td>
                            <td class="hidden-480"><?php echo $r->debit ?></td>
                            <td class="hidden-480"><?php echo $r->credit ?></td>
                              <td class="hidden-480"><?php echo $r->discount ?></td>

                            <td class="hidden-480"><?php echo dateformatesformysql_fata($r->date) ?></td>

                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>