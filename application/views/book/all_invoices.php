  

<div class="row">
    <div class="col-xs-12">
        <div class="table-header">
             Payment Details of <?php echo $data->fullname ?>
        </div>
      
        <div>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <table id="dyntable" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                        <th>Sr. No</th>
                    
                        <th >Credit</th>
                        <th >Discount / Bilty</th>
					 <th >Debit</th>	
                      
                        <th >Date</th>
                       
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                 
                    foreach ($book as $r) {
                        
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                   
                            <td ><?php echo $r->credit ?></td>
                            <td ><?php echo $r->discount . " / " . $r->bilty ?></td>
                               <td ><?php echo $r->debit ?></td>
                        
                            <td ><?php echo dateformatesformysql_fata($r->date) ?></td>
                           
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                  
                </tbody>
            </table>
        </div>
    </div>
</div>